
import mysql.connector

mydb = mysql.connector.connect(
    host="192.168.10.68",
    user="simec",
    passwd="S!mec12345678",
    database="daito"
)

mycursor = mydb.cursor()

ids = [1, 2, 45055, 45058, 45059]

sql="""INSERT INTO `t_message` 
    (`f_message_id`, `f_push_reserve_id`, `f_agency_id`, `f_message_title`, `f_message`,
    `f_schedule_start_date_time`, `f_url`, `f_send_flg`, `f_del_flg`) VALUES 
    (NULL, '2', %s, %s, %s, '2019-12-20 16:14:00', 'te', '0', '0')"""
vals = []
for i in range(50) :
    vals.append((str(ids[i%5]), "title-" + str(i) , "msg-" + str(i)))
#print(vals)
mycursor.executemany(sql, vals)

mydb.commit()

print(mycursor.rowcount, "record inserted.")