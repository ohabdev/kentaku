var schedule = require('node-schedule');
const fcm = require('./sendFCM');

var j = schedule.scheduleJob('*/2 * * * * *', function(){
    console.log('Sending FCM ' + new Date());
    fcm.sendFCM();
});