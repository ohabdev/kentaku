const fs = require('fs');
var path = require('path');
var mime = require('mime-types');


function fz(filePath = "") {
    try {
        
        let buffer = fs.readFileSync(filePath);
        let file_ext = {
            "ext": "",
            "mime": ""
        };
        try {
            let ext = path.extname(filePath).substr(1);
            let mimeType = mime.contentType(path.extname(filePath));
            if(typeof mimeType === "string" && mimeType !== false) {
                file_ext["ext"] = ext;
                file_ext["mime"] = mimeType;
            }
        } catch (err) {
            console.log(err);
            
        }
        return [[{
            // "path" : filePath,
            "file-info": file_ext,
            "file-base64" : Buffer.from(buffer).toString('base64')
        }], 200, "ok"];
    } catch (error) {
        return [[{
            // "path" : "",
            "file-info": {
                "ext": "",
                "mime": ""
            },
            "file-base64" : ""
        }], 404, "File not Found {error : "+ error + "}"];
    }
    
}

try {
    filePath = process.argv[2];
    console.log(JSON.stringify(fz(filePath)[0][0]["file-info"], null, 4));
} catch (err) {
    console.log(err);
    
}


