var http = require('http');
var fs = require('fs');

const data = JSON.stringify({
    "token" : "697b20cc1a2e15e07a0ebfeaa4caa81fa4de80a47b4fe1b038c185b58bc57771e99681d8fd014b8b3a8d653476309675",
    "payload" : {
        "file-info": {
            "id": "magazine_file",
            "index": "",
            "value": "201911252102271627805407.pdf"
        }
    }
});

const options = {
    hostname: '127.0.0.1',
    port: 3000,
    path: '/api/v2/fs/fike',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': data.length
    }
};


const req = http.request(options, (res) => {
    console.log(`statusCode: ${res.statusCode}`);
    res.on('data', (d) => {
        
    });
});

req.on('error', (error) => {
    console.log(error);
});

req.write(data)
req.end()