SELECT 
	*
FROM (
	SELECT 
		page_content1.f_page_content_id,page_content1.f_page_content_sub_id,page_content1.f_page_title,page_content1.f_current_position,
		IF(CHAR_LENGTH(COALESCE(page_content1.f_parent_position,''))>0,
		page_content1.f_parent_position, 0) AS f_parent_position,
	 	page_content1.f_slag, 
	 	DATE_FORMAT(page_content1.f_disp_date_from, '%Y/%m/%d') AS f_disp_date_from, 
	 	DATE_FORMAT(page_content1.f_disp_date_to, '%Y/%m/%d') AS f_disp_date_to,
	 	page_content1.f_open_flg,page_content1.f_new_flg,page_content1.f_new_flg_days,page_content1.f_individual_chk,page_content1.f_content,page_content1.f_view_level,page_content1.f_order
	FROM t_page_content page_content1
	WHERE 
		    page_content1.f_del_flg = '0' 
		AND f_open_flg = '1' 
		AND f_individual_chk != '1' 
		AND page_content1.f_page_content_id = (
				SELECT 
					f_page_content_id
				FROM t_page_content
				WHERE 
						 f_del_flg = '0' 
					AND f_open_flg = '1' 
					AND f_disp_date_from <= NOW() 
					AND ( f_disp_date_to >= NOW() OR f_disp_date_to IS NULL ) 
						AND f_page_content_sub_id = page_content1.f_page_content_sub_id
				ORDER BY ABS(NOW() - f_disp_date_from) ASC
				LIMIT 0, 1
		)
	GROUP BY page_content1.f_page_content_sub_id
	) page_content
ORDER BY page_content.f_order
