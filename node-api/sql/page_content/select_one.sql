SELECT 
		page_content.f_page_content_id,page_content.f_page_content_sub_id,
		page_content.f_page_title,page_content.f_current_position,page_content.f_current_position AS f_current_position_old,
		page_content.f_parent_position,page_content.f_parent_position AS f_parent_position_old,
		page_content.f_slag, DATE_FORMAT(page_content.f_disp_date_from, '%Y/%m/%d') AS f_disp_date_from,
		DATE_FORMAT(page_content.f_disp_date_to, '%Y/%m/%d') AS f_disp_date_to, 
		DATE_FORMAT(page_content.f_disp_date_from, '%k') AS f_disp_hour_from,
		DATE_FORMAT(page_content.f_disp_date_from, '%i') AS f_disp_minute_from,
		DATE_FORMAT(page_content.f_disp_date_to, '%k') AS f_disp_hour_to,
		DATE_FORMAT(page_content.f_disp_date_to, '%i') AS f_disp_minute_to, 
		DATE_FORMAT(page_content.f_disp_date_to, '%Y/%m/%d %H:%i:%s') AS f_disp_datetime_to, 
		DATE_FORMAT(page_content.f_disp_date_from, '%Y/%m/%d %H:%i:%s') AS f_disp_datetime_from,
		page_content.f_open_flg,page_content.f_new_flg,page_content.f_new_flg_days,page_content.f_individual_chk,
		page_content.f_content,page_content.f_del_flg,page_content.f_reg_account,page_content.f_reg_time,
		page_content.f_upd_account,page_content.f_upd_time,page_content.f_order,page_content.f_view_level,
		(
			SELECT GROUP_CONCAT(f_page_title SEPARATOR ',')
			FROM t_page_content
			WHERE f_del_flg = '0' AND page_content.f_page_content_id = f_parent_position
		) AS f_child_slags
FROM t_page_content page_content
WHERE 
	    page_content.f_del_flg = '0' 
	AND page_content.f_open_flg = '1' 
	AND page_content.f_slag = 'art-2d' 
	AND page_content.f_disp_date_from = (
			SELECT
				MAX(f_disp_date_from) 
			FROM t_page_content 
			WHERE 
				    f_del_flg = '0' 
				AND f_open_flg = '1' 
				AND f_slag = 'art-2d' 
				AND f_disp_date_from <= NOW() 
				AND (f_disp_date_to >= NOW() OR f_disp_date_to IS NULL)
		)