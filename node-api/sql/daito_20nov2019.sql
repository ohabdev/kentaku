-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2019 年 11 月 20 日 12:53
-- サーバのバージョン： 5.5.57-0+deb8u1
-- PHP Version: 5.6.40-0+deb8u4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `daito`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `m_admin`
--

CREATE TABLE IF NOT EXISTS `m_admin` (
`f_admin_id` int(8) unsigned NOT NULL COMMENT '管理者ID',
  `f_surname` varchar(50) DEFAULT NULL COMMENT '名前（姓)',
  `f_firstname` varchar(50) DEFAULT NULL COMMENT '名前（名）',
  `f_surname_kana` varchar(100) DEFAULT NULL COMMENT '名前かな（姓）',
  `f_firstname_kana` varchar(100) DEFAULT NULL COMMENT '名前かな（名）',
  `f_name` text COMMENT '名前',
  `f_mailaddress` varchar(255) DEFAULT NULL COMMENT 'メールアドレス',
  `f_tel` varchar(20) DEFAULT NULL COMMENT '電話番号',
  `f_fax` varchar(20) DEFAULT NULL COMMENT 'FAX番号',
  `f_id` varchar(20) DEFAULT NULL COMMENT 'ID',
  `f_password` varchar(255) DEFAULT NULL COMMENT 'パスワード',
  `f_auth_kbn` char(1) DEFAULT NULL COMMENT '権限区分',
  `f_admin_kbn` char(1) DEFAULT NULL COMMENT '管理区分',
  `f_view_level` char(1) DEFAULT NULL COMMENT '閲覧レベル',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='管理者マスタ';


INSERT INTO `m_admin` (`f_admin_id`, `f_surname`, `f_firstname`, `f_surname_kana`, `f_firstname_kana`, `f_name`, `f_mailaddress`, `f_tel`, `f_fax`, `f_id`, `f_password`, `f_auth_kbn`, `f_admin_kbn`, `f_view_level`, `f_del_flg`, `f_reg_account`, `f_reg_time`, `f_upd_account`, `f_upd_time`) VALUES
(1, 'ノーマル', 'ノーマル', 'ノーマル', 'ノーマル', NULL, 'm-inoue@threet.co.jp', NULL, NULL, 'simplan', '358ebcc36ac0b475d999a6c7bc79f5a3f6b14420922dd0c0a19dc93729e00a1333a7486a27650552', '1', '0', NULL, '1', 1, '2014-09-08 11:28:35', 2, '2016-02-24 17:05:33');

-- --------------------------------------------------------

--
-- テーブルの構造 `m_agency`
--

CREATE TABLE IF NOT EXISTS `m_agency` (
`f_agency_id` int(8) unsigned NOT NULL COMMENT '業者ID',
  `f_agency_type` char(1) DEFAULT NULL COMMENT 'アカウント属性',
  `f_company_cd` varchar(3) DEFAULT NULL COMMENT '会社CD',
  `f_emp_kbn` char(1) DEFAULT NULL COMMENT '従要員区分',
  `f_branch_cd` varchar(3) DEFAULT NULL COMMENT '支部コード',
  `f_agency_cd` varchar(6) DEFAULT NULL COMMENT '業者コード',
  `f_sales_cd` varchar(3) DEFAULT NULL COMMENT '営業所コード',
  `f_industry` varchar(200) DEFAULT NULL COMMENT '業種',
  `f_occupation` varchar(200) DEFAULT NULL COMMENT '職種',
  `f_emp_cd` varchar(8) DEFAULT NULL COMMENT '社員コード',
  `f_name` varchar(200) DEFAULT NULL COMMENT '業者名',
  `f_name_kana` varchar(200) DEFAULT NULL COMMENT '業者名（カナ）',
  `f_director_h_kbn` char(1) DEFAULT NULL COMMENT '役員区分本部',
  `f_director_s_kbn` char(1) DEFAULT NULL COMMENT '役員区分支部',
  `f_employee_attribute` varchar(20) DEFAULT NULL COMMENT '従業員属性',
  `f_sales_name` varchar(200) DEFAULT NULL COMMENT '営業所名',
  `f_representative_name` varchar(200) DEFAULT NULL COMMENT '代表者名',
  `f_representative_name_kana` varchar(200) DEFAULT NULL COMMENT '代表者名（カナ）',
  `f_siten_cd` varchar(6) DEFAULT NULL COMMENT '支店コード',
  `f_parent_id` int(8) DEFAULT NULL COMMENT '所属業者ID',
  `f_name2` varchar(200) DEFAULT NULL COMMENT '氏名',
  `f_name2_kana` varchar(200) DEFAULT NULL COMMENT '氏名(カナ)',
  `f_foreign_nationality_flg` char(1) DEFAULT NULL COMMENT '外国籍フラグ',
  `f_foreign_emp_flg` char(1) DEFAULT NULL COMMENT '外国人雇用フラグ',
  `f_zip` varchar(32) DEFAULT NULL COMMENT '郵便番号',
  `f_prefecture_id` int(2) unsigned DEFAULT NULL COMMENT '都道府県',
  `f_town` varchar(256) DEFAULT NULL COMMENT '市区町村',
  `f_address` varchar(256) DEFAULT NULL COMMENT '住所',
  `f_mailaddress` varchar(255) DEFAULT NULL COMMENT 'メールアドレス',
  `f_tel` varchar(20) DEFAULT NULL COMMENT '電話番号',
  `f_fax` varchar(20) DEFAULT NULL COMMENT 'ＦＡＸ',
  `f_url` varchar(255) DEFAULT NULL COMMENT 'ＵＲＬ',
  `f_id` varchar(20) DEFAULT NULL COMMENT 'ID',
  `f_password` varchar(255) DEFAULT NULL COMMENT 'パスワード',
  `f_csv_del_off_flg` char(1) DEFAULT NULL COMMENT 'CSVアップ時削除不可',
  `f_view_level` char(1) DEFAULT NULL COMMENT '閲覧レベル',
  `f_authentication_flg` char(1) DEFAULT NULL COMMENT '認証フラグ（アカウント有効・無効）',
  `f_register_date` date DEFAULT NULL COMMENT '登録日',
  `f_basic_contract_date` date DEFAULT NULL COMMENT '基本契約締結日',
  `f_date_consignment_contract` date DEFAULT NULL COMMENT '委託契約締結日',
  `f_temporary_suspension_date` date DEFAULT NULL COMMENT '一時取引停止日',
  `f_suspension_release_date` date DEFAULT NULL COMMENT '停止解除日',
  `f_cancellation_date` date DEFAULT NULL COMMENT '取消日',
  `f_revival_date` date DEFAULT NULL COMMENT '取消復活日',
  `f_membership_date` date DEFAULT NULL COMMENT '持株会入会日',
  `f_membership_withdrawal_date` date DEFAULT NULL COMMENT '持株会退会日',
  `f_acct_valid_flg` char(1) DEFAULT NULL COMMENT 'アカウント有効無効フラグ',
  `f_auth` char(1) DEFAULT NULL COMMENT '認証フラグ',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='業者マスタ';

-- --------------------------------------------------------

--
-- テーブルの構造 `m_base`
--

CREATE TABLE IF NOT EXISTS `m_base` (
`f_base_id` int(8) unsigned NOT NULL COMMENT '拠点ID',
  `f_base_code` varchar(8) DEFAULT NULL COMMENT '拠点コード',
  `f_base_name` varchar(50) DEFAULT NULL COMMENT '拠点名',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='拠点マスタ';

-- --------------------------------------------------------

--
-- テーブルの構造 `m_industry`
--

CREATE TABLE IF NOT EXISTS `m_industry` (
`f_industry_id` int(8) unsigned NOT NULL COMMENT '業種ID',
  `f_industry_code` varchar(8) DEFAULT NULL COMMENT '業種コード',
  `f_industry_name` varchar(50) DEFAULT NULL COMMENT '業種名',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='業種マスタ';

-- --------------------------------------------------------

--
-- テーブルの構造 `m_occupation`
--

CREATE TABLE IF NOT EXISTS `m_occupation` (
`f_occupation_id` int(8) unsigned NOT NULL COMMENT '職種ID',
  `f_occupation_code` varchar(8) DEFAULT NULL COMMENT '職種コード',
  `f_occupation_name` varchar(50) DEFAULT NULL COMMENT '職種名',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='職種マスタ';

-- --------------------------------------------------------

--
-- テーブルの構造 `m_prefecture`
--

CREATE TABLE IF NOT EXISTS `m_prefecture` (
`f_prefecture_id` int(4) NOT NULL COMMENT '都道府県ID',
  `f_area_id` int(4) NOT NULL DEFAULT '0' COMMENT 'エリアID',
  `f_prefecture_name` varchar(50) NOT NULL DEFAULT '' COMMENT '都道府県名',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='都道府県マスタ';

-- --------------------------------------------------------

--
-- テーブルの構造 `m_siten`
--

CREATE TABLE IF NOT EXISTS `m_siten` (
`f_siten_id` int(8) unsigned NOT NULL COMMENT '支店ID',
  `f_siten_code` varchar(8) DEFAULT NULL COMMENT '支店コード',
  `f_siten_name` varchar(50) DEFAULT NULL COMMENT '支店名',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='支店マスタ';

-- --------------------------------------------------------

--
-- テーブルの構造 `m_zip`
--

CREATE TABLE IF NOT EXISTS `m_zip` (
`f_zip_id` int(8) unsigned NOT NULL COMMENT '郵便番号ID',
  `f_zip_cd` int(7) unsigned NOT NULL COMMENT '郵便番号コード',
  `f_prefecture_id` int(4) unsigned NOT NULL COMMENT '都道府県ID',
  `f_town` varchar(200) NOT NULL COMMENT '市区町村名',
  `f_regist_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_regist_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_update_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_update_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='郵便番号マスタ';

-- --------------------------------------------------------

--
-- テーブルの構造 `t_about`
--

CREATE TABLE IF NOT EXISTS `t_about` (
`f_about_id` int(8) unsigned NOT NULL COMMENT '会報誌ID',
  `f_release_date` datetime DEFAULT NULL COMMENT '公開日時',
  `f_release_flg` char(1) DEFAULT NULL COMMENT '公開フラグ',
  `f_greeting_content` text COMMENT 'ご挨拶内容',
  `f_main_company_name` varchar(100) DEFAULT NULL COMMENT '会長会社名',
  `f_main_company_url` varchar(255) DEFAULT NULL COMMENT '会長会社URL',
  `f_chairman_ship` varchar(100) DEFAULT NULL COMMENT '会長役職',
  `f_president_name` varchar(100) DEFAULT NULL COMMENT '会長名',
  `f_president_kana` varchar(100) DEFAULT NULL COMMENT '会長名カナ',
  `f_president_image` varchar(255) DEFAULT NULL COMMENT '会長メイン画像',
  `f_president_list_image` varchar(255) DEFAULT NULL COMMENT '会長一覧画像',
  `f_sign_image` varchar(255) DEFAULT NULL COMMENT '会長サイン画像',
  `f_pdf_file` varchar(255) DEFAULT NULL COMMENT '会則PDFアップ',
  `f_pdf_name` varchar(100) DEFAULT NULL COMMENT '会則PDF名',
  `f_pdf_size1` varchar(10) DEFAULT NULL COMMENT 'PDFサイズ1',
  `f_pdf_directory` varchar(255) DEFAULT NULL COMMENT '名簿PDFアップ',
  `f_pdf_directory_name` varchar(100) DEFAULT NULL COMMENT '名簿PDF名',
  `f_pdf_size2` varchar(10) DEFAULT NULL COMMENT 'PDFサイズ2',
  `f_title1` varchar(100) DEFAULT NULL COMMENT '役職1',
  `f_area1` varchar(100) DEFAULT NULL COMMENT 'エリア1',
  `f_name1` varchar(100) DEFAULT NULL COMMENT '氏名1',
  `f_kana_name1` varchar(100) DEFAULT NULL COMMENT '氏名カナ1',
  `f_company_title1` varchar(100) DEFAULT NULL COMMENT '会社役職1',
  `f_company_name1` varchar(100) DEFAULT NULL COMMENT '社名1',
  `f_company_url1` varchar(255) DEFAULT NULL COMMENT '会社URL1',
  `f_image1` varchar(255) DEFAULT NULL COMMENT '画像1',
  `f_title2` varchar(100) DEFAULT NULL COMMENT '役職2',
  `f_area2` varchar(100) DEFAULT NULL COMMENT 'エリア2',
  `f_name2` varchar(100) DEFAULT NULL COMMENT '氏名2',
  `f_kana_name2` varchar(100) DEFAULT NULL COMMENT '氏名カナ2',
  `f_company_title2` varchar(100) DEFAULT NULL COMMENT '会社役職2',
  `f_company_name2` varchar(100) DEFAULT NULL COMMENT '社名2',
  `f_company_url2` varchar(255) DEFAULT NULL COMMENT '会社URL2',
  `f_image2` varchar(255) DEFAULT NULL COMMENT '画像2',
  `f_title3` varchar(100) DEFAULT NULL COMMENT '役職3',
  `f_area3` varchar(100) DEFAULT NULL COMMENT 'エリア3',
  `f_name3` varchar(100) DEFAULT NULL COMMENT '氏名3',
  `f_kana_name3` varchar(100) DEFAULT NULL COMMENT '氏名カナ3',
  `f_company_title3` varchar(100) DEFAULT NULL COMMENT '会社役職3',
  `f_company_name3` varchar(100) DEFAULT NULL COMMENT '社名3',
  `f_company_url3` varchar(255) DEFAULT NULL COMMENT '会社URL3',
  `f_image3` varchar(255) DEFAULT NULL COMMENT '画像3',
  `f_title4` varchar(100) DEFAULT NULL COMMENT '役職4',
  `f_area4` varchar(100) DEFAULT NULL COMMENT 'エリア4',
  `f_name4` varchar(100) DEFAULT NULL COMMENT '氏名4',
  `f_kana_name4` varchar(100) DEFAULT NULL COMMENT '氏名カナ4',
  `f_company_title4` varchar(100) DEFAULT NULL COMMENT '会社役職4',
  `f_company_name4` varchar(100) DEFAULT NULL COMMENT '社名4',
  `f_company_url4` varchar(255) DEFAULT NULL COMMENT '会社URL4',
  `f_image4` varchar(255) DEFAULT NULL COMMENT '画像4',
  `f_title5` varchar(100) DEFAULT NULL COMMENT '役職5',
  `f_area5` varchar(100) DEFAULT NULL COMMENT 'エリア5',
  `f_name5` varchar(100) DEFAULT NULL COMMENT '氏名5',
  `f_kana_name5` varchar(100) DEFAULT NULL COMMENT '氏名カナ5',
  `f_company_title5` varchar(100) DEFAULT NULL COMMENT '会社役職5',
  `f_company_name5` varchar(100) DEFAULT NULL COMMENT '社名5',
  `f_company_url5` varchar(255) DEFAULT NULL COMMENT '会社URL5',
  `f_image5` varchar(255) DEFAULT NULL COMMENT '画像5',
  `f_title6` varchar(100) DEFAULT NULL COMMENT '役職6',
  `f_area6` varchar(100) DEFAULT NULL COMMENT 'エリア6',
  `f_name6` varchar(100) DEFAULT NULL COMMENT '氏名6',
  `f_kana_name6` varchar(100) DEFAULT NULL COMMENT '氏名カナ6',
  `f_company_title6` varchar(100) DEFAULT NULL COMMENT '会社役職6',
  `f_company_name6` varchar(100) DEFAULT NULL COMMENT '社名6',
  `f_company_url6` varchar(255) DEFAULT NULL COMMENT '会社URL6',
  `f_image6` varchar(255) DEFAULT NULL COMMENT '画像6',
  `f_title7` varchar(100) DEFAULT NULL COMMENT '役職7',
  `f_area7` varchar(100) DEFAULT NULL COMMENT 'エリア7',
  `f_name7` varchar(100) DEFAULT NULL COMMENT '氏名7',
  `f_kana_name7` varchar(100) DEFAULT NULL COMMENT '氏名カナ7',
  `f_company_title7` varchar(100) DEFAULT NULL COMMENT '会社役職7',
  `f_company_name7` varchar(100) DEFAULT NULL COMMENT '社名7',
  `f_company_url7` varchar(255) DEFAULT NULL COMMENT '会社URL7',
  `f_image7` varchar(255) DEFAULT NULL COMMENT '画像7',
  `f_title8` varchar(100) DEFAULT NULL COMMENT '役職8',
  `f_area8` varchar(100) DEFAULT NULL COMMENT 'エリア8',
  `f_name8` varchar(100) DEFAULT NULL COMMENT '氏名8',
  `f_kana_name8` varchar(100) DEFAULT NULL COMMENT '氏名カナ8',
  `f_company_title8` varchar(100) DEFAULT NULL COMMENT '会社役職8',
  `f_company_name8` varchar(100) DEFAULT NULL COMMENT '社名8',
  `f_company_url8` varchar(255) DEFAULT NULL COMMENT '会社URL8',
  `f_image8` varchar(255) DEFAULT NULL COMMENT '画像8',
  `f_title9` varchar(100) DEFAULT NULL COMMENT '役職9',
  `f_area9` varchar(100) DEFAULT NULL COMMENT 'エリア9',
  `f_name9` varchar(100) DEFAULT NULL COMMENT '氏名9',
  `f_kana_name9` varchar(100) DEFAULT NULL COMMENT '氏名カナ9',
  `f_company_title9` varchar(100) DEFAULT NULL COMMENT '会社役職9',
  `f_company_name9` varchar(100) DEFAULT NULL COMMENT '社名9',
  `f_company_url9` varchar(100) DEFAULT NULL COMMENT '会社URL9',
  `f_image9` varchar(255) DEFAULT NULL COMMENT '画像9',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='登録用テーブル';

-- --------------------------------------------------------

--
-- テーブルの構造 `t_agency_auto_login`
--

CREATE TABLE IF NOT EXISTS `t_agency_auto_login` (
  `f_agency_id` int(8) NOT NULL DEFAULT '0' COMMENT '法人ID',
  `f_key` text COMMENT 'キー',
  `f_expire` datetime DEFAULT NULL COMMENT '有効期限'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='自動ログインテーブル';

-- --------------------------------------------------------

--
-- テーブルの構造 `t_agency_login_history`
--

CREATE TABLE IF NOT EXISTS `t_agency_login_history` (
`f_agency_login_history_id` int(8) NOT NULL COMMENT '業者ログイン履歴ID',
  `f_login_date` datetime DEFAULT NULL COMMENT 'ログイン日時',
  `f_agency_id` int(8) unsigned DEFAULT NULL COMMENT '業者ID',
  `f_agency_type` char(1) DEFAULT NULL COMMENT '属性',
  `f_branch_cd` varchar(3) DEFAULT NULL COMMENT '支部',
  `f_agency_cd` varchar(3) DEFAULT NULL COMMENT '業者',
  `f_sales_cd` varchar(3) DEFAULT NULL COMMENT '営業所',
  `f_name` varchar(200) DEFAULT NULL COMMENT '業者名',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='業者ログイン履歴';

-- --------------------------------------------------------

--
-- テーブルの構造 `t_banner`
--

CREATE TABLE IF NOT EXISTS `t_banner` (
  `f_banner_id` int(8) unsigned NOT NULL COMMENT 'バナーID',
  `f_image1` varchar(255) DEFAULT NULL COMMENT '画像1',
  `f_thumb1` varchar(255) DEFAULT NULL COMMENT 'サムネイル1',
  `f_image_alt1` varchar(255) DEFAULT NULL COMMENT '画像ALT1',
  `f_page_content_id1` varchar(50) DEFAULT NULL COMMENT 'リンクURL1',
  `f_sort_no1` int(8) unsigned DEFAULT NULL COMMENT '表示順',
  `f_image2` varchar(255) DEFAULT NULL COMMENT '画像2',
  `f_thumb2` varchar(255) DEFAULT NULL COMMENT 'サムネイル2',
  `f_image_alt2` varchar(255) DEFAULT NULL COMMENT '画像ALT2',
  `f_page_content_id2` varchar(50) DEFAULT NULL COMMENT 'リンクURL2',
  `f_sort_no2` int(8) unsigned DEFAULT NULL COMMENT '表示順',
  `f_image3` varchar(255) DEFAULT NULL COMMENT '画像3',
  `f_thumb3` varchar(255) DEFAULT NULL COMMENT 'サムネイル3',
  `f_image_alt3` varchar(255) DEFAULT NULL COMMENT '画像ALT3',
  `f_page_content_id3` varchar(255) DEFAULT NULL COMMENT 'リンクURL3',
  `f_sort_no3` int(8) unsigned DEFAULT NULL COMMENT '表示順',
  `f_image4` varchar(255) DEFAULT NULL COMMENT '画像4',
  `f_thumb4` varchar(255) DEFAULT NULL COMMENT 'サムネイル4',
  `f_image_alt4` varchar(255) DEFAULT NULL COMMENT '画像ALT4',
  `f_page_content_id4` varchar(50) DEFAULT NULL COMMENT 'リンクURL4',
  `f_sort_no4` int(8) unsigned DEFAULT NULL COMMENT '表示順',
  `f_image5` varchar(255) DEFAULT NULL COMMENT '画像5',
  `f_thumb5` varchar(255) DEFAULT NULL COMMENT 'サムネイル5',
  `f_image_alt5` varchar(255) DEFAULT NULL COMMENT '画像ALT5',
  `f_page_content_id5` varchar(50) DEFAULT NULL COMMENT 'リンクURL5',
  `f_sort_no5` int(8) unsigned DEFAULT NULL COMMENT '表示順',
  `f_image6` varchar(255) DEFAULT NULL COMMENT '画像6',
  `f_thumb6` varchar(255) DEFAULT NULL COMMENT 'サムネイル6',
  `f_image_alt6` varchar(255) DEFAULT NULL COMMENT '画像ALT6',
  `f_page_content_id6` varchar(50) DEFAULT NULL COMMENT 'リンクURL6',
  `f_sort_no6` int(8) unsigned DEFAULT NULL COMMENT '表示順',
  `f_disp_flg` char(1) DEFAULT NULL COMMENT '表示フラグ',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='バナーテーブル';

-- --------------------------------------------------------

--
-- テーブルの構造 `t_cli_agency`
--

CREATE TABLE IF NOT EXISTS `t_cli_agency` (
`f_cli_id` int(11) unsigned NOT NULL COMMENT 'CLIID',
  `f_start_time` datetime DEFAULT NULL COMMENT '開始時刻',
  `f_end_time` datetime DEFAULT NULL COMMENT '終了時刻',
  `f_cli_kbn` char(1) DEFAULT NULL,
  `f_status` char(1) DEFAULT NULL COMMENT 'ステータス',
  `f_message` text COMMENT 'メッセージ',
  `f_del_flg` char(1) NOT NULL COMMENT '削除フラグ',
  `f_reg_account` int(8) DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='業者取込情報テーブル';

-- --------------------------------------------------------

--
-- テーブルの構造 `t_display`
--

CREATE TABLE IF NOT EXISTS `t_display` (
`f_display_id` int(8) unsigned NOT NULL COMMENT 'ID',
  `f_greeting_content` text COMMENT 'ご挨拶内容',
  `f_main_company_name` varchar(100) DEFAULT NULL COMMENT '会長会社名',
  `f_main_company_url` varchar(255) DEFAULT NULL COMMENT '会長会社URL',
  `f_chairman_ship` varchar(100) DEFAULT NULL COMMENT '会長役職',
  `f_president_name` varchar(100) DEFAULT NULL COMMENT '会長名',
  `f_president_kana` varchar(100) DEFAULT NULL COMMENT '会長名カナ',
  `f_president_image` varchar(255) DEFAULT NULL COMMENT '会長メイン画像',
  `f_president_list_image` varchar(255) DEFAULT NULL COMMENT '会長一覧画像',
  `f_sign_image` varchar(255) DEFAULT NULL COMMENT '会長サイン画像',
  `f_pdf_file` varchar(255) DEFAULT NULL COMMENT '会則PDFアップ',
  `f_pdf_name` varchar(100) DEFAULT NULL COMMENT '会則PDF名',
  `f_pdf_size1` varchar(10) DEFAULT NULL COMMENT 'PDFサイズ1',
  `f_pdf_directory` varchar(255) DEFAULT NULL COMMENT '名簿PDFアップ',
  `f_pdf_directory_name` varchar(100) DEFAULT NULL COMMENT '名簿PDF名',
  `f_pdf_size2` varchar(10) DEFAULT NULL COMMENT 'PDFサイズ2',
  `f_title1` varchar(100) DEFAULT NULL COMMENT '役職1',
  `f_area1` varchar(100) DEFAULT NULL COMMENT 'エリア1',
  `f_name1` varchar(100) DEFAULT NULL COMMENT '氏名1',
  `f_kana_name1` varchar(100) DEFAULT NULL COMMENT '氏名カナ1',
  `f_company_title1` varchar(100) DEFAULT NULL COMMENT '会社役職1',
  `f_company_name1` varchar(100) DEFAULT NULL COMMENT '社名1',
  `f_company_url1` varchar(255) DEFAULT NULL COMMENT '会社URL1',
  `f_image1` varchar(255) DEFAULT NULL COMMENT '画像1',
  `f_title2` varchar(100) DEFAULT NULL COMMENT '役職2',
  `f_area2` varchar(100) DEFAULT NULL COMMENT 'エリア2',
  `f_name2` varchar(100) DEFAULT NULL COMMENT '氏名2',
  `f_kana_name2` varchar(100) DEFAULT NULL COMMENT '氏名カナ2',
  `f_company_title2` varchar(100) DEFAULT NULL COMMENT '会社役職2',
  `f_company_name2` varchar(100) DEFAULT NULL COMMENT '社名2',
  `f_company_url2` varchar(255) DEFAULT NULL COMMENT '会社URL2',
  `f_image2` varchar(255) DEFAULT NULL COMMENT '画像2',
  `f_title3` varchar(100) DEFAULT NULL COMMENT '役職3',
  `f_area3` varchar(100) DEFAULT NULL COMMENT 'エリア3',
  `f_name3` varchar(100) DEFAULT NULL COMMENT '氏名3',
  `f_kana_name3` varchar(100) DEFAULT NULL COMMENT '氏名カナ3',
  `f_company_title3` varchar(100) DEFAULT NULL COMMENT '会社役職3',
  `f_company_name3` varchar(100) DEFAULT NULL COMMENT '社名3',
  `f_company_url3` varchar(255) DEFAULT NULL COMMENT '会社URL3',
  `f_image3` varchar(255) DEFAULT NULL COMMENT '画像3',
  `f_title4` varchar(100) DEFAULT NULL COMMENT '役職4',
  `f_area4` varchar(100) DEFAULT NULL COMMENT 'エリア4',
  `f_name4` varchar(100) DEFAULT NULL COMMENT '氏名4',
  `f_kana_name4` varchar(100) DEFAULT NULL COMMENT '氏名カナ4',
  `f_company_title4` varchar(100) DEFAULT NULL COMMENT '会社役職4',
  `f_company_name4` varchar(100) DEFAULT NULL COMMENT '社名4',
  `f_company_url4` varchar(255) DEFAULT NULL COMMENT '会社URL4',
  `f_image4` varchar(255) DEFAULT NULL COMMENT '画像4',
  `f_title5` varchar(100) DEFAULT NULL COMMENT '役職5',
  `f_area5` varchar(100) DEFAULT NULL COMMENT 'エリア5',
  `f_name5` varchar(100) DEFAULT NULL COMMENT '氏名5',
  `f_kana_name5` varchar(100) DEFAULT NULL COMMENT '氏名カナ5',
  `f_company_title5` varchar(100) DEFAULT NULL COMMENT '会社役職5',
  `f_company_name5` varchar(100) DEFAULT NULL COMMENT '社名5',
  `f_company_url5` varchar(255) DEFAULT NULL COMMENT '会社URL5',
  `f_image5` varchar(255) DEFAULT NULL COMMENT '画像5',
  `f_title6` varchar(100) DEFAULT NULL COMMENT '役職6',
  `f_area6` varchar(100) DEFAULT NULL COMMENT 'エリア6',
  `f_name6` varchar(100) DEFAULT NULL COMMENT '氏名6',
  `f_kana_name6` varchar(100) DEFAULT NULL COMMENT '氏名カナ6',
  `f_company_title6` varchar(100) DEFAULT NULL COMMENT '会社役職6',
  `f_company_name6` varchar(100) DEFAULT NULL COMMENT '社名6',
  `f_company_url6` varchar(255) DEFAULT NULL COMMENT '会社URL6',
  `f_image6` varchar(255) DEFAULT NULL COMMENT '画像6',
  `f_title7` varchar(100) DEFAULT NULL COMMENT '役職7',
  `f_area7` varchar(100) DEFAULT NULL COMMENT 'エリア7',
  `f_name7` varchar(100) DEFAULT NULL COMMENT '氏名7',
  `f_kana_name7` varchar(100) DEFAULT NULL COMMENT '氏名カナ7',
  `f_company_title7` varchar(100) DEFAULT NULL COMMENT '会社役職7',
  `f_company_name7` varchar(100) DEFAULT NULL COMMENT '社名7',
  `f_company_url7` varchar(255) DEFAULT NULL COMMENT '会社URL7',
  `f_image7` varchar(255) DEFAULT NULL COMMENT '画像7',
  `f_title8` varchar(100) DEFAULT NULL COMMENT '役職8',
  `f_area8` varchar(100) DEFAULT NULL COMMENT 'エリア8',
  `f_name8` varchar(100) DEFAULT NULL COMMENT '氏名8',
  `f_kana_name8` varchar(100) DEFAULT NULL COMMENT '氏名カナ8',
  `f_company_title8` varchar(100) DEFAULT NULL COMMENT '会社役職8',
  `f_company_name8` varchar(100) DEFAULT NULL COMMENT '社名8',
  `f_company_url8` varchar(255) DEFAULT NULL COMMENT '会社URL8',
  `f_image8` varchar(255) DEFAULT NULL COMMENT '画像8',
  `f_title9` varchar(100) DEFAULT NULL COMMENT '役職9',
  `f_area9` varchar(100) DEFAULT NULL COMMENT 'エリア9',
  `f_name9` varchar(100) DEFAULT NULL COMMENT '氏名9',
  `f_kana_name9` varchar(100) DEFAULT NULL COMMENT '氏名カナ9',
  `f_company_title9` varchar(100) DEFAULT NULL COMMENT '会社役職9',
  `f_company_name9` varchar(100) DEFAULT NULL COMMENT '社名9',
  `f_company_url9` varchar(100) DEFAULT NULL COMMENT '会社URL9',
  `f_image9` varchar(255) DEFAULT NULL COMMENT '画像9',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='表示用テーブル';

-- --------------------------------------------------------

--
-- テーブルの構造 `t_document`
--

CREATE TABLE IF NOT EXISTS `t_document` (
`f_document_id` int(8) unsigned NOT NULL COMMENT '資料ID',
  `f_title` varchar(255) DEFAULT NULL COMMENT '資料タイトル',
  `f_content` text COMMENT '資料内容',
  `f_category` char(1) DEFAULT NULL COMMENT 'カテゴリ',
  `f_disp_flg` char(1) DEFAULT NULL COMMENT '表示フラグ',
  `f_keyword_ids` text COMMENT 'キーワード',
  `f_new_arrival_flg` char(1) DEFAULT NULL COMMENT '新着フラグ',
  `f_new_arrival_start_date` date DEFAULT NULL COMMENT '新着開始日',
  `f_new_arrival_end_date` date DEFAULT NULL COMMENT '新着終了日',
  `f_filename` varchar(255) DEFAULT NULL COMMENT 'ファイル名前',
  `f_file` varchar(255) DEFAULT NULL COMMENT 'ファイルパス',
  `f_extension` varchar(50) DEFAULT NULL COMMENT 'ファイル拡張子',
  `f_file_size` varchar(10) DEFAULT NULL COMMENT 'ファイルサイズ',
  `f_download_name` varchar(255) DEFAULT NULL COMMENT 'ファイルダウンロード名前',
  `f_order` int(8) unsigned DEFAULT NULL COMMENT '表示順',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='資料テーブル';

-- --------------------------------------------------------

--
-- テーブルの構造 `t_gnavi`
--

CREATE TABLE IF NOT EXISTS `t_gnavi` (
`f_id` int(8) unsigned NOT NULL COMMENT 'ページID',
  `f_page_name` varchar(255) DEFAULT NULL COMMENT 'ページ名前',
  `f_kbn` char(1) DEFAULT NULL COMMENT '区分',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='Gナビ閲覧設定';

-- --------------------------------------------------------

--
-- テーブルの構造 `t_inquiry`
--

CREATE TABLE IF NOT EXISTS `t_inquiry` (
`f_inquiry_id` int(8) unsigned NOT NULL COMMENT 'お問合せID',
  `f_id` varchar(20) DEFAULT NULL COMMENT 'ID',
  `f_incharge` varchar(200) DEFAULT NULL COMMENT '担当者',
  `f_name` varchar(200) DEFAULT NULL COMMENT '業者名',
  `f_agency_type` char(1) DEFAULT NULL COMMENT '属性',
  `f_branch_cd` int(3) unsigned DEFAULT NULL COMMENT '支部',
  `f_agency_cd` int(3) unsigned DEFAULT NULL COMMENT '業者',
  `f_sales_cd` int(3) unsigned DEFAULT NULL COMMENT '営業所',
  `f_zip` varchar(32) DEFAULT NULL COMMENT '郵便番号',
  `f_prefecture_id` int(2) unsigned DEFAULT NULL COMMENT '都道府県',
  `f_town` varchar(256) DEFAULT NULL COMMENT '市区町村',
  `f_address` varchar(256) DEFAULT NULL COMMENT '住所',
  `f_tel` varchar(20) DEFAULT NULL COMMENT '電話番号',
  `f_fax` varchar(20) DEFAULT NULL COMMENT 'ＦＡＸ',
  `f_mailaddress` varchar(255) DEFAULT NULL COMMENT 'メールアドレス',
  `f_content` text COMMENT 'お問合せ内容',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='お問合せ情報テーブル';

-- --------------------------------------------------------

--
-- テーブルの構造 `t_keyword`
--

CREATE TABLE IF NOT EXISTS `t_keyword` (
`f_keyword_id` int(8) unsigned NOT NULL COMMENT 'キーワードID',
  `f_keyword` varchar(255) DEFAULT NULL COMMENT 'キーワードテキスト',
  `f_disp_flg` char(1) DEFAULT NULL COMMENT '表示フラグ',
  `f_order` int(8) unsigned DEFAULT NULL COMMENT '表示順',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='キーワードテーブル';

-- --------------------------------------------------------

--
-- テーブルの構造 `t_magazine`
--

CREATE TABLE IF NOT EXISTS `t_magazine` (
`f_magazine_id` int(8) unsigned NOT NULL COMMENT '会報誌ID',
  `f_title` varchar(200) DEFAULT NULL COMMENT 'タイトル',
  `f_magazine_issue_no` varchar(200) DEFAULT NULL COMMENT 'マガジン課題番号',
  `f_disp_date_from` datetime DEFAULT NULL COMMENT '開始日付',
  `f_disp_date_to` date DEFAULT NULL COMMENT '終了日付',
  `f_disp_date` date DEFAULT NULL COMMENT '表示日付',
  `f_disp_flg` char(1) DEFAULT NULL COMMENT '表示フラグ',
  `f_content` text COMMENT '備考',
  `f_image` varchar(255) DEFAULT NULL COMMENT '画像',
  `f_thumb` varchar(255) DEFAULT NULL COMMENT 'サムネイル',
  `f_filename` varchar(255) DEFAULT NULL COMMENT 'ファイル名前',
  `f_file` varchar(255) DEFAULT NULL COMMENT 'リンクダウンロード資料',
  `f_extension` varchar(50) DEFAULT NULL COMMENT 'ファイルタイプ',
  `f_file_size` varchar(10) DEFAULT NULL COMMENT 'ファイルサイズ',
  `f_download_name` varchar(255) DEFAULT NULL COMMENT 'ダウンロードファイル名前',
  `f_view_level` char(1) DEFAULT NULL COMMENT '閲覧レベル',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='会報誌テーブル';

-- --------------------------------------------------------

--
-- テーブルの構造 `t_magazine_detail`
--

CREATE TABLE IF NOT EXISTS `t_magazine_detail` (
`f_magazine_detail_id` int(8) unsigned NOT NULL COMMENT '会報誌ページ見出しID',
  `f_magazine_id` int(8) unsigned NOT NULL COMMENT '会報誌ID',
  `f_title` varchar(200) DEFAULT NULL COMMENT 'タイトル',
  `f_filename_1` varchar(255) DEFAULT NULL COMMENT 'ファイル名前1',
  `f_filename_2` varchar(255) DEFAULT NULL COMMENT 'ファイル名前2',
  `f_filename_3` varchar(255) DEFAULT NULL COMMENT 'ファイル名前3',
  `f_filename_4` varchar(255) DEFAULT NULL COMMENT 'ファイル名前4',
  `f_filename_5` varchar(255) DEFAULT NULL COMMENT 'ファイル名前5',
  `f_file_1` varchar(255) DEFAULT NULL COMMENT 'リンクダウンロード資料1',
  `f_file_2` varchar(255) DEFAULT NULL COMMENT 'リンクダウンロード資料2',
  `f_file_3` varchar(255) DEFAULT NULL COMMENT 'リンクダウンロード資料3',
  `f_file_4` varchar(255) DEFAULT NULL COMMENT 'リンクダウンロード資料4',
  `f_file_5` varchar(255) DEFAULT NULL COMMENT 'リンクダウンロード資料5',
  `f_extension_1` varchar(50) DEFAULT NULL COMMENT 'ファイルタイプ1',
  `f_extension_2` varchar(50) DEFAULT NULL COMMENT 'ファイルタイプ2',
  `f_extension_3` varchar(50) DEFAULT NULL COMMENT 'ファイルタイプ3',
  `f_extension_4` varchar(50) DEFAULT NULL COMMENT 'ファイルタイプ4',
  `f_extension_5` varchar(50) DEFAULT NULL COMMENT 'ファイルタイプ5',
  `f_file_size_1` varchar(10) DEFAULT NULL COMMENT 'ファイルサイズ1',
  `f_file_size_2` varchar(10) DEFAULT NULL COMMENT 'ファイルサイズ1',
  `f_file_size_3` varchar(10) DEFAULT NULL COMMENT 'ファイルサイズ1',
  `f_file_size_4` varchar(10) DEFAULT NULL COMMENT 'ファイルサイズ1',
  `f_file_size_5` varchar(10) DEFAULT NULL COMMENT 'ファイルサイズ1',
  `f_content_1` text COMMENT '備考1',
  `f_content_2` text COMMENT '備考2',
  `f_content_3` text COMMENT '備考3',
  `f_content_4` text COMMENT '備考4',
  `f_content_5` text COMMENT '備考5',
  `f_download_name_1` varchar(255) DEFAULT NULL COMMENT 'ダウンロードファイル名前1',
  `f_download_name_2` varchar(255) DEFAULT NULL COMMENT 'ダウンロードファイル名前2',
  `f_download_name_3` varchar(255) DEFAULT NULL COMMENT 'ダウンロードファイル名前3',
  `f_download_name_4` varchar(255) DEFAULT NULL COMMENT 'ダウンロードファイル名前4',
  `f_download_name_5` varchar(255) DEFAULT NULL COMMENT 'ダウンロードファイル名前5',
  `f_order` int(8) unsigned DEFAULT NULL COMMENT '表示順',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='会報誌ページ見出しテーブル';

-- --------------------------------------------------------

--
-- テーブルの構造 `t_mimiyori`
--

CREATE TABLE IF NOT EXISTS `t_mimiyori` (
`f_mimiyori_id` int(8) unsigned NOT NULL COMMENT '耳寄りID',
  `f_intro_type` char(1) DEFAULT NULL COMMENT '紹介のカテゴリ',
  `f_acceptor_name` varchar(200) DEFAULT NULL COMMENT '紹介者名',
  `f_provider_name` varchar(200) DEFAULT NULL COMMENT '提供者名',
  `f_acceptor_name_kana` varchar(200) DEFAULT NULL COMMENT '紹介者名（かな）',
  `f_provider_name_kana` varchar(200) DEFAULT NULL COMMENT '提供者名（かな）',
  `f_acceptor_zip` varchar(32) DEFAULT NULL COMMENT '紹介者の郵便番号',
  `f_provider_zip` varchar(32) DEFAULT NULL COMMENT '提供者の電話番号',
  `f_acceptor_prefecture_id` int(2) unsigned DEFAULT NULL COMMENT '紹介者の都道府県',
  `f_provider_prefecture_id` int(2) unsigned DEFAULT NULL COMMENT '提供者の都道府県',
  `f_acceptor_town` varchar(256) DEFAULT NULL COMMENT '紹介者の市区町村',
  `f_provider_town` varchar(256) DEFAULT NULL COMMENT '提供者の市区町村',
  `f_acceptor_address` varchar(256) DEFAULT NULL COMMENT '紹介者の住所',
  `f_provider_address` varchar(256) DEFAULT NULL COMMENT '提供者の住所',
  `f_acceptor_tel` varchar(20) DEFAULT NULL COMMENT '紹介者の電話番号',
  `f_provider_tel` varchar(20) DEFAULT NULL COMMENT '提供者の電話番号',
  `f_acceptor_time_from` varchar(5) DEFAULT NULL COMMENT '開始時間',
  `f_provider_time_from` varchar(5) DEFAULT NULL COMMENT '提供者の開始時間',
  `f_acceptor_time_to` varchar(5) DEFAULT NULL COMMENT '終了時間',
  `f_provider_time_to` varchar(5) DEFAULT NULL COMMENT '提供者の終了時間',
  `f_acceptor_mailaddress` varchar(255) DEFAULT NULL COMMENT '紹介者のメールアドレス',
  `f_provider_mailaddress` varchar(255) DEFAULT NULL COMMENT '提供者のメールアドレス',
  `f_acceptor_content` text COMMENT '紹介者のお問合せ内容',
  `f_provider_content` text COMMENT '提供者のお問合せ内容',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='耳寄り情報テーブル';

-- --------------------------------------------------------

--
-- テーブルの構造 `t_movie`
--

CREATE TABLE IF NOT EXISTS `t_movie` (
`f_movie_id` int(8) unsigned NOT NULL COMMENT '動画ID',
  `f_title` varchar(255) DEFAULT NULL COMMENT '動画タイトル',
  `f_property` varchar(255) DEFAULT NULL COMMENT '動画プロパティ',
  `f_category` char(1) DEFAULT NULL COMMENT '動画カテゴリ',
  `f_description` text COMMENT '動画説明',
  `f_disp_date_from` datetime DEFAULT NULL COMMENT '開始日付',
  `f_view_level` char(1) DEFAULT NULL COMMENT '閲覧レベル',
  `f_disp_flg` char(1) DEFAULT NULL COMMENT '表示フラグ',
  `f_new_arrival_flg` char(1) DEFAULT NULL COMMENT '新着フラグ',
  `f_new_arrival_start_date` date DEFAULT NULL COMMENT '新着日付開始',
  `f_new_arrival_end_date` date DEFAULT NULL COMMENT '新着日付終了',
  `f_movie_filename` varchar(255) DEFAULT NULL COMMENT '動画ファイル名',
  `f_video_name` varchar(255) DEFAULT NULL COMMENT '動画',
  `f_image` varchar(255) DEFAULT NULL COMMENT '画像',
  `f_thumb` varchar(255) DEFAULT NULL COMMENT 'サムネイル',
  `t_movie_keyword_ids` varchar(255) DEFAULT NULL,
  `f_order` int(8) unsigned DEFAULT NULL COMMENT '表示順',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='動画テーブル';

-- --------------------------------------------------------

--
-- テーブルの構造 `t_new_arrival`
--

CREATE TABLE IF NOT EXISTS `t_new_arrival` (
`f_new_arrival_id` int(8) unsigned NOT NULL COMMENT '新着ID',
  `f_new_arrival_sub_id` int(8) NOT NULL,
  `f_list_title` varchar(200) DEFAULT NULL COMMENT '一覧表示タイトル',
  `f_category` char(1) DEFAULT NULL COMMENT 'カテゴリ',
  `f_page_setting_kbn` char(1) DEFAULT NULL COMMENT '遷移区分',
  `f_url` varchar(256) DEFAULT NULL COMMENT 'リンク遷移先ＵＲＬ',
  `f_url_target` char(1) DEFAULT NULL COMMENT 'リンクターゲット',
  `f_disp_date_from` datetime DEFAULT NULL COMMENT '開始日付',
  `f_disp_date_to` datetime DEFAULT NULL COMMENT '終了日付',
  `f_disp_date` date DEFAULT NULL COMMENT '一覧表示日付',
  `f_open_flg` char(1) DEFAULT NULL COMMENT '公開・非公開',
  `f_new_flg` char(1) NOT NULL DEFAULT '0',
  `f_new_flg_days` int(8) unsigned NOT NULL,
  `f_content` text COMMENT 'エディタ',
  `f_filename` varchar(255) DEFAULT NULL COMMENT 'ファイル名前',
  `f_file` varchar(255) DEFAULT NULL COMMENT 'リンクダウンロード資料',
  `f_extension` varchar(50) DEFAULT NULL COMMENT 'ファイルタイプ',
  `f_file_size` varchar(10) DEFAULT NULL COMMENT 'ファイルサイズ',
  `f_download_name` varchar(255) DEFAULT NULL COMMENT 'ダウンロードファイル名前',
  `f_view_level` char(1) DEFAULT NULL COMMENT '閲覧レベル',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='新着テーブル';

-- --------------------------------------------------------

--
-- テーブルの構造 `t_new_arrival_file`
--

CREATE TABLE IF NOT EXISTS `t_new_arrival_file` (
`f_file_id` int(8) unsigned NOT NULL COMMENT 'ファイルID',
  `f_new_arrival_id` int(8) unsigned NOT NULL COMMENT '新着ID',
  `f_filename` varchar(255) DEFAULT NULL COMMENT 'ファイル名前',
  `f_file` varchar(255) DEFAULT NULL COMMENT 'リンクダウンロード資料',
  `f_extension` varchar(50) DEFAULT NULL COMMENT 'ファイルタイプ',
  `f_file_size` varchar(10) DEFAULT NULL COMMENT 'ファイルサイズ',
  `f_download_name` varchar(255) DEFAULT NULL COMMENT 'ダウンロードファイル名前',
  `f_order` int(8) unsigned DEFAULT NULL COMMENT '表示順',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='新着ファイルテーブル';

-- --------------------------------------------------------

--
-- テーブルの構造 `t_new_arrival_image`
--

CREATE TABLE IF NOT EXISTS `t_new_arrival_image` (
`f_image_id` int(8) unsigned NOT NULL COMMENT '画像ID',
  `f_image` varchar(255) DEFAULT NULL COMMENT '画像',
  `f_thumb` varchar(255) DEFAULT NULL COMMENT 'サムネイル',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='新着画像テーブル';

-- --------------------------------------------------------

--
-- テーブルの構造 `t_page_content`
--

CREATE TABLE IF NOT EXISTS `t_page_content` (
`f_page_content_id` int(8) unsigned NOT NULL COMMENT 'ページコンテンツID',
  `f_page_content_sub_id` int(8) NOT NULL COMMENT 'sub ID',
  `f_page_title` varchar(200) DEFAULT NULL COMMENT 'ページタイトル',
  `f_current_position` varchar(1) DEFAULT NULL COMMENT '階層選択',
  `f_parent_position` varchar(20) DEFAULT NULL COMMENT '上位ページ選択',
  `f_slag` varchar(100) DEFAULT NULL COMMENT 'スラッグ設定',
  `f_disp_date_from` datetime DEFAULT NULL COMMENT '開始日付',
  `f_disp_date_to` datetime DEFAULT NULL COMMENT '終了日付',
  `f_open_flg` char(1) DEFAULT NULL COMMENT '公開・非公開',
  `f_new_flg` char(1) NOT NULL DEFAULT '0',
  `f_new_flg_days` int(8) unsigned NOT NULL,
  `f_individual_chk` char(1) DEFAULT NULL COMMENT '独立ページ',
  `f_content` text COMMENT 'エディタ',
  `f_view_level` char(1) DEFAULT NULL COMMENT '閲覧レベル',
  `f_order` int(8) unsigned DEFAULT NULL COMMENT '表示順',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='ページコンテンツテーブル';

-- --------------------------------------------------------

--
-- テーブルの構造 `t_page_content_file`
--

CREATE TABLE IF NOT EXISTS `t_page_content_file` (
`f_file_id` int(8) unsigned NOT NULL COMMENT 'ファイルID',
  `f_page_content_id` int(8) unsigned NOT NULL COMMENT 'ページコンテンツID',
  `f_filename` varchar(255) DEFAULT NULL COMMENT 'ファイル名前',
  `f_file` varchar(255) DEFAULT NULL COMMENT 'リンクダウンロード資料',
  `f_extension` varchar(50) DEFAULT NULL COMMENT 'ファイルタイプ',
  `f_file_size` varchar(10) DEFAULT NULL COMMENT 'ファイルサイズ',
  `f_download_name` varchar(255) DEFAULT NULL COMMENT 'ダウンロードファイル名前',
  `f_order` int(8) unsigned DEFAULT NULL COMMENT '表示順',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='ページコンテンツファイルテーブル';

-- --------------------------------------------------------

--
-- テーブルの構造 `t_page_content_image`
--

CREATE TABLE IF NOT EXISTS `t_page_content_image` (
`f_image_id` int(8) unsigned NOT NULL COMMENT '画像ID',
  `f_image` varchar(255) DEFAULT NULL COMMENT '画像',
  `f_thumb` varchar(255) DEFAULT NULL COMMENT 'サムネイル',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='ページコンテンツ画像テーブル';

-- --------------------------------------------------------

--
-- テーブルの構造 `t_push_reservation`
--

CREATE TABLE IF NOT EXISTS `t_push_reservation` (
`f_push_reserve_id` int(8) unsigned NOT NULL COMMENT 'プッシュ通知予約ID',
  `f_page_record_id` int(8) DEFAULT NULL COMMENT 'ページ記録ID',
  `f_type` char(1) DEFAULT NULL COMMENT '種別',
  `f_heading` varchar(200) DEFAULT NULL COMMENT '見出し',
  `f_content` text COMMENT '内容',
  `f_delivary_start_time` datetime DEFAULT NULL COMMENT '配信開始時間',
  `f_account` varchar(256) DEFAULT NULL COMMENT 'アカウント',
  `f_view_authority` varchar(256) DEFAULT NULL COMMENT '閲覧権限',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='プッシュ予約テーブル';

-- --------------------------------------------------------

--
-- テーブルの構造 `t_top`
--

CREATE TABLE IF NOT EXISTS `t_top` (
  `f_top_id` int(8) unsigned NOT NULL COMMENT 'ID',
  `f_image1` varchar(255) DEFAULT NULL COMMENT '画像1',
  `f_thumb1` varchar(255) DEFAULT NULL COMMENT 'サムネイル1',
  `f_image_alt1` varchar(255) DEFAULT NULL COMMENT '画像ALT1',
  `f_url1` varchar(255) DEFAULT NULL COMMENT 'リンクURL1',
  `f_url_kbn1` char(1) DEFAULT NULL COMMENT 'URL区分1',
  `f_sort_no1` int(8) unsigned DEFAULT NULL COMMENT '表示順',
  `f_image2` varchar(255) DEFAULT NULL COMMENT '画像2',
  `f_thumb2` varchar(255) DEFAULT NULL COMMENT 'サムネイル2',
  `f_image_alt2` varchar(255) DEFAULT NULL COMMENT '画像ALT2',
  `f_url2` varchar(255) DEFAULT NULL COMMENT 'リンクURL2',
  `f_url_kbn2` char(1) DEFAULT NULL COMMENT 'URL区分2',
  `f_sort_no2` int(8) unsigned DEFAULT NULL COMMENT '表示順',
  `f_image3` varchar(255) DEFAULT NULL COMMENT '画像3',
  `f_thumb3` varchar(255) DEFAULT NULL COMMENT 'サムネイル3',
  `f_image_alt3` varchar(255) DEFAULT NULL COMMENT '画像ALT3',
  `f_url3` varchar(255) DEFAULT NULL COMMENT 'リンクURL3',
  `f_url_kbn3` char(1) DEFAULT NULL COMMENT 'URL区分3',
  `f_sort_no3` int(8) unsigned DEFAULT NULL COMMENT '表示順',
  `f_image4` varchar(255) DEFAULT NULL COMMENT '画像4',
  `f_thumb4` varchar(255) DEFAULT NULL COMMENT 'サムネイル4',
  `f_image_alt4` varchar(255) DEFAULT NULL COMMENT '画像ALT4',
  `f_url4` varchar(255) DEFAULT NULL COMMENT 'リンクURL4',
  `f_url_kbn4` char(1) DEFAULT NULL COMMENT 'URL区分4',
  `f_sort_no4` int(8) unsigned DEFAULT NULL COMMENT '表示順',
  `f_image5` varchar(255) DEFAULT NULL COMMENT '画像5',
  `f_thumb5` varchar(255) DEFAULT NULL COMMENT 'サムネイル5',
  `f_image_alt5` varchar(255) DEFAULT NULL COMMENT '画像ALT5',
  `f_url5` varchar(255) DEFAULT NULL COMMENT 'リンクURL5',
  `f_url_kbn5` char(1) DEFAULT NULL COMMENT 'URL区分5',
  `f_sort_no5` int(8) unsigned DEFAULT NULL COMMENT '表示順',
  `f_image6` varchar(255) DEFAULT NULL COMMENT '画像6',
  `f_thumb6` varchar(255) DEFAULT NULL COMMENT 'サムネイル6',
  `f_image_alt6` varchar(255) DEFAULT NULL COMMENT '画像ALT6',
  `f_url6` varchar(255) DEFAULT NULL COMMENT 'リンクURL6',
  `f_url_kbn6` char(1) DEFAULT NULL COMMENT 'URL区分6',
  `f_sort_no6` int(8) unsigned DEFAULT NULL COMMENT '表示順',
  `f_disp_flg` char(1) DEFAULT NULL COMMENT '表示フラグ',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='トップテーブル';

-- --------------------------------------------------------

--
-- テーブルの構造 `t_top_link`
--

CREATE TABLE IF NOT EXISTS `t_top_link` (
`f_top_link_id` int(8) unsigned NOT NULL COMMENT 'トップリンクID',
  `f_title` varchar(200) DEFAULT NULL COMMENT 'タイトル',
  `f_url` varchar(256) DEFAULT NULL COMMENT 'リンク遷移先ＵＲＬ',
  `f_url_target` char(1) DEFAULT NULL COMMENT 'リンクターゲット',
  `f_description` varchar(200) DEFAULT NULL COMMENT '説明',
  `f_open_flg` char(1) DEFAULT NULL COMMENT '公開・非公開',
  `f_order` int(8) unsigned DEFAULT NULL COMMENT '表示順',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) unsigned DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) unsigned DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='トップ画面リンクテーブル';

-- --------------------------------------------------------

--
-- テーブルの構造 `t_user_password_remind`
--

CREATE TABLE IF NOT EXISTS `t_user_password_remind` (
`f_remind_id` int(8) NOT NULL COMMENT 'リマインダーID',
  `f_agency_id` int(8) DEFAULT NULL COMMENT '業者ID',
  `f_ref_key` varchar(100) NOT NULL COMMENT 'キー',
  `f_valid_time` datetime DEFAULT NULL COMMENT '有効時間',
  `f_del_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `f_reg_account` int(8) DEFAULT NULL COMMENT '登録者',
  `f_reg_time` datetime DEFAULT NULL COMMENT '登録日時',
  `f_upd_account` int(8) DEFAULT NULL COMMENT '更新者',
  `f_upd_time` datetime DEFAULT NULL COMMENT '更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='ユーザーパスワードリマインダーテーブル';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_admin`
--
ALTER TABLE `m_admin`
 ADD PRIMARY KEY (`f_admin_id`);

--
-- Indexes for table `m_agency`
--
ALTER TABLE `m_agency`
 ADD PRIMARY KEY (`f_agency_id`), ADD KEY `f_id` (`f_id`);

--
-- Indexes for table `m_base`
--
ALTER TABLE `m_base`
 ADD PRIMARY KEY (`f_base_id`);

--
-- Indexes for table `m_industry`
--
ALTER TABLE `m_industry`
 ADD PRIMARY KEY (`f_industry_id`);

--
-- Indexes for table `m_occupation`
--
ALTER TABLE `m_occupation`
 ADD PRIMARY KEY (`f_occupation_id`);

--
-- Indexes for table `m_prefecture`
--
ALTER TABLE `m_prefecture`
 ADD PRIMARY KEY (`f_prefecture_id`);

--
-- Indexes for table `m_siten`
--
ALTER TABLE `m_siten`
 ADD PRIMARY KEY (`f_siten_id`);

--
-- Indexes for table `m_zip`
--
ALTER TABLE `m_zip`
 ADD PRIMARY KEY (`f_zip_id`), ADD KEY `f_zip_cd` (`f_zip_cd`);

--
-- Indexes for table `t_about`
--
ALTER TABLE `t_about`
 ADD PRIMARY KEY (`f_about_id`);

--
-- Indexes for table `t_agency_login_history`
--
ALTER TABLE `t_agency_login_history`
 ADD PRIMARY KEY (`f_agency_login_history_id`);

--
-- Indexes for table `t_banner`
--
ALTER TABLE `t_banner`
 ADD PRIMARY KEY (`f_banner_id`);

--
-- Indexes for table `t_cli_agency`
--
ALTER TABLE `t_cli_agency`
 ADD PRIMARY KEY (`f_cli_id`);

--
-- Indexes for table `t_display`
--
ALTER TABLE `t_display`
 ADD PRIMARY KEY (`f_display_id`);

--
-- Indexes for table `t_document`
--
ALTER TABLE `t_document`
 ADD PRIMARY KEY (`f_document_id`);

--
-- Indexes for table `t_gnavi`
--
ALTER TABLE `t_gnavi`
 ADD PRIMARY KEY (`f_id`);

--
-- Indexes for table `t_inquiry`
--
ALTER TABLE `t_inquiry`
 ADD PRIMARY KEY (`f_inquiry_id`);

--
-- Indexes for table `t_keyword`
--
ALTER TABLE `t_keyword`
 ADD PRIMARY KEY (`f_keyword_id`);

--
-- Indexes for table `t_magazine`
--
ALTER TABLE `t_magazine`
 ADD PRIMARY KEY (`f_magazine_id`);

--
-- Indexes for table `t_magazine_detail`
--
ALTER TABLE `t_magazine_detail`
 ADD PRIMARY KEY (`f_magazine_detail_id`), ADD KEY `f_new_arrival_id` (`f_magazine_id`);

--
-- Indexes for table `t_mimiyori`
--
ALTER TABLE `t_mimiyori`
 ADD PRIMARY KEY (`f_mimiyori_id`);

--
-- Indexes for table `t_movie`
--
ALTER TABLE `t_movie`
 ADD PRIMARY KEY (`f_movie_id`);

--
-- Indexes for table `t_new_arrival`
--
ALTER TABLE `t_new_arrival`
 ADD PRIMARY KEY (`f_new_arrival_id`);

--
-- Indexes for table `t_new_arrival_file`
--
ALTER TABLE `t_new_arrival_file`
 ADD PRIMARY KEY (`f_file_id`), ADD KEY `f_new_arrival_id` (`f_new_arrival_id`);

--
-- Indexes for table `t_new_arrival_image`
--
ALTER TABLE `t_new_arrival_image`
 ADD PRIMARY KEY (`f_image_id`);

--
-- Indexes for table `t_page_content`
--
ALTER TABLE `t_page_content`
 ADD PRIMARY KEY (`f_page_content_id`);

--
-- Indexes for table `t_page_content_file`
--
ALTER TABLE `t_page_content_file`
 ADD PRIMARY KEY (`f_file_id`), ADD KEY `f_new_arrival_id` (`f_page_content_id`);

--
-- Indexes for table `t_page_content_image`
--
ALTER TABLE `t_page_content_image`
 ADD PRIMARY KEY (`f_image_id`);

--
-- Indexes for table `t_push_reservation`
--
ALTER TABLE `t_push_reservation`
 ADD PRIMARY KEY (`f_push_reserve_id`);

--
-- Indexes for table `t_top`
--
ALTER TABLE `t_top`
 ADD PRIMARY KEY (`f_top_id`);

--
-- Indexes for table `t_top_link`
--
ALTER TABLE `t_top_link`
 ADD PRIMARY KEY (`f_top_link_id`);

--
-- Indexes for table `t_user_password_remind`
--
ALTER TABLE `t_user_password_remind`
 ADD PRIMARY KEY (`f_remind_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_admin`
--
ALTER TABLE `m_admin`
MODIFY `f_admin_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理者ID';
--
-- AUTO_INCREMENT for table `m_agency`
--
ALTER TABLE `m_agency`
MODIFY `f_agency_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '業者ID';
--
-- AUTO_INCREMENT for table `m_base`
--
ALTER TABLE `m_base`
MODIFY `f_base_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '拠点ID';
--
-- AUTO_INCREMENT for table `m_industry`
--
ALTER TABLE `m_industry`
MODIFY `f_industry_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '業種ID';
--
-- AUTO_INCREMENT for table `m_occupation`
--
ALTER TABLE `m_occupation`
MODIFY `f_occupation_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '職種ID';
--
-- AUTO_INCREMENT for table `m_prefecture`
--
ALTER TABLE `m_prefecture`
MODIFY `f_prefecture_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '都道府県ID';
--
-- AUTO_INCREMENT for table `m_siten`
--
ALTER TABLE `m_siten`
MODIFY `f_siten_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '支店ID';
--
-- AUTO_INCREMENT for table `m_zip`
--
ALTER TABLE `m_zip`
MODIFY `f_zip_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '郵便番号ID';
--
-- AUTO_INCREMENT for table `t_about`
--
ALTER TABLE `t_about`
MODIFY `f_about_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '会報誌ID';
--
-- AUTO_INCREMENT for table `t_agency_login_history`
--
ALTER TABLE `t_agency_login_history`
MODIFY `f_agency_login_history_id` int(8) NOT NULL AUTO_INCREMENT COMMENT '業者ログイン履歴ID';
--
-- AUTO_INCREMENT for table `t_cli_agency`
--
ALTER TABLE `t_cli_agency`
MODIFY `f_cli_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'CLIID';
--
-- AUTO_INCREMENT for table `t_display`
--
ALTER TABLE `t_display`
MODIFY `f_display_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID';
--
-- AUTO_INCREMENT for table `t_document`
--
ALTER TABLE `t_document`
MODIFY `f_document_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '資料ID';
--
-- AUTO_INCREMENT for table `t_gnavi`
--
ALTER TABLE `t_gnavi`
MODIFY `f_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ページID';
--
-- AUTO_INCREMENT for table `t_inquiry`
--
ALTER TABLE `t_inquiry`
MODIFY `f_inquiry_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'お問合せID';
--
-- AUTO_INCREMENT for table `t_keyword`
--
ALTER TABLE `t_keyword`
MODIFY `f_keyword_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'キーワードID';
--
-- AUTO_INCREMENT for table `t_magazine`
--
ALTER TABLE `t_magazine`
MODIFY `f_magazine_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '会報誌ID';
--
-- AUTO_INCREMENT for table `t_magazine_detail`
--
ALTER TABLE `t_magazine_detail`
MODIFY `f_magazine_detail_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '会報誌ページ見出しID';
--
-- AUTO_INCREMENT for table `t_mimiyori`
--
ALTER TABLE `t_mimiyori`
MODIFY `f_mimiyori_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '耳寄りID';
--
-- AUTO_INCREMENT for table `t_movie`
--
ALTER TABLE `t_movie`
MODIFY `f_movie_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '動画ID';
--
-- AUTO_INCREMENT for table `t_new_arrival`
--
ALTER TABLE `t_new_arrival`
MODIFY `f_new_arrival_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '新着ID';
--
-- AUTO_INCREMENT for table `t_new_arrival_file`
--
ALTER TABLE `t_new_arrival_file`
MODIFY `f_file_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ファイルID';
--
-- AUTO_INCREMENT for table `t_new_arrival_image`
--
ALTER TABLE `t_new_arrival_image`
MODIFY `f_image_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '画像ID';
--
-- AUTO_INCREMENT for table `t_page_content`
--
ALTER TABLE `t_page_content`
MODIFY `f_page_content_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ページコンテンツID';
--
-- AUTO_INCREMENT for table `t_page_content_file`
--
ALTER TABLE `t_page_content_file`
MODIFY `f_file_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ファイルID';
--
-- AUTO_INCREMENT for table `t_page_content_image`
--
ALTER TABLE `t_page_content_image`
MODIFY `f_image_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '画像ID';
--
-- AUTO_INCREMENT for table `t_push_reservation`
--
ALTER TABLE `t_push_reservation`
MODIFY `f_push_reserve_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'プッシュ通知予約ID';
--
-- AUTO_INCREMENT for table `t_top_link`
--
ALTER TABLE `t_top_link`
MODIFY `f_top_link_id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'トップリンクID';
--
-- AUTO_INCREMENT for table `t_user_password_remind`
--
ALTER TABLE `t_user_password_remind`
MODIFY `f_remind_id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'リマインダーID';
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
