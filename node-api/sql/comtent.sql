select f_page_content_id, f_parent_position, f_current_position, f_page_title, f_slag, f_disp_date_from, f_disp_date_to, 1 from t_page_content pc
    where
          now() between f_disp_date_from and IFNULL(f_disp_date_to, now())
    and 1=1;