

module.exports.validateEmail =  function (emailAddress) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(emailAddress).toLowerCase());
}

module.exports.alphaNumericCheck =  function (data) {
    let regex = new RegExp("^[a-zA-Z0-9]+$");
    return regex.test(String(data));
}