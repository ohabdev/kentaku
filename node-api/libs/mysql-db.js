var mysqlDB = require('mysql');
const config = require('config');
const util = require('util');
var connDB;


dbconfig = {
    connectionLimit : 40,
    host: config.get("db.host"),
    user: config.get("db.user"),
    password: config.get("db.password"),
    database: config.get("db.database"),
    connectTimeout : 500000,
    port : config.get("db.port")
};

exports.openDB = async function () {
    handleDisconnect();
};



function handleDisconnect() {
    connDB = mysqlDB.createPool(dbconfig);        // Recreate the connection, since the old one cannot be reused.

    connDB.getConnection( function onConnect(err) {           // The server is either down
        if (err) {                                      // or restarting (takes a while sometimes).
            console.log('error when connecting to db:', err);
            setTimeout(handleDisconnect, 10000);        // We introduce a delay before attempting to reconnect,
        }
                                                    // to avoid a hot loop, and to allow our node script to
    });                                                 // process asynchronous requests in the meantime.
                                                        // If you're also serving http, display a 503 error.
    connDB.on('error', function onError(err) {
        console.log('db error', err);
        if (err.code == 'PROTOCOL_CONNECTION_LOST') {   // Connection to the MySQL server is usually
            handleDisconnect();                         // lost due to either server restart, or a
        } else {                                        // connnection idle timeout (the wait_timeout
            throw err;                                  // server variable configures this)
        }
    });
}

exports.excSQL = function (sql, parm, callback) {
    if(mysqlDB.state === "disconnected") {
        console.log("reconnect");
        
        openDB();
    }
    if (callback != undefined) {
        try {
            //console.log({"sql" : sql, "parm" : parm});
            // console.log(connDB.escapeId("=1 or 1=1 545' \" asd`sad -- we", true));
            
            connDB.query(sql, parm, function (err, result) {
                //console.log("-----------");
                if (err) {
                    callback({"rows": [], "len": 0, "status": "error "+ err});
                }
                else {
                    callback({"rows": result, "len": result.length, "status" : "ok"});
                }
            });
        } catch (err) {
            callback({"rows": [], "len": 0, "status": "error "+ err});
            console.log(err);
        }
    }
}



exports.excAsyncSQL1 = function (sql, parm) {
    if(mysqlDB.state === "disconnected") {
        console.log("reconnect");
        
        openDB();
    }
    return new Promise( ( resolve, reject ) => {
        connDB.query( sql, parm, ( err, rows ) => {
            if ( err )
                return reject( err );
            resolve( rows );
        } );
    } );
}





exports.excFieldSQL = function (sql, fields, parm, callback) {
    if(mysqlDB.state === "disconnected") {
        console.log("reconnect");
        
        openDB();
    }
    if (callback != undefined) {
        try {
            console.log({"sql" : sql, "fields": fields, "parms" : parm});
            
            connDB.query(sql, parm, function (err, result) {
                console.log("-----------");
                if (err) {
                    callback({"rows": [], "len": 0, "status": "error "+ err});
                }
                else {
                    callback({"rows": result, "len": result.length, "status" : "ok"});
                }
            });
        } catch (err) {
            callback({"rows": [], "len": 0, "status": "error "+ err});
            console.log(err);
        }
    }
}
exports.closeDB = async function (str, bit) {
    await connDB.release();
    console.log("mysqlDB closed.");
    return 0;
}
