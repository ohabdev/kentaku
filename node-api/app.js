const config = require('config');
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const Entities = require('html-entities').XmlEntities;
const mysqlDB = require("./libs/mysql-db");
const TokenRWClass = require("./libs/TokenRWClass");

var schedule = require('node-schedule');
const fcm = require('./common/sendFCMBatch');

const indexAuthRouter = require('./routes/index');

const homeModule = require('./models/home-model');
const contentModule = require('./models/content-model');
const arrivalModule = require('./models/arrival-model');
const magazineModule = require('./models/magazine-model');
const movieModule = require('./models/movie-model');
const documentModule = require('./models/document-model');
const aboutModule = require('./models/about-model');
const agencyModule = require('./models/agency-model');
const filesModule = require('./models/files-model');
const fcmModule = require('./models/fcm-model');


const agencyAuthRouter = require('./routes/agency');
const homeAuthRouter = require('./routes/home');
const aboutAuthRouter = require('./routes/about');
const contentAuthRouter = require('./routes/content');
const arrivalAuthRouter = require('./routes/arrival');
const movieAuthRouter = require('./routes/movie');
const documentAuthRouter = require('./routes/document');
const magazineAuthRouter = require('./routes/magazine');
const otherAuthRouter = require('./routes/other');
const pagelinkAuthRouter = require('./routes/page-link');
const fsAuthRouter = require('./routes/file-op');
const fcmAuthRouter = require('./routes/fcm');

const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const entities = new Entities();
global.tokens = {};
global.tokenRW = new TokenRWClass();
tokenRW.tokenRead();
mysqlDB.openDB();

agencyModule.init(mysqlDB);
homeModule.init(mysqlDB);
aboutModule.init(mysqlDB);
contentModule.init(mysqlDB);
arrivalModule.init(mysqlDB);
movieModule.init(mysqlDB);
documentModule.init(mysqlDB);
magazineModule.init(mysqlDB);
filesModule.init(mysqlDB);
fcm.init(mysqlDB);
fcmModule.init(mysqlDB);



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('entities', entities);

app.set('config', config);
app.set('agencyModule', agencyModule);
app.set('homeModule', homeModule);
app.set('aboutModule', aboutModule);
app.set('contentModule', contentModule);
app.set('arrivalModule', arrivalModule);
app.set('movieModule', movieModule);
app.set('documentModule', documentModule);
app.set('magazineModule', magazineModule);
app.set('filesModule', filesModule);
app.set('fcmModule', fcmModule);

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// app.use(bodyParser.json())
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexAuthRouter);
app.use('/api/v2/agency', agencyAuthRouter);
app.use('/api/v2/other', otherAuthRouter);

app.use(function (req, res, next) {
    try {
        let tokenID = req.body["token"];
        if(tokenID === undefined) {
            res.status(401).json({
                "nonce" : new Date().getTime(),
                "status": "unauthorized"
            });
        } else {
            let userInfo = tokens[tokenID];
            if(userInfo === undefined) {
                res.status(401).json({
                    "nonce" : new Date().getTime(),
                    "status": "unauthorized"
                });
            } else {
                agencyModule.ckAgencyValid(tokens[tokenID]["id"], (data) => {
                    let result = data.rows;
                    if(result.length === 1) {
                        tokens[tokenID]["exp"] = tokens[tokenID]["exp"] + config.get("token-exptime");
                        req.userInfo = userInfo;
                        tokenRW.tokenSave();
                        next();
                    } else {
                        delete tokens[tokenID];
                        //console.log("remove : " + tokenID);
                        tokenRW.tokenSave();
                        res.status(401).json({
                            "nonce" : new Date().getTime(),
                            "status": "unauthorized"
                        });
                    }
                });
            }
        }
    } catch (error) {
        res.status(500).json({
            "nonce" : new Date().getTime(),
            "status": error
        });
    }
    
});

app.use('/', indexAuthRouter);


app.use('/api/v2/home', homeAuthRouter);
app.use('/api/v2/about', aboutAuthRouter);
app.use('/api/v2/content', contentAuthRouter);
app.use('/api/v2/arrival', arrivalAuthRouter);
app.use('/api/v2/movie', movieAuthRouter);
app.use('/api/v2/document', documentAuthRouter);
app.use('/api/v2/magazine', magazineAuthRouter);
app.use('/api/v2/fs', fsAuthRouter);
app.use('/api/v2/fcm', fcmAuthRouter);
app.use('/api/v2/pagelink', pagelinkAuthRouter);


// // catch 404 and forward to error handler
// app.use(function(req, res, next) {
//     next(createError(404));
// });

app.all('*', function(req, res){
    res.status(404).json({
        "nonce" : new Date().getTime(),
        "status": "url not found"
    });
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

var j = schedule.scheduleJob('*/5 * * * * *', function(){
    //console.log('Sending FCM ' + new Date());
    fcm.sendFCM();
});


var tokenRemove = schedule.scheduleJob('0 0 */2 * * *', function(){
    let dt = new Date().getTime();
    for ( let [ key, token ] of Object.entries( tokens ) ) {
        if(token.exp < dt + config.get("token-exptime") - 2*60000) {
            //console.log(token);
            try {
                delete tokens[key];
                //console.log("remove : " + key);
            } catch (error) {
                console.log(error);
            }
        }
    }
    tokenRW.tokenSave();
    //console.log(Object.keys(tokens).length);
    
});

app.on("end", function() {
    //console.log("end");
    
});

module.exports = {app: app, server: server};
