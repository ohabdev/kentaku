var FCM = require( 'fcm-node' );
var config = require( 'config' );

// var serverKey = "AAAAdL52I9Q:APA91bGWIsy54f_RlH31kapGuVkI3rtLNtJ6SAtg5RbeeZB8wSsdqdaF_hvi2TXJJ5IiAjWS_oEh7RBqQIUBVr04849NLT6BbecQvHZB-GkAHuPE-BfFnCCONlcYnP5EQZJmiO5VosQX";
var serverKey = config.get( 'fcm-server-key' );

var fcm = new FCM( serverKey );

var mysqlDB;

exports.init = function ( mysqlDBa ) {
    mysqlDB = mysqlDBa;
};

module.exports.sendFCM = function() {
    let fcm_tokens = [];
    for ( let [ key, token ] of Object.entries( tokens ) ) {
        //console.log("key :: ", key );
        // begin retreive authenticated users
        if(token["fcm-token"] === undefined || typeof token["fcm-token"] !== "string" || token["fcm-token"] === "" || token["fcm-token"] === null) {
            continue;
        }
        var fAgencyID = token.id;
        let fcm_token = token["fcm-token"];
        if(fcm_tokens.includes(fcm_token) === true) {
            continue;
        }
        fcm_tokens.push(fcm_tokens);
        let sqlSelectAgency = 
            "SELECT\
                *\
            FROM\
                `t_message`\
            WHERE\
                `f_agency_id` = ? AND `f_schedule_start_date_time` <= NOW() AND (`f_send_flg` = 0 or `f_send_flg` is null or `f_send_flg` = '') AND `f_del_flg` = 0";
        mysqlDB.excSQL(sqlSelectAgency, [parseInt(fAgencyID)], function( SQLResult ) {
            let results = SQLResult.rows;
            //console.log(results.length);
            for( let i = 0; i < results.length; i++ ) {

                var message = { 
                    //"to": "eLQk78EtU3E:APA91bHp2awi-YG9RZj3rP_ifP0Zxjqa5pF3hu4i_-pmE-vF36BxaQD8vdGV4SsGVZ7Tj6RTBMjJXNMIwjBMQibmlc9h2NfIij7sRpumv0c1rW9GgtgtVe71Ww126IP2_eu3LSa8QnOS", // mobarok
                    "to": fcm_token,
                    "data": {
                        "data": {
                            "title": results[i].f_message_title,
                            "is_background": false,
                            "message": results[i].f_message,
                            "image": "",
                            "payload": {
                                "article_data": results[i].f_url ? results[i].f_url + "" : "",
                                "message_id" : results[i].message_id ? results[i].message_id : -1
                            }, 
                        "timestamp": new Date()
                        }
                    }
                };
                //console.log( results[i] );
                // begin send notification
                fcm.send( message, function ( err, response ) {
                    let sqlUpdateAgency = "";
                    if ( err ) {
                        //console.log( "Something has gone wrong!" + err );
                        sqlUpdateAgency = 
                            "UPDATE\
                                `t_message`\
                            SET\
                                `f_send_flg` = 6 , \
                                f_upd_account = 9999 ,\
                                f_upd_time = NOW() \
                            WHERE\
                                `f_message_id` = ? ";
                        delete tokens[key];
                        // console.log("this : ", mysqlDB);
                    } else {
                        //console.log( "Successfully sent with response: ", response );
                        sqlUpdateAgency = 
                            "UPDATE\
                                `t_message`\
                            SET\
                                `f_send_flg` = 1, \
                                f_upd_account = 9999 ,\
                                f_upd_time = NOW() \
                            WHERE\
                                `f_message_id` = ? ";
                        // console.log("this : ", mysqlDB);
                        try {
                            tokens[key]["exp"] = tokens[key]["exp"] + config.get("token-exptime");
                        } catch (error) {
                            console.log(error);
                            
                        }
                        
                        
                        
                    }
                    mysqlDB.excSQL(sqlUpdateAgency, [results[i].f_message_id], function( SQLResult ) {
                        // console.log( 'Updated agency info' );
                    } );
                });
                
                // end send notification
            }
        } );
        // end retreive authenticated users
    }
};