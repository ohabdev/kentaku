var FCM = require( 'fcm-node' );
var config = require( 'config' );

// var serverKey = "AAAAdL52I9Q:APA91bGWIsy54f_RlH31kapGuVkI3rtLNtJ6SAtg5RbeeZB8wSsdqdaF_hvi2TXJJ5IiAjWS_oEh7RBqQIUBVr04849NLT6BbecQvHZB-GkAHuPE-BfFnCCONlcYnP5EQZJmiO5VosQX";
var serverKey = config.get( 'fcm-server-key' );

var fcm = new FCM( serverKey );

var mysqlDB;

exports.init = function ( mysqlDBa ) {
    mysqlDB = mysqlDBa;
};

module.exports.sendFCM = function() {
    let fcm_tokens = [];
    let sqlSelectAgencyList = [];
    let fcmSendMSG = {};
    for ( let [ key, token ] of Object.entries( tokens ) ) {
        //console.log("key :: ", key );
        // begin retreive authenticated users
        if(token["fcm-token"] === undefined || typeof token["fcm-token"] !== "string" || token["fcm-token"] === "" || token["fcm-token"] === null) {
            continue;
        }
        var fAgencyID = token.id;
        let fcm_token = token["fcm-token"];
        if(fcm_tokens.includes(fcm_token) === true) {
            //console.log( token.id + " : "+  fcm_token);
            continue;
        }
        fcm_tokens.push(fcm_token);
        if(fcmSendMSG[token.id] === undefined) {
            fcmSendMSG[token.id] = [];
        }
        fcmSendMSG[token.id].push(fcm_token);
    }
    for ( let [ key, fcmSendMSG_val ] of Object.entries( fcmSendMSG ) ) {
        sqlSelectAgencyList.push(key);
    }
    //sqlSelectAgencyList = sqlSelectAgencyList.substring(0, sqlSelectAgencyList.length ? sqlSelectAgencyList.length-1: 0 );
    // console.log("---------------");
    //console.log("list : " , sqlSelectAgencyList);
    // // console.log(fcm_tokens);
    // console.log(fcmSendMSG);
    // console.log("---------------");
    let sqlSelectAgency = 
            "SELECT\
                *\
            FROM\
                `t_message`\
            WHERE\
                `f_agency_id` in (?) AND \
                `f_schedule_start_date_time` <= NOW() AND \
                (`f_send_flg` = 0 or `f_send_flg` is null or `f_send_flg` = '') AND `f_del_flg` = 0";
    mysqlDB.excSQL(sqlSelectAgency, [sqlSelectAgencyList], function( SQLResult ) {
        let results = SQLResult.rows;
        //console.log(results.length);
        var message = {};
        for( let i = 0; i < results.length; i++ ) {
            message = { 
                //"to": fcmSendMSG[results[i].f_agency_id],
                "registration_ids": fcmSendMSG[results[i].f_agency_id],
                "data": {
                    "data": {
                        "title": results[i].f_message_title,
                        "is_background": false,
                        "message": results[i].f_message ? results[i].f_message : "",
                        "image": "",
                        "payload": {
                            "article_data": results[i].f_url ? results[i].f_url + "" : "",
                            "message_id" : results[i].f_message_id ? results[i].f_message_id : -1
                        }, 
                    "timestamp": new Date()
                    }
                },
                "aps" : { 
                    "alert" : { 
                       "title" : results[i].f_message_title,
                       "subtitle" : results[i].f_message ? results[i].f_message : "",
                       "body" : results[i].f_message ? results[i].f_message : "",
                       "payload" : {
                            "article_data" : results[i].f_url ? results[i].f_url + "" : "",
                            "message_id" : results[i].f_message_id ? results[i].f_message_id : -1
                        }
                    }
                },
                "notification": { 
                    "title" : results[i].f_message_title,
                    "body" : results[i].f_message ? results[i].f_message : "",
                    "content_available" : true,
                    "payload" : {
                        "article_data" : results[i].f_url ? results[i].f_url + "" : "",
                        "message_id" : results[i].f_message_id ? results[i].f_message_id : -1
                    }
                }
            };
            //console.log(message);
            
            fcm.send( message, function ( err, response ) {
                let sqlUpdateAgency = "";
                if ( err ) {
                    //console.log( "Something has gone wrong! : " , err );
                    sqlUpdateAgency = 
                        "UPDATE\
                            `t_message`\
                        SET\
                            `f_send_flg` = 6 , \
                            f_upd_account = 9999 ,\
                            f_upd_time = NOW() \
                        WHERE\
                            `f_message_id` = ? ";
                    //delete tokens[key];
                    // console.log("this : ", mysqlDB);
                } else {
                    //console.log( "Successfully sent with response: ", response );
                    sqlUpdateAgency = 
                        "UPDATE\
                            `t_message`\
                        SET\
                            `f_send_flg` = 1, \
                            f_upd_account = 9999 ,\
                            f_upd_time = NOW() \
                        WHERE\
                            `f_message_id` = ? ";
                    // console.log("this : ", mysqlDB);
                    // try {
                    //     tokens[key]["exp"] = tokens[key]["exp"] + config.get("token-exptime");
                    // } catch (error) {
                    //     console.log(error);
                        
                    // }
                }
                //console.log(sqlUpdateAgency);
                
                mysqlDB.excSQL(sqlUpdateAgency, [results[i].f_message_id], function( SQLResult ) {
                    //console.log( 'Updated agency info' );
                } );
            });
            

            
        //     // end send notification
        }
    });
    
    // end retreive authenticated users
    
};