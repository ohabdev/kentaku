var FCM = require( 'fcm-node' );
var config = require( 'config' );
const fs = require("fs");
const months = {
    0: 'Jan',
    1: 'Feb',
    2: 'Mar',
    3: 'Apr',
    4: 'May',
    5: 'Jun',
    6: 'Jul',
    7: 'Aug',
    8: 'Sep',
    9: 'Oct',
    10: 'Nov',
    11: 'Dec'
};

var serverKey = config.get( 'fcm-server-key' );

var fcm = new FCM( serverKey );

var mysqlDB;

exports.init = function ( mysqlDBa ) {
    mysqlDB = mysqlDBa;
};

module.exports.sendFCM = function() {
    let fcm_tokens = {};
    let sqlSelectAgencyList = [];
    let fcmSendMSG = {};
    //console.log(tokens);
    for ( let [ key, token ] of Object.entries( tokens ) ) {
        //console.log("key :: ", key );
        // begin retreive authenticated users
        if(token["fcm-token"] === undefined || typeof token["fcm-token"] !== "string" || token["fcm-token"] === "" || token["fcm-token"] === null) {
            continue;
        }

        var fAgencyID = token.id;
        // let shasum = crypto.createHash('sha1')
        let device = //shasum.update(
            token["user-agent"];
            //).digest("hex");
        let fcm_token = token["fcm-token"];
        // console.log(device);
        if(fcm_tokens[device] === undefined) {
            fcm_tokens[device] = [];
        }
        if(fcm_tokens[device].includes(fcm_token) === true) {
            //console.log( token.id + " : "+  fcm_token);
            continue;
        }
        fcm_tokens[device].push(fcm_token);
        if(fcmSendMSG[token.id] === undefined) {
            fcmSendMSG[token.id] = {};
        }
        if(fcmSendMSG[token.id][device] === undefined) {
            fcmSendMSG[token.id][device] = [];
        }
        fcmSendMSG[token.id][device].push(fcm_token);
    }
    //console.log(fcmSendMSG);
    for ( let [ key, fcmSendMSG_val ] of Object.entries( fcmSendMSG ) ) {
        sqlSelectAgencyList.push(key);
    }
    // sqlSelectAgencyList = sqlSelectAgencyList.substring(0, sqlSelectAgencyList.length ? sqlSelectAgencyList.length-1: 0 );
    // console.log("---------------");
    // console.log("list : " , sqlSelectAgencyList);
    // // console.log(fcm_tokens);
    // console.log(fcmSendMSG);
    // console.log("---------------");
    let sqlSelectAgency = 
            "SELECT\
                *\
            FROM\
                `t_message`\
            WHERE\
                `f_agency_id` in (?) AND \
                `f_schedule_start_date_time` <= NOW() AND \
                (`f_send_flg` = 0 or `f_send_flg` is null or `f_send_flg` = '') AND `f_del_flg` = 0";
    mysqlDB.excSQL(sqlSelectAgency, [sqlSelectAgencyList], function( SQLResult ) {
        let results = SQLResult.rows;
        //console.log(results.length);
        var message = {};
        
        for( let i = 0; i < results.length; i++ ) {
            for ( let [ key, fcmSendMSG_val ] of Object.entries( fcmSendMSG[results[i].f_agency_id] ) ) {
                //console.log([`${key} : ${JSON.stringify(fcmSendMSG[results[i].f_agency_id][key], null, 4)}`, `is include : ${key.includes('okhttp')}`]);
                message = { 
                    //"to": fcmSendMSG[results[i].f_agency_id],
                    "registration_ids": fcmSendMSG[results[i].f_agency_id][key],
                    // "content_available": true,
                    // "priority": "high",
                    "data": {
                        "data": {
                            "title": results[i].f_message_title,
                            "is_background": true,
                            "message": results[i].f_message ? results[i].f_message : "",
                            "image": "",
                            "payload": {
                                "article_data": results[i].f_url ? results[i].f_url + "" : "",
                                "message_id" : results[i].f_message_id ? results[i].f_message_id : -1
                            }, 
                        "timestamp": new Date()
                        }
                    },
                };
                if(key.includes('okhttp') === false) {
                    message["notification"] = { 
                        "title" : results[i].f_message_title,
                        "body" : results[i].f_message ? results[i].f_message : "",
                        "content_available" : true,
                        "payload" : {
                            "article_data" : results[i].f_url ? results[i].f_url + "" : "",
                            "message_id" : results[i].f_message_id ? results[i].f_message_id : -1
                        },
                        "sound": "default"
                    };
                    message["apns"] = {
                        headers: {
                            'apns-priority': '10',
                        },
                        payload: {
                            aps: {
                                sound: 'default',
                            }
                        },
                    };
                    message["aps"] = { 
                        "content-available": 1,
                        "apns-push-type" : "background",
                        "alert" : { 
                        "title" : results[i].f_message_title,
                        "subtitle" : results[i].f_message ? results[i].f_message : "",
                        "body" : results[i].f_message ? results[i].f_message : "",
                        "payload" : {
                                "article_data" : results[i].f_url ? results[i].f_url + "" : "",
                                "message_id" : results[i].f_message_id ? results[i].f_message_id : -1
                            }
                        },
                        sound : "default",
                        sound2 : "pixies"
                    };
                }
                //console.log(key, JSON.stringify(message, null, 4));
                
                fcm.send( message, function ( err, response ) {
                    let sqlUpdateAgency = "";
                    //console.log(response);
                    if ( err ) {
                        //console.log( "Something has gone wrong! : " , err );
                        let dt = new Date();
                        let logFileName = "error_" + dt.getFullYear() + ""+ months[dt.getMonth()] + "" + dt.getDate() + ".log";
                        fs.mkdir("../logs/" , {recursive: true}, (errmk) => {
                            fs.appendFile('../logs/'+logFileName, dt.toString() + " ::: " + JSON.stringify(err) + "\n", function (err) {
                                
                            });
                        });
                        sqlUpdateAgency = 
                            "UPDATE\
                                `t_message`\
                            SET\
                                `f_send_flg` = 6 , \
                                f_upd_account = 9999 ,\
                                f_upd_time = NOW() \
                            WHERE\
                                `f_message_id` = ? ";
                        //delete tokens[key];
                        // console.log("this : ", mysqlDB);
                    } else {
                        //console.log( "Successfully sent with response: ", response );
                        sqlUpdateAgency = 
                            "UPDATE\
                                `t_message`\
                            SET\
                                `f_send_flg` = 1, \
                                f_upd_account = 9999 ,\
                                f_upd_time = NOW() \
                            WHERE\
                                `f_message_id` = ? ";
                        // console.log("this : ", mysqlDB);
                        // try {
                        //     tokens[key]["exp"] = tokens[key]["exp"] + config.get("token-exptime");
                        // } catch (error) {
                        //     console.log(error);
                            
                        // }
                    }
                    //console.log(sqlUpdateAgency);
                    
                    mysqlDB.excSQL(sqlUpdateAgency, [results[i].f_message_id], function( SQLResult ) {
                        //console.log( 'Updated agency info' );
                    } );
                });
                

                
            //     // end send notification
            }
        }
    });
    
    // end retreive authenticated users
    
};