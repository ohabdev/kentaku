var express = require('express');
var router = express.Router();
const fs = require('fs');

/* GET SLIDER */
router.post('/slider', function(req, res, next) {
    let userInfo = req.userInfo;
    if(userInfo === undefined) {
        res.status(401).json({
            "nonce": new Date().getTime(),
            "status" : "unauthorized"
        });
    } else {
        var homeModule = req.app.get("homeModule");
        homeModule.getAllSlider((SQLResults) => {
            var config = req.app.get("config");
            var results = SQLResults.rows;
            var countResult = results.length;
            
            resArr = [];
            for(let i=1; i<100 && countResult !== 0; i++) {
                if(results[0]["f_image" + i ] === undefined || results[0]["f_image" + i ] == "") {
                        break;
                }
                resArr.push({
                    "slider_id": i,
                    "image": config.get("url.base_url") + config.get("url.slider_img") + results[0]["f_image" + i ],
                    "image_thumb": config.get("url.base_url") + config.get("url.slider_img") + results[0]["f_thumb" + i ],
                    "image_alt": results[0]["f_image_alt" + i ],
                    "url": results[0]["f_url" + i ],
                    "url_kbn": results[0]["f_url_kbn" + i ],
                    "sort_no": results[0]["f_sort_no" + i ]
                });
                
            }
            let u = {
                "nonce": new Date().getTime(),
                "status": "ok",
                "payload": resArr
            };
            res.status(200).json(u);
        });
    }
});


/* GET BANNER */
router.post('/banner', function(req, res, next) {
    let userInfo = req.userInfo;
    if(userInfo === undefined) {
        res.status(401).json({
            "nonce": new Date().getTime(),
            "status" : "unauthorized"
        });
    } else {
        try {
            var homeModule = req.app.get("homeModule");
            homeModule.getAllBanner((SQLResults) => {
                var config = req.app.get("config");
                var results = SQLResults.rows;
                var countResult = results.length;
                resArr = [];
                for(let i=1; i<100 && countResult !== 0; i++) {
                    try {
                        if(results[0]["f_image" + i ] === undefined || results[0]["f_image" + i ] == "") {
                            break;
                        }
                        resArr.push({
                            "banner_id": i,
                            "image": config.get("url.base_url") + config.get("url.banner_img") + results[0]["f_image" + i ],
                            "image_thumb": config.get("url.base_url") + config.get("url.banner_img") + results[0]["f_thumb" + i ],
                            "image_alt": results[0]["f_image_alt" + i ],
                            "page_content_id": results[0]["f_page_content_id" + i ],
                            "sort_no": results[0]["f_sort_no" + i ]
                        });
                    } catch (error) {
                        
                    }
                };

                let u = {
                    "nonce": new Date().getTime(),
                    "status": "ok",
                    "payload": resArr
                };

                res.status(200).json(u);
            });
        } catch (error) {
            let u = {
                "nonce": new Date().getTime(),
                "status": "error" + error
            };

            res.status(500).json(u);
        }
    }
    
});

/* GET BANNER */
router.post('/top-link', function(req, res, next) {
    let userInfo = req.userInfo;
    if(userInfo === undefined) {
        res.status(401).json({
            "nonce": new Date().getTime(),
            "status" : "unauthorized"
        });
    } else {
        try {
            var homeModule = req.app.get("homeModule");
            homeModule.getAllTopLink((SQLResults) => {
                var config = req.app.get("config");
                var results = SQLResults.rows;
                var countResult = results.length;
                //console.log("--------------------------------------");
                
                resArr = [];
                for(let i=0; i<countResult; i++) {
                    try {
                        resArr[i] = {
                            "title": results[i]["f_title"],
                            "url": results[i]["f_url"],
                            "description": results[i]["f_description"]
                        };
                        
                    } catch (error) {
                       
                    }
                }
                let u = {
                    "nonce": new Date().getTime(),
                    "status": "ok",
                    "payload": resArr
                };
                res.status(200).json(u);
                
            });
        } catch (error) {
            console.log(error);
            let u = {
                "nonce": new Date().getTime(),
                "status": "error" + error
            };

            res.status(500).json(u);
        }
    }
});

module.exports = router;