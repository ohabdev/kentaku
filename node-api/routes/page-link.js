var express = require( "express" );
var url = require( "url" );
var queryString = require( "query-string" );
var router = express.Router();
const config = require( "config" );
const webURL = config.get( "url.web_link" );
const fileInfoObj = config.get( "file_info" );
const navInfo = config.get( "nav" );

router.post( "/info-nav", function ( req, res, next ) {
    res.status(200).json({
        "nonce": new Date().getTime(), "status" : "ok", "payload": navInfo
    });
});

router.post( "/", function ( req, res, next ) {
    
    // Check user valid Auth token
    let userInfo = req.userInfo;
    if(userInfo === undefined) {
        res.status(401).json({
            "nonce": new Date().getTime(),
            "status" : "unauthorized"
        });
    } else {
        let payload = req.body["payload"];
        if(payload === undefined ||
            payload["link"] === undefined || typeof payload["link"] !== "string" ||
            payload["message_id"]  === undefined || typeof payload["message_id"]  === Number) {
            res.status(400).json({"nonce": new Date().getTime(), "status" : "error: link not found (invalid request message format)"});
        } else {
            try {
                //console.log(payload["link"]);
                
                if(payload["message_id"]  !== undefined) {
                    let message_id = payload["message_id"];
                    //console.log("message_id : ", message_id);
                    var fcmModule = req.app.get("fcmModule");
                    fcmModule.updateFMessagesbyID( message_id, ( SQLResults ) => {
                        //console.log(SQLResults);
                        //console.log( 'Updated all f messages those are not 2' );
                    });
                    
                }

                let link = encodeURI(payload["link"]);
                let uri = url.parse(link);
                let path = uri["pathname"];
                //console.log(path, path === undefined , path === null , typeof path === undefined);
                
                if( path === undefined || path === null || typeof path === undefined) {
                    path = "";
                }
                
                for(i=0; i<webURL.prefixes.length; i++) {
                   path = path.replace(webURL.prefixes[i], "");
                }
                //console.log(link, uri, path);
                
                s = queryString.parse(uri["query"], {arrayFormat: 'bracket'});
                // if(uri["host"] === url.parse(webURL.hostname).hostname) {
                var hostNames = webURL.hostnames;
                var hostNamesParsed = [];
                hostNames.forEach( function( hostName, index ) {
                    hostNamesParsed.push( url.parse( hostName ).hostname );
                } );
                if(
                    hostNamesParsed.includes( uri["host"] ) &&
                    ( path !== webURL.inquiry && path !== webURL.mimiyori )
                ) {
                    let response = {
                        "device" : "app",
                        "jump": 0,
                        "id": "",
                        "link": ""
                    };

                    // console.log( 'path---: ' + path );
                    // console.log("sub:", path.split( '/' ) );
                    // console.log(path.includes( '/mb/info/' ) , 4 < path.split( '/' ).length);
                    
                    if( path.includes( '/mb/info/' ) && 4 < path.split( '/' ).length ) {
                        let contentModule = req.app.get( "contentModule" );
                        let urlSlots = path.split( '/' );
                        let urlSlotsLength = urlSlots.length;
                        let slag = "";

                        
                        //"https://exam54.threet.co.jp/daito/mb/info/asd/asd/123/1234/kenshin"
                        //"https://exam54.threet.co.jp/daito/mb/info/asd/asd/asd/asd/kenshin/"
                        if(urlSlots[urlSlots.length - 1].length === 0) {
                            slag = urlSlots[urlSlots.length - 2];
                        } else {
                            slag = urlSlots[urlSlots.length - 1];
                        }
                        //console.log("slag:" , slag);
                        
                        response["jump"] = navInfo.page_content_level.id;
                        new Promise
                            ( ( resolve, reject ) => {
                                contentModule.getSlagWiseContent( slag, ( SQLResult ) => {
                                    let results = SQLResult.rows;
                                    //console.log(results);
                                    
                                    if( results[0] ) {
                                        response["id"] = results[0].f_page_content_sub_id + "";
                                        resolve( response );
                                    } else {
                                        reject( true );
                                    }
                                } );
                            } )
                            .then( ( data ) => {
                                //console.log(data);
                                
                                res.status(200).json({
                                    "nonce": new Date().getTime(), "status" : "ok", "payload": data
                                });
                            } )
                            .catch( ( error ) => {
                                let response = {
                                    "device" : "web",
                                    "jump": -1,
                                    "id": "",
                                    "link": payload["link"]
                                };
                                res.status( 200 ).json( { "nonce": new Date().getTime(), "status" : "ok", "payload": response } );
                            } );
                    } else {
                        switch( path ) {
                            case webURL.home: {
                                response["jump"] = navInfo.home.id;
                                break;
                            }
                            case webURL.about: {
                                response["jump"] = navInfo.about.id;
                                break;
                            }
                            case webURL.about_index: {
                                if( s['cmd'] === 'outline' ) {
                                    response["jump"] = navInfo.about_address.id;
                                } else if( s['cmd'] === 'officer'  ) {
                                    response["jump"] = navInfo.about_officer.id;
                                } else {
                                    response["jump"] = navInfo.about.id;
                                }
                                break;
                            }
                            case webURL.arrival : {
                                response["jump"] = navInfo.arrival.id;
                                break;
                            }
                            case webURL.page_content : {
                                response["jump"] = navInfo.page_content.id;
                                break;
                            }
                            case webURL.movie : {
                                response["jump"] = navInfo.movie.id;
                                if( uri.hash === '#movie_list' ) {
                                    response["jump"] = navInfo.movie.id;
                                } else if( uri.hash === '#document_list' ) {
                                    response["jump"] = navInfo.document.id;
                                }
                                break;
                            }
                            case webURL.document : {
                                response["jump"] = navInfo.document.id;
                                break;
                            }
                            case webURL.magazine : {
                                response["jump"] = navInfo.magazine.id;
                                break;
                            }
                            case webURL.privacy_policy : {
                                response["jump"] = navInfo.privacy_policy.id;
                                break;
                            }
    
                            case webURL.mimiyori : {
                                response["jump"] = navInfo.mimiyori.id;
                                break;
                            }
                            case webURL.arrival_index : {
                                response["jump"] = navInfo.arrival_index.id;
                                response["id"] = "";
                                if(s["sid"] !== undefined && typeof s["sid"] === "string") {
                                    response["id"] = s["sid"];
                                }
                                break;
                            }
                            case webURL.inquiry : {
                                response["jump"] = navInfo.inquiry.id;
                                break;
                            }
                            case webURL.library_index : {
                                response[ "id" ] = "";
                                if( s['document_pno'] === undefined && s['pno'] !== undefined ) {
                                    response["jump"] = navInfo.movie_show.id;
                                    response[ "id" ] = s['pno'];
                                } else if( s['pno'] === undefined && s['document_pno'] !== undefined ) {
                                    response["jump"] = navInfo.document_show.id;
                                    response[ "id" ] = s['document_pno'];
                                }
                                break;
                            }
                            case webURL.magazine_index : {
                                response["jump"] = navInfo.magazine_index.id;
                                response[ "id" ] = "";
                                if( s['sid'] === undefined && s['f_disp_date_y'] !== undefined ) {
                                    response["jump"] = navInfo.magazine_index.id;
                                    response[ "id" ] = s['f_disp_date_y'];
                                } else if( s['f_disp_date_y'] === undefined && s['sid'] !== undefined ) {
                                    response["jump"] = navInfo.magazine_detail.id;
                                    response[ "id" ] = s['sid'];
                                }
                                break;
                            }
                            case webURL.mb_info1 : {
                                response["jump"] = navInfo.mb_info1.id;
                                break;
                            }
                            case webURL.mb_info2 : {
                                response["jump"] = navInfo.mb_info2.id;
                                break;
                            }
                            case webURL.mb_info3 : {
                                response["jump"] = navInfo.mb_info3.id;
                                break;
                            }
                            default : {
                                response = {
                                    "device" : "web",
                                    "jump": -1,
                                    "id": "",
                                    "link": payload["link"]
                                };
                                break;
                            }
                        }
                        //console.log(response);
                        res.status(200).json({
                            "nonce": new Date().getTime(), "status" : "ok", "payload": response
                        });
                    }
                } else {
                    let response = {
                        "device" : "web",
                        "jump": -1,
                        "id": "",
                        "link": payload["link"]
                    };
                    res.status( 200 ).json( { "nonce": new Date().getTime(), "status" : "ok", "payload": response } );
                }
            } catch (error) {
                res.status(500).json({
                    "nonce": new Date().getTime(), "status" : "error " + error
                });
            }
        }
    }
});

module.exports = router;
