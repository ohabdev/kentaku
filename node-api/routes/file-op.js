var express = require('express');
var router = express.Router();
const fs = require('fs');
var mime = require('mime-types');
var pathSys = require('path');
const fsRead = require("../libs/fs-read");
const config = require("config");
const fileInfoObj = config.get("file_info");

router.post("/asd", function(req, res, next) {
    let payload = req.body["payload"];
    //console.log(payload);
    res.status(200).json({"fileInfo" : fileInfoObj});
});

router.post("/", function(req, res, next) {
    let userInfo = req.userInfo;
    //console.log(userInfo);
    if(userInfo === undefined) {
        //console.log("untoken");
        
        res.status(401).json({
            "nonce": new Date().getTime(),
            "status" : "unauthorized"
        });
    } else {
        try {
            let payload = req.body["payload"];
            if(payload["file-info"] === undefined) {
                res.status(400).json({"nonce": new Date().getTime(), "status" : "error: file-info not found (invalid request message framing)"});
            } else if(
                payload["file-info"]["id"]     === undefined ||
                payload["file-info"]["value"]  === undefined || 
                payload["file-info"]["index"]  === undefined
            ) {
                res.status(400).json({"nonce": new Date().getTime(), "status" : "error: file-info not found (invalid request message framing)"});
            } else if (
                typeof payload["file-info"]["id"]    === "string" &&
                typeof payload["file-info"]["value"] === "string" &&
                typeof payload["file-info"]["index"] === "string" &&
                fileInfoObj[payload["file-info"]["id"]] !== undefined
            ) {
                var fileInfo = {
                    "table"     : "" + fileInfoObj[payload["file-info"]["id"]]["table"],
                    "field"     : "" + fileInfoObj[payload["file-info"]["id"]]["field"] + "" + payload["file-info"]["index"] + "",
                    "file_path" : "" + fileInfoObj[payload["file-info"]["id"]]["file_path"],
                    "value"     : "" + payload["file-info"]["value"] + "",
                };
                //console.log(fileInfo);
                
                let filesModule = req.app.get("filesModule");
                // console.log({"level" : userInfo["level"]})
                let u = undefined;
                if(fileInfoObj[payload["file-info"]["id"]]["view_level"] === true) {
                    u = {"level": userInfo["level"]};
                }
                filesModule.getFile(fileInfo, userInfo = u, (SQLResult) => {
                    let results = SQLResult.rows;
                    console.log("result",results.length);
                    if(results === undefined || results.length === 0 || results[0]["count"] === undefined || results[0]["count"] === 0) {
                        res.status(403).json({"nonce": new Date().getTime(), "status" : "ファイルアクセスが拒否されました (file access denied)"});
                    }
                    else {
                        let fread = fsRead.readFile(
                            config.get("url.base_physical_path") +
                            fileInfo["file_path"] +
                            payload["file-info"]["value"]
                        );
                        res.status(fread[1]).json({
                            "nonce": new Date().getTime(),
                            "status" : fread[2],
                            "payload": fread[0]
                        });
                    }
                });
            } else {
                // console.log(typeof payload["file-info"]["id"],
                //             typeof payload["file-info"]["value"],
                //             typeof payload["file-info"]["index"],
                //             fileInfoObj[payload["file-info"]["id"]]);
                
                res.status(412).json({"nonce": new Date().getTime(), "status" : "error: Invalid parameter type. 'id' and 'value' must be string."});
            }
        } catch(err) {
            res.status(500).json({"nonce": new Date().getTime(), "status" : "error: " + err});
        }
    }
});

router.post("/fike", function(req, res, next) {
    let userInfo = req.userInfo;
    //console.log(userInfo);
    if(userInfo === undefined) {
        //console.log("untoken");
        
        res.status(401).json({
            "nonce": new Date().getTime(),
            "status" : "unauthorized"
        });
    } else {
        try {
            let payload = req.body["payload"];
            //console.log(payload);
            
            if(payload["file-info"] === undefined) {
                res.status(400).json({"nonce": new Date().getTime(), "status" : "error: file-info not found (invalid request message framing)"});
            } else if(
                payload["file-info"]["id"]     === undefined ||
                payload["file-info"]["value"]  === undefined || 
                payload["file-info"]["index"]  === undefined
            ) {
                res.status(400).json({"nonce": new Date().getTime(), "status" : "error: file-info not found (invalid request message framing)"});
            } else if (
                typeof payload["file-info"]["id"]    === "string" &&
                typeof payload["file-info"]["value"] === "string" &&
                typeof payload["file-info"]["index"] === "string" &&
                fileInfoObj[payload["file-info"]["id"]] !== undefined
            ) {
                var fileInfo = {
                    "table"     : "" + fileInfoObj[payload["file-info"]["id"]]["table"],
                    "field"     : "" + fileInfoObj[payload["file-info"]["id"]]["field"] + "" + payload["file-info"]["index"] + "",
                    "file_path" : "" + fileInfoObj[payload["file-info"]["id"]]["file_path"],
                    "value"     : "" + payload["file-info"]["value"] + "",
                };
                //console.log(fileInfo);
                
                let filesModule = req.app.get("filesModule");
                // console.log({"level" : userInfo["level"]})
                let u = undefined;
                if(fileInfoObj[payload["file-info"]["id"]]["view_level"] === true) {
                    u = {"level": userInfo["level"]};
                }
                // let path = config.get("url.base_physical_path");
                // console.log(path);
                filesModule.getFile(fileInfo, userInfo = u, (SQLResult) => {
                    //let payload = req.body["payload"];
                    let count = SQLResult.rows;
                    if(count.length === 0) {
                        res.status(404).json({"nonce": new Date().getTime(), "status" : config.get("lang.file_denied")});
                    } else {
                        let filePath = config.get("url.base_physical_path") +
                                       fileInfo["file_path"] +
                                       payload["file-info"]["value"];
                        // let filePath = "/home/pavel/projects/test/c/p.o";
                        if (fs.existsSync(filePath)) {
                            let stats = fs.statSync(filePath);
                            let mimeType = mime.contentType(pathSys.extname(filePath));
                            if(mimeType === false) {
                                mimeType = "application/octet-stream";
                            }
                            res.setHeader('Content-Type', mimeType);
                            res.setHeader('Content-Length', stats.size);
                            //res.setHeader("Content-Dispositon","filename=" + "test.zip");
                            const src = fs.createReadStream(filePath);
                            src.pipe(res);
                        } else {
                            res.status(404).json({
                                "nonce": new Date().getTime(),
                                "status" : config.get("lang.file_not_found")
                            });
                        }
                    }
                });
            }
        } catch(err) {
            res.status(500).json({"nonce": new Date().getTime(), "status" : "error: " + err});
        }
    }
    
});


module.exports = router;