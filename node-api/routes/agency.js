var express = require('express');
var router = express.Router();
const crypto = require('crypto');
const config = require("config");
const navInfoObj = config.get("nav");
const utility = require("../libs/utility");

//==========================================================================
//                      Login Token
//==========================================================================

//===================== Get Token route ===================
router.post("/get-token", (req, res, next) => {
    let dt      = new Date();
    let nonce   = dt.getTime();
    try {
        let payload = req.body["payload"];
        if(
            ( payload["user"]       !== undefined   &&  typeof  payload["user"]     === "string" ) &&
            ( payload["password"]   !== undefined   &&  typeof  payload["password"] === "string" )
        ) {
            try {
                let user        =   payload["user"];
                let password    =   payload["password"];
                if( user === "" || password === "" ) {
                    res.status(400).json({
                        "status": "error "+ "user or password"
                    });
                } else {
                    try {
                        let agencyModule = req.app.get("agencyModule");
                        var userInfo = {};
                        agencyModule.getAgencyLogin(user, password, (SQLResult) => {
                            if( SQLResult[0] !== undefined ) {
                                let u =  SQLResult[0];
                                if( parseInt(u["f_acct_valid_flg_new"]) === 1 && parseInt(u["f_auth_new"]) === 1 ) {
                                    let sha384 = crypto.createHmac("sha384", "1122");
                                    let api_key = crypto.createHash('md5').update("" + u["f_id"]).digest("hex");
                                    let api_secret = crypto.createHash('sha256').update( "salt"
                                                        + crypto.createHash('md5').update(""+ u["f_id"])
                                                        + crypto.createHash('md5').update(password)
                                                        + req.headers["user-agent"]
                                                        + nonce
                                                    ).digest("hex");
                                    let userTypeCheck = u["f_agency_type"];
                                    let userInfo = {
                                        "nonce" : nonce,
                                        "user-agent" : req.headers["user-agent"],
                                        "api-key" : api_key,
                                        "api-secret" : api_secret,
                                        "exp" : nonce + config.get("token-exptime"),
                                        "level" : parseInt(u["f_view_level"]),
                                        "id" : parseInt(u["f_agency_id"]),
                                        "type": userTypeCheck
                                    };

                                    if( userTypeCheck === "1" || userTypeCheck === "2" || userTypeCheck === "3" || userTypeCheck === "4" ){
                                        userInfo["name"] = u["f_name2"];
                                        userInfo["code"] = u["f_emp_cd"];
                                        userInfo["account_attribute"] = "admin";
                                    }else if( userTypeCheck === "5" ){
                                        userInfo["name"] = u["f_name2"];
                                        userInfo["code"] = u["f_agency_cd"];
                                        userInfo["user_id"] = u["f_id"];
                                        // userInfo["parent_id"] = u["f_parent_id"];
                                        userInfo["zip"] = u["f_zip"];
                                        userInfo["account_attribute"] = "agency";
                                        userInfo["parent_name"] = u["f_name"];
                                        userInfo["name_kana"] = u["f_name_kana"];
                                        userInfo["sales_cd"] = u["f_sales_cd"];
                                    }else {
                                        userInfo["name"] = "";
                                        userInfo["code"] = u["f_name2"];
                                        userInfo["account_attribute"] = "register";
                                    }

                                    let v = sha384.update(JSON.stringify(userInfo)).digest("hex");
                                    tokens[""+ v] = userInfo;
                                    tokenRW.tokenSave();
                                    res.status(200).json({
                                        "nonce" : nonce,
                                        "status" : "ok",
                                        "payload" : [{
                                            "token" : v,
                                            "userInfo" : userInfo
                                        }]
                                    });

                                } else if( parseInt(u["f_auth_new"]) === 2 ) {
                                    res.status(401).json({
                                        "nonce" : nonce,
                                        "status": config.lang.account_not_active
                                    });
                                } else {
                                    res.status(401).json({
                                        "nonce" : nonce,
                                        "status": config.lang.account_not_active
                                    });
                                }
                            } else {
                                res.status(401).json({
                                    "nonce" : nonce,
                                    "status": config.lang.login_invalid_id_pass
                                });
                            }
                        });
                    } catch (error) {

                        res.status(401).json({
                            "nonce" : nonce,
                            "status": config.lang.login_invalid_id_pass
                        });
                    }
                }
            } catch (error) {
                res.status(400).json({
                    "nonce" : nonce,
                    "status": "Data not found " + error
                });
            }
        } else {
            res.status(400).json({
                "nonce" : nonce,
                "status": "error user or password data not valid"
            });
        }
    } catch (error) {
        res.status(500).json({
            "nonce" : nonce,
            "status": "error "+ error
        });
    }
});
//==========================================================================
//                      Navbar view level check
//==========================================================================

//============ Navbar View Level check route ==============
router.post("/get-nav", (req, res, next) => {
    let nonce = new Date().getTime();
    try {
        let tokenID = req.body["token"];
        if( tokenID === undefined || typeof tokenID !== "string" || tokens[tokenID] === undefined ) {
            res.status(401).json({
                "nonce" : new Date().getTime(),
                "status": "unauthorized"
            });
        } else {
            let userInfo        =   tokens[tokenID];
            let agencyModule    =   req.app.get("agencyModule");
            agencyModule.getAllGnavi( userInfo = {"level": userInfo["level"]}, ( SQLResult ) => {
                let nav = {};
                navDB = SQLResult.rows;
                for ( let [ key, navInfo ] of Object.entries( navInfoObj ) ) {
                    nav[key] = {
                        "id" : navInfo["id"],
                        "enable" : navInfo["enable"]
                    }
                    
                    for(i=0; i<navDB.length; i++) {
                        if( navInfo["db_name"] === navDB[i]["f_page_name"] ) {
                            nav[key]["enable"] = (navDB[i]["FLAG"] === 1 ? true: false);
                        }
                    }
                }
                res.status(200).json({
                    "nonce" : nonce,
                    "status": "ok",
                    "payload" : [nav]
                });
            });
        }
    } catch (error) {
        res.status(500).json({
            "nonce" : nonce,
            "status": "error: " + error
        });
    }
});

//==========================================================================
//                      Admin My Top  Page
//==========================================================================

//================== Admin View Route ====================
router.post("/mytop-page-admin", (req, res, next) => {
    let nonce = new Date().getTime();
    try {
        let tokenID = req.body["token"];
        if( tokenID === undefined || typeof tokenID !== "string" || tokens[tokenID] === undefined ) {
            res.status(401).json({
                "nonce": new Date().getTime(),
                "status" : "unauthorized"
            });
        } else {

            let userInfo        =   tokens[tokenID];
            let agencyId        =   userInfo["id"];
            let userType        =   userInfo["account_attribute"];
            let agencyModule    =   req.app.get("agencyModule");
            
            agencyModule.getAgencyInfo( agencyId, ( SQLResult ) => {
                let u = SQLResult.rows[0];
                if( u === undefined){
                    res.status(403).json({
                        "nonce" : nonce,
                        "status" : config.lang.access_denied
                    });
                }else{
                    if( userType === "admin" ){
                        agencyModule.getAllOccupation( ( occupations ) => {
                            occupations = occupations.rows;
                            let countOccupations= occupations.length;
                            var occupationArr = [];

                            for(let i=0; i<countOccupations; i++) {
                                occupationArr.push({
                                    "id": occupations[i]["id"],
                                    "name": occupations[i]["name"],
                                    "value": false
                                })
                            }

                            var isEnableOccupation = [];
                            if(u["f_occupation"] !== undefined && typeof u["f_occupation"] === "string") {
                                isEnableOccupation = u["f_occupation"].split(',');
                            }
                            
                            if(Array.isArray(isEnableOccupation) === true ) {
                                for(i=0; i<isEnableOccupation.length; i++) {
                                    let index = occupationArr.findIndex((item)=> {
                                        return item.id === parseInt(isEnableOccupation[i]);
                                    });
                                    if(index !== -1) {
                                        occupationArr[index]["value"] = true;
                                    }
                                }
                            }

                            let companyCD = "";
                            if( u["f_company_cd"] == 1 ){
                                companyCD = "大東建託";
                            }else if( u["f_company_cd"] == 2){
                                companyCD = "大東建託パートナーズ";
                            }else{
                                companyCD = "大東建設";
                            }

                            let userInfo = {
                                // "company_cd" : u["f_company_cd"], 
                                "agency_id" : u["f_agency_id"], 
                                "company_cd" : companyCD,
                                "agency_code" : "" + u["f_emp_cd"],
                                "name" : "" + u["f_name2"],
                                "mailaddress" : u["f_mailaddress"],
                                "user_id" : u["f_id"],
                                "occupations" : occupationArr
                            }
    
                            res.status(200).json({
                                "nonce" : nonce,
                                "status" : "ok",
                                "payload" : [{
                                    "userInfo" : userInfo
                                }]
                            });
                        });
                    } else {
                        res.status(403).json({
                            "nonce" : nonce,
                            "status" : config.lang.access_denied
                        });
                    }
                }
            });
        }
    } catch ( error ) {
        res.status(500).json({
            "nonce": new Date().getTime(),
            "status" : "error: " + error
        });
    }
});

//============ Admin Update Route ===========
router.post("/mytop-page-admin-update", (req, res, next) => {
    try {
        let nonce   = new Date().getTime();
        let tokenID = req.body["token"];
        if ( tokenID === undefined || typeof tokenID !== "string" || tokens[tokenID] === undefined ) {
            res.status(401).json({
                "nonce": new Date().getTime(),
                "status" : "unauthorized"
            });
        } else {
            try {
                let userInfo    =   tokens[tokenID];
                let agencyId    =   userInfo["id"];
                let payload     =   req.body["payload"];
                //----- Payload input data ----- 
                let mailaddress     =   payload["mailaddress"];
                let occupations     =   payload["occupations"];
                let newPassword     =   payload["new_password"];
                let confirmPassword =   payload["old_password"];

                if( 
                    ( mailaddress     !==   undefined   &&  typeof  mailaddress     === "string" )   &&
                    ( occupations     !==   undefined   &&  typeof  occupations     === "string" )   &&
                    ( newPassword     !==   undefined   &&  typeof  newPassword     === "string" )   &&
                    ( confirmPassword !==   undefined   &&  typeof  confirmPassword === "string" )
                ) {
                    let updateData = {};
                    if ( mailaddress !== '' ) {
                        if ( utility.validateEmail( mailaddress ) !== true ) {
                                res.status(400).json({
                                    "nonce": new Date().getTime(),
                                    "status" : config.lang.enter_valid_email
                                });
                        } else {
                            // unique email check
                            let agencyModule = req.app.get("agencyModule");
                            agencyModule.uniqueEmailCheck( agencyId, mailaddress, ( SQLResult ) => {
                                let result = SQLResult.rows;
                                if ( result.length > 0 ) {
                                    res.status(409).json({
                                        "nonce": new Date().getTime(),
                                        "status" : config.lang.duplicate_email
                                    });
                                } else {
                                    updateData["f_mailaddress"] = mailaddress;
                                    updateData['occupations']   = occupations;
                                    if( newPassword !== '' && confirmPassword !== '' ) {
                                        //--- Password update ---
                                        if(  
                                            ( newPassword.length     <  6  ||  newPassword.length     > 20 ) || 
                                            ( confirmPassword.length <  6  ||  confirmPassword.length > 20 ) 
                                            ){
                                            res.status(400).json({
                                                "nonce": new Date().getTime(),
                                                "status" : config.lang.password_min_six_char
                                            });
                                        } else if ( newPassword !== confirmPassword ) {
                                            res.status(400).json({
                                                "nonce": new Date().getTime(),
                                                "status" : config.lang.password_not_matched
                                            });
                                        } else {
                                            //======================= New Password & Confirm Password ====================
                                            const PASSWORD_SALT_WORDS = "0123456789abcdef";
                                            let newPasswordLength = PASSWORD_SALT_WORDS.length;
                                            let newPasswordSalt = "";
                                            for ( var i = 0; i < newPasswordLength; i++ ) {
                                                newPasswordSalt += PASSWORD_SALT_WORDS.charAt( Math.floor( Math.random() * newPasswordLength ) );
                                            }
    
                                            let newPasswordHashed = newPasswordSalt + crypto.createHash('sha256').update(confirmPassword + newPasswordSalt).digest('hex');
                                            let agencyModule = req.app.get("agencyModule");
                                            if ( userInfo["account_attribute"] === "admin" ) {
                                                agencyModule.adminInfoUpdate( agencyId, updateData, newPasswordHashed, ( SQLResult ) => {
                                                    let result = SQLResult.rows;
                                                    if( result.affectedRows === 1 ){
                                                        res.status(200).json({
                                                            "nonce": new Date().getTime(),
                                                            "status" : config.lang.update_success
                                                        });
                                                    }else{
                                                        res.status(400).json({
                                                            "nonce": new Date().getTime(),
                                                            "status" : "Password not updated yet"
                                                        });
                                                    }
                                                });
                                            } else {
                                                res.status(403).json({
                                                    "nonce": new Date().getTime(),
                                                    "status" : config.lang.access_denied
                                                });
                                            }
                                            //======================= New Password & Old Password update ====================
                                            // let agencyModule = req.app.get("agencyModule");
                                            // agencyModule.getAgencyInfo(agencyId, (SQLResult) => {
                                            //     if (SQLResult === undefined) {
                                            //         res.status(403).json({
                                            //             "nonce": new Date().getTime(),
                                            //             "status" : "Access Denied"
                                            //         });
                    
                                            //     } else {
                                            //         // let password = SQLResult.rows[0]["f_password"];
                                            //         // let oldPasswordSalt = password.substring(0, 16);
                                            //         // let oldPasswordHashed = oldPasswordSalt + crypto.createHash('sha256').update(oldPassword + oldPasswordSalt).digest('hex');
                                            //         if( password === oldPasswordHashed ) {
                                
                                            //             const PASSWORD_SALT_WORDS = "0123456789abcdef";
                                            //             let newPasswordLength = PASSWORD_SALT_WORDS.length;
                                            //             let newPasswordSalt = "";
                                            //             for (var i = 0; i < newPasswordLength; i++) {
                                            //                 newPasswordSalt += PASSWORD_SALT_WORDS.charAt( Math.floor( Math.random() * newPasswordLength ) );
                                            //             }
                                
                                            //             let newPasswordHashed = newPasswordSalt + crypto.createHash('sha256').update(newPassword + newPasswordSalt).digest('hex');
                                            //             let agencyModule = req.app.get("agencyModule");
                                            //             agencyModule.adminInfoUpdate(agencyId, updateData, newPasswordHashed, (SQLResult) => {
                                            //                 let result = SQLResult.rows;
                                            //                 if(result.affectedRows === 1){
                                            //                     res.status(200).json({
                                            //                         "nonce": new Date().getTime(),
                                            //                         "status" : "Data & New Password Updated Successfully"
                                            //                     });
                                            //                 }else{
                                            //                     res.status(400).json({
                                            //                         "nonce": new Date().getTime(),
                                            //                         "status" : "Password not updated yet"
                                            //                     });
                                            //                 }
                                            //             });
                                
                                            //         } else {
    
                                            //             res.status(400).json({
                                            //                 "nonce": new Date().getTime(),
                                            //                 "status" : "Your old password is not correct."
                                            //             });
    
                                            //         }
                                            //     }
                                            // });
                                        }
                                    } else if( newPassword !== '' && confirmPassword === '' ){
                                        res.status(400).json({
                                            "nonce": new Date().getTime(),
                                            "status" : config.lang.password_not_matched
                                        });
                                    } else if( newPassword === '' && confirmPassword !== '' ){
                                        res.status(400).json({
                                            "nonce": new Date().getTime(),
                                            "status" : config.lang.password_not_matched
                                        });
                                    } else {
                                        let agencyModule = req.app.get( "agencyModule" );
                                        agencyModule.getAgencyInfo( agencyId, ( SQLResult ) => {
                                            let countResult = SQLResult.rows.length
                                            if ( countResult === 0 ) {
                                                res.status(403).json({
                                                    "nonce": new Date().getTime(),
                                                    "status" : config.lang.access_denied
                                                });
                                            } else {
                                                let password = SQLResult.rows[0][ "f_password" ];
                                                if (userInfo["account_attribute"] === "admin") {
                                                    agencyModule.adminInfoUpdate( agencyId, updateData, password, ( SQLResult ) => {
                                                        let result = SQLResult.rows;
                                                        if( result.affectedRows === 1 ){
                                                            res.status(200).json({
                                                                "nonce": new Date().getTime(),
                                                                "status" : config.lang.update_success
                                                            });
                                                        }else{
                                                            res.status(400).json({
                                                                "nonce": new Date().getTime(),
                                                                "status" : "Error: Data not updated"
                                                            });
                                                        }
                                                    });
                                                } else {
                                                    res.status(403).json({
                                                        "nonce": new Date().getTime(),
                                                        "status" : config.lang.access_denied
                                                    });
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    } else {
                        res.status(400).json({
                            "nonce": new Date().getTime(),
                            "status" : config.lang.email_field_empty
                        });
                    }
                }else{
                    res.status(400).json({
                        "nonce": new Date().getTime(),
                        "status" : config.lang.data_not_valid
                    });
                }
            } catch (error) {
                res.status(400).json({
                    "nonce": new Date().getTime(),
                    "status" : "error: Data not found"+ error
                });
            }
        }
    } catch (error) {
        res.status(500).json({
            "nonce": new Date().getTime(),
            "status" : "error: " + error
        });
    }
});

//==========================================================================
//                      Agency My Top  Page
//==========================================================================

//============ Agency view Route ================
router.post("/mytop-page-agency", (req, res, next) => {
    let nonce = new Date().getTime();
    try {
        let tokenID = req.body["token"];
        if( tokenID === undefined || typeof tokenID !== "string" || tokens[tokenID] === undefined ) {
            res.status(401).json({
                "nonce": new Date().getTime(),
                "status" : "unauthorized"
            });
        } else {
            try {

                let userInfo     =  tokens[tokenID];
                let agencyModule =  req.app.get("agencyModule");
                let agencyId     =  userInfo["id"];
                let userType     =  userInfo["account_attribute"];

                agencyModule.getAgencyInfo( agencyId, ( SQLResult ) => {
                    let u = SQLResult.rows[0];
                    if( u === undefined ){
                        res.status(403).json({
                            "nonce" : nonce,
                            "status" : config.lang.access_denied
                        });
                    }else{
                        //check user type
                        if( userType === "agency" ) {
                            let agencyModule = req.app.get( "agencyModule" );
                            agencyModule.getAllIndustry( ( industries ) => {
                                industries = industries.rows;
                                try {
                                    let countIndustries = industries.length;
                                var industryarr = [];

                                for( let i=0; i<countIndustries; i++ ) {
                                    industryarr.push({
                                        "id": industries[i]["id"],
                                        "name": industries[i]["name"],
                                        "value": false
                                    })
                                }

                                var isEnableIndustry = [];
                                if( u["f_industry"] !== undefined && typeof u["f_industry"] === "string" ) {
                                    isEnableIndustry = u["f_industry"].split(',');
                                }
                                
                                if( Array.isArray(isEnableIndustry) === true ) {
                                    for( i=0; i<isEnableIndustry.length; i++ ) {
                                        let index = industryarr.findIndex((item)=> {
                                            return item.id === parseInt(isEnableIndustry[i]);
                                        });
                                        if( index !== -1 ) {
                                            industryarr[index]["value"] = true;
                                        }
                                    }
                                }
                                } catch ( error ) {
                                    //console.log(error);
                                }
                                
                                // agencyModule.getAllOccupation((occupations) => {
                                //     occupations = occupations.rows;
                                //     let countOccupations= occupations.length;
                                //     var occupationArr = [];

                                //     for(let i=0; i<countOccupations; i++) {
                                //         occupationArr.push({
                                //             "id": occupations[i]["id"],
                                //             "name": occupations[i]["name"],
                                //             "value": false
                                //         })
                                //     }

                                //     var isEnableOccupation = [];
                                //     if(u["f_occupation"] !== undefined && typeof u["f_occupation"] === "string") {
                                //         isEnableOccupation = u["f_occupation"].split(',');
                                //     }
                                    
                                //     if(Array.isArray(isEnableOccupation) === true ) {
                                //         for(i=0; i<isEnableOccupation.length; i++) {
                                //             let index = occupationArr.findIndex((item)=> {
                                //                 return item.id === parseInt(isEnableOccupation[i]);
                                //             });
                                //             if(index !== -1) {
                                //                 occupationArr[index]["value"] = true;
                                //             }
                                //         }
                                //     }
                                //  Here all getAllSiten 
                                // });

                                agencyModule.getAllSiten( ( sitens ) => {
                                    sitens = sitens.rows;
                                    let countSitens = sitens.length;
                                    var sitensArr = [];

                                        for( let i=0; i<countSitens; i++ ) {
                                            sitensArr.push({
                                                "id": sitens[i]["id"],
                                                "name": sitens[i]["name"],
                                                "value": false
                                            })
                                        }

                                    var isEnableSitens = [];
                                        if( u["f_siten_cd"] !== undefined && typeof u["f_siten_cd"] === "string" ) {
                                            isEnableSitens = u["f_siten_cd"].split(',');
                                        }

                                        if( Array.isArray(isEnableSitens) === true ) {
                                            for(i=0; i<isEnableSitens.length; i++) {
                                                let index = sitensArr.findIndex((item)=> {
                                                    return item.id === parseInt(isEnableSitens[i]);
                                                });
                                                if(index !== -1) {
                                                    sitensArr[index]["value"] = true;
                                                }
                                            }
                                        }
                                    userInfo = {
                                        "agency_id" : parseInt(u["f_agency_id"]),
                                        "company_name" : "" + u["f_name"],
                                        "company_name_kana" : "" + u["f_name_kana"],
                                        "agency_code" : "" + u["f_agency_cd"],
                                        "mailaddress" : "" + u["f_mailaddress"],
                                        "user_id" : u["f_id"],
                                        "sales_cd" : u["f_sales_cd"],
                                        "emp_no" : parseInt(u["f_no_emp"]),
                                        "full_name" : "" + u["f_name2"],
                                        "foreign_nationality_flg" : parseInt(u["foreign_nationality_flg"]),
                                        "industries" : industryarr,
                                        // "occupations" : occupationArr,
                                        "sitens" : sitensArr,
                                    }
                                    res.status(200).json({
                                        "nonce" : nonce,
                                        "status" : "ok",
                                        "payload" : [{
                                            "userInfo" : userInfo
                                        }]
                                    });
                                });
                            });
                        } else {
                            res.status(403).json({
                                "nonce" : nonce,
                                "status" : config.lang.access_denied
                            });
                        }
                    } 
                });
            } catch ( error ) {
                res.status(400).json({
                    "nonce" : nonce,
                    "status" : "error: Login fail" + error
                });
            }
        }
    } catch ( error ) {
        res.status(500).json({
            "nonce" : new Date().getTime(),
            "status" : "error : " + error
        });
    }
});

//============ Agency Update Route ===========
router.post("/mytop-page-agency-update", (req, res, next) => {
    try {
        let nonce   = new Date().getTime();
        let tokenID = req.body["token"];
        if( tokenID === undefined || typeof tokenID !== "string" || tokens[tokenID] === undefined ) {
            res.status(401).json({
                "nonce": new Date().getTime(),
                "status" : "unauthorized"
            });
        } else {
            try {

                let userInfo    =   tokens[tokenID];
                let agencyId    =   userInfo["id"];
                let payload     =   req.body["payload"];

                // let userName    =   payload["user_id"];
                let empNo       =   payload["emp_no"];
                let fullName    =   payload["full_name"];
                let mailAddress =   payload["mailaddress"];
                let foNaFlg     =   payload["foreign_nationality_flg"];
                let industries  =   payload["industries"];
                // let occupations =   payload["occupations"];
                let sitens      =   payload["sitens"];
                let newPassword =   payload["new_password"];
                let confirmPassword =   payload["old_password"];
                if(
                    // userName        !==  undefined && typeof userName       === "string" &&
                    ( empNo           !==  undefined && typeof empNo            === "number" ) &&
                    ( fullName        !==  undefined && typeof fullName         === "string" ) &&
                    ( mailAddress     !==  undefined && typeof mailAddress      === "string" ) &&
                    ( industries      !==  undefined && typeof industries       === "string" ) && 
                    // occupations     !==  undefined && typeof occupations    === "string" && 
                    ( sitens          !==  undefined && typeof sitens           === "string" ) && 
                    ( foNaFlg         !==  undefined && typeof foNaFlg          === "number" ) &&
                    ( newPassword     !==  undefined && typeof newPassword      === "string" ) && 
                    ( confirmPassword !==  undefined && typeof confirmPassword  === "string" )
                ){
                    let updateData = {};
                    if ( mailAddress !== '' ) {
                        if( utility.validateEmail(mailAddress) === false ){
                            res.status(400).json({
                                "nonce": new Date().getTime(),
                                "status" : config.lang.enter_valid_email
                            });
                        }else{
                            // unique email check
                            let agencyModule = req.app.get("agencyModule");
                            agencyModule.uniqueEmailCheck( agencyId, mailAddress, ( SQLResult ) => {
                                let result = SQLResult.rows;
                                if ( result.length > 0 ) {
                                    res.status(409).json({
                                        "nonce": new Date().getTime(),
                                        "status" : config.lang.duplicate_email
                                    });
                                } else {
                                    updateData['mailAddress'] = mailAddress;
                                    // updateData['userName']  = userName;
                                    updateData['empNo'] = empNo;
                                    updateData['fullName'] = fullName;
                                    updateData['industries'] = industries;
                                    // updateData['occupations'] = occupations;
                                    updateData['sitens'] = sitens;
                                    updateData['foNaFlg'] = foNaFlg;
                                    // console.log(updateData);
                                    if( newPassword !== '' && confirmPassword !== '' ) {
                                        if(  
                                            ( newPassword.length     <  6  ||  newPassword.length     > 20 ) || 
                                            ( confirmPassword.length <  6  ||  confirmPassword.length > 20 ) 
                                            ){
                                            res.status(400).json({
                                                "nonce": new Date().getTime(),
                                                "status" : config.lang.password_min_six_char
                                            });
                                        } else if ( newPassword !== confirmPassword ) {
                                            res.status(400).json({
                                                "nonce": new Date().getTime(),
                                                "status" : config.lang.password_not_matched
                                            });
                                        } else {
                                            //========= New password & confirm password ===========================
                                            const PASSWORD_SALT_WORDS = "0123456789abcdef";
                                            let newPasswordLength = PASSWORD_SALT_WORDS.length;
                                            let newPasswordSalt = "";
                                            for ( var i = 0; i < newPasswordLength; i++ ) {
                                                newPasswordSalt += PASSWORD_SALT_WORDS.charAt( Math.floor( Math.random() * newPasswordLength ) );
                                            }

                                            let newPasswordHashed = newPasswordSalt + crypto.createHash('sha256').update(confirmPassword + newPasswordSalt).digest('hex');
                                            // console.log(newPasswordHashed);
                                            let agencyModule = req.app.get("agencyModule");
                                            if ( userInfo["account_attribute"] === "agency" ) {
                                                agencyModule.agencyInfoUpdate( agencyId, updateData, newPasswordHashed, ( SQLResult ) => {
                                                    let result = SQLResult.rows;
                                                    if( result.affectedRows === 1 ){
                                                        res.status(200).json({
                                                            "nonce": new Date().getTime(),
                                                            "status" : config.lang.update_success
                                                        });
                                                    }else{
                                                        res.status(400).json({
                                                            "nonce": new Date().getTime(),
                                                            "status" : "Password not updated yet ( May be query problem )"
                                                        });
                                                    }
                                                });
                                            } else {
                                                res.status(403).json({
                                                    "nonce": new Date().getTime(),
                                                    "status" : config.lang.access_denied
                                                });
                                            }

                                            //========= New password & Old password ===========================

                                            // let agencyModule = req.app.get("agencyModule");
                                            // agencyModule.getAgencyInfo(agencyId, (SQLResult) => {
                                            //     if (SQLResult === undefined) {
                                            //         res.status(403).json({
                                            //             "nonce": new Date().getTime(),
                                            //             "status" : "Access Denied"
                                            //         });
                                            //     } else {
                                            //         let password = SQLResult.rows[0]["f_password"];
                                            //         let oldPasswordSalt = password.substring(0, 16);
                                            //         let oldPasswordHashed = oldPasswordSalt + crypto.createHash('sha256').update(oldPassword + oldPasswordSalt).digest('hex');
                                            //         if( password === oldPasswordHashed ) {
                                            //             const PASSWORD_SALT_WORDS = "0123456789abcdef";
                                            //             let newPasswordLength = PASSWORD_SALT_WORDS.length;
                                            //             let newPasswordSalt = "";
                                            //             for (var i = 0; i < newPasswordLength; i++) {
                                            //                 newPasswordSalt += PASSWORD_SALT_WORDS.charAt( Math.floor( Math.random() * newPasswordLength ) );
                                            //             }
                                
                                            //             let newPasswordHashed = newPasswordSalt + crypto.createHash('sha256').update(newPassword + newPasswordSalt).digest('hex');
                                            //             let agencyModule = req.app.get("agencyModule");

                                            //             agencyModule.agencyInfoUpdate(agencyId, updateData, newPasswordHashed, (SQLResult) => {
                                            //                 let result = SQLResult.rows;
                                            //                 if(result.affectedRows === 1){
                                            //                     res.status(200).json({
                                            //                         "nonce": new Date().getTime(),
                                            //                         "status" : "Data Updated Successfully"
                                            //                     });
                                            //                 }else{
                                            //                     res.status(400).json({
                                            //                         "nonce": new Date().getTime(),
                                            //                         "status" : "Password not updated yet"
                                            //                     });
                                            //                 }
                                            //             });
                                
                                            //         } else {
                                            //             res.status(400).json({
                                            //                 "nonce": new Date().getTime(),
                                            //                 "status" : "Old password is not correct."
                                            //             });
                                            //         }
                                            //     }
                                            // });
                                        }
                                    } else if( newPassword !== '' && confirmPassword === ''){
                                        res.status(400).json({
                                            "nonce": new Date().getTime(),
                                            "status" : config.lang.password_not_matched
                                        });
                                    } else if( newPassword === '' && confirmPassword !== ''){
                                        res.status(400).json({
                                            "nonce": new Date().getTime(),
                                            "status" : config.lang.password_not_matched
                                        });
                                    } else if( sitens.length > 6 ) {
                                        res.status(400).json({
                                            "nonce": new Date().getTime(),
                                            "status" : "Error: sitens not more then 3"
                                        });
                                    } else {
                                        let agencyModule = req.app.get( "agencyModule" );
                                        agencyModule.getAgencyInfo( agencyId, ( SQLResult ) => {
                                            let countResult = SQLResult.rows.length
                                            if ( countResult === 0 ) {
                                                res.status(403).json({
                                                    "nonce": new Date().getTime(),
                                                    "status" : config.lang.access_denied
                                                });
                                            } else {
                                                if ( userInfo["account_attribute"] === "agency" ) {
                                                    let password = SQLResult.rows[0]["f_password"];
                                                    agencyModule.agencyInfoUpdate( agencyId, updateData, password, ( SQLResult ) => {
                                                        let result = SQLResult.rows;
                                                        if( result.affectedRows === 1 ){
                                                            res.status(200).json({
                                                                "nonce": new Date().getTime(),
                                                                "status" : config.lang.update_success
                                                            });
                                                        }else{
                                                            res.status(400).json({
                                                                "nonce": new Date().getTime(),
                                                                "status" : "Error: Data not updated"
                                                            });
                                                        }
                                                    });
                                                } else {
                                                    res.status(403).json({
                                                        "nonce": new Date().getTime(),
                                                        "status" : config.lang.access_denied
                                                    });
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    } else {
                        res.status(400).json({
                            "nonce": new Date().getTime(),
                            "status" : config.lang.email_field_empty
                        });
                    }
                }else{
                    res.status(400).json({
                        "nonce": new Date().getTime(),
                        "status" : config.lang.data_not_valid
                    });
                }
            } catch ( error ) {
                res.status(400).json({
                    "nonce": new Date().getTime(),
                    "status" : "error:"+ error
                });
            }
        }
    } catch ( error ) {
        res.status(500).json({
            "nonce": new Date().getTime(),
            "status" : "error: " + error
        });
    }
});

//==========================================================================
//                      Agency Employee My Top  Page
//==========================================================================

//============ Agency Employees list View Route ==============
router.post("/mytop-page-employee-list", ( req, res, next ) => {
    let nonce = new Date().getTime();
    try {
        
        let tokenID = req.body["token"];
        if(
            tokenID === undefined || typeof tokenID !== "string" || tokens[tokenID] === undefined
        ) {
            res.status(401).json({
                "nonce": new Date().getTime(),
                "status" : "unauthorized"
            });
        } else {
            try {

                let userInfo        =   tokens[tokenID];
                let parentId        =   userInfo["id"];
                let userType        =   userInfo["account_attribute"];
                let agencyModule    =   req.app.get("agencyModule");

                agencyModule.employeeList( parentId, ( SQLResult ) => {
                    let results = SQLResult.rows;
                    let countResult = results.length;
                    if ( results  === undefined ) {
                        res.status(403).json({
                            "nonce": new Date().getTime(),
                            "status" : config.lang.access_denied
                        });
                    } else {
                        var resArr = [];
                        try {
                            for( let i=0; i<countResult; i++ ) {
                                resArr[i] = {
                                    "agency_id": parseInt(results[i]["f_agency_id"]),
                                    "full_name": results[i]["f_name2"],
                                    "mailaddress": "" + results[i]["f_mailaddress"],
                                    "is_enable": parseInt(results[i]["f_acct_valid_flg"])
                                }
                            }
                        } catch ( error ) {
                            res.status(500).json({
                                "nonce": new Date().getTime(),
                                "status" : "error: " + error
                            });
                        }
                        //Empty result/array check
                        try{
                            if ( Array.isArray(resArr) == true && resArr.length === 0 ) {
                                let u = {
                                    "nonce": new Date().getTime(),
                                    "status" : config.lang.no_employ_found,
                                };
                                res.status(404).json(u);
                            } else {
                                
                                let u = {
                                    "nonce": new Date().getTime(),
                                    "status" : "ok",
                                    "payload" : resArr
                                };
                                res.status(200).json(u);
                            }
                        }catch( error ){
                            res.status(500).json({
                                "nonce": new Date().getTime(),
                                "status" : "error: " + error
                            });
                        }
                    }
                });
            } catch ( error ) {
                res.status(500).json({
                    "nonce": new Date().getTime(),
                    "status" : "error: " + error
                });
            }
        }
    } catch ( error ) {
        res.status(500).json({
            "nonce": new Date().getTime(),
            "status" : "error: " + error
        });
    }
});

//============ Agency Single Employee Edit view ===============
router.post("/mytop-page-employee/:employee_id", ( req, res, next ) => {
    try {
        let nonce = new Date().getTime();
        let tokenID = req.body["token"];
        if(
            tokenID === undefined || typeof tokenID !== "string" || tokens[tokenID] === undefined
        ) {
            res.status(401).json({
                "nonce": new Date().getTime(),
                "status" : "unauthorized"
            });
        } else {
            let empoloyeeId = parseInt(req.params.employee_id, 10);
            if( isNaN(empoloyeeId) === true ) {
                res.status(400).json({
                    "nonce": new Date().getTime(),
                    "status" : "error: Employee ID is not number"
                });
            } else {
                let userInfo = tokens[tokenID];
                // let agencyZip = userInfo["zip"];
                let userType = userInfo["account_attribute"];
                // console.log(parentId);
                let agencyModule = req.app.get( "agencyModule" );
                agencyModule.employeeInfo( empoloyeeId, ( SQLResult ) => {
                    let u = SQLResult.rows[0];
                    if ( u === undefined ) {
                        res.status(403).json({
                            "nonce" : nonce,
                            "status" : config.lang.access_denied
                        });
                    } else {
                        try {
                            if( userType === "agency" ) {
                                agencyModule.getAllOccupation( ( occupations ) => {
                                    occupations = occupations.rows;
                                    let countOccupations= occupations.length;
                                    var occupationArr = [];
        
                                    for( let i=0; i<countOccupations; i++ ) {
                                        occupationArr.push({
                                            "id": occupations[i]["id"],
                                            "name": occupations[i]["name"],
                                            "value": false
                                        })
                                    }
        
                                    var isEnableOccupation = [];
                                    if( u["f_occupation"] !== undefined && typeof u["f_occupation"] === "string" ) {
                                        isEnableOccupation = u["f_occupation"].split(',');
                                    }
                                    
                                    if( Array.isArray(isEnableOccupation) === true ) {
                                        for(i=0; i<isEnableOccupation.length; i++) {
                                            let index = occupationArr.findIndex((item)=> {
                                                return item.id === parseInt(isEnableOccupation[i]);
                                            });
                                            if(index !== -1) {
                                                occupationArr[index]["value"] = true;
                                            }
                                        }
                                    }

                                    let parentName = userInfo["parent_name"];
                                    let parentNameKana = userInfo["name_kana"];
                                    let parentAgencyCd = userInfo["code"];
                                    let parentSalesCd = userInfo["sales_cd"];

                                    resRasult = {
                                        "agency_id" : u["f_agency_id"],
                                        "company_name" : parentName ? parentName : '',
                                        "company_name_kana" : parentNameKana ? parentNameKana : '',
                                        "agency_code" : parentAgencyCd ? parentAgencyCd : '',
                                        "sales_cd" : parentSalesCd ? parentSalesCd : '',
                                        "full_name" : u["f_name2"],
                                        "mailaddress" : "" + u["f_mailaddress"],
                                        "user_id" : u["f_id"],
                                        "acct_valid_flg": u["f_acct_valid_flg"] ? parseInt(u["f_acct_valid_flg"]) : 0,
                                        "foreign_nationality_flg" : u["foreign_nationality_flg"] ? parseInt(u["foreign_nationality_flg"]): 0 ,
                                        "foreign_country" : u["f_foreign_country"] ?  "" + u["f_foreign_country"] : "",
                                        "occupations" : occupationArr
                                    }
                                    res.status(200).json({
                                        "nonce" : nonce,
                                        "status" : "ok",
                                        "payload" : [{
                                            "employeeInfo" : resRasult
                                        }]
                                    });
                                });
                            }else {
                                res.status(403).json({
                                    "nonce" : nonce,
                                    "status" : config.lang.access_denied
                                });
                            }
                        } catch ( error ) {
                            res.status(500).json({
                                "nonce": new Date().getTime(),
                                "status" : "error: " + error
                            });
                        }   
                    }
                });
            }
        }
    } catch ( error ) {
        res.status(500).json({
            "nonce": new Date().getTime(),
            "status" : "error: " + error
        });
    }
});

//============ Agency Employee Update ===========
router.post("/mytop-page-employee-update", ( req, res, next ) => {
    try {        
        let nonce = new Date().getTime();
        let tokenID = req.body["token"];
        if(
            tokenID === undefined || typeof tokenID !== "string" || tokens[tokenID] === undefined
        ) {
            res.status(401).json({
                "nonce": new Date().getTime(),
                "status" : "unauthorized"
            });
        } else {
            try {
                let userInfo        =   tokens[tokenID];
                let userType        =   userInfo["account_attribute"];
                let payload         =   req.body["payload"];
                
                let agencyId        =   payload["agency_id"];
                let fullName        =   payload["full_name"];
                // let mailAddress =   payload["mailaddress"];
                let userName        =   payload["user_id"];
                let acctValidFlg    =   payload["acct_valid_flg"];
                let foNaFlg         =   payload["foreign_nationality_flg"];
                let foreignCountry  =   payload["foreign_country"];
                let occupations     =   payload["occupations"];
                let newPassword     =   payload["new_password"];
                let confirmPassword =   payload["old_password"];
                if(
                    ( agencyId        !==  undefined  &&  typeof  agencyId        ===   "number" ) && 
                    ( fullName        !==  undefined  &&  typeof  fullName        ===   "string" ) && 
                    // mailAddress !==  undefined  && typeof mailAddress   === "string" && 
                    ( userName        !==  undefined  &&  typeof  userName        ===   "string" ) &&
                    ( acctValidFlg    !==  undefined  &&  typeof  acctValidFlg    ===   "number" ) &&
                    ( foNaFlg         !==  undefined  &&  typeof  foNaFlg         ===   "number" ) &&
                    ( foreignCountry  !==  undefined  &&  typeof  foreignCountry  ===   "string" ) &&
                    ( occupations     !==  undefined  &&  typeof  occupations     ===   "string" ) && 
                    ( newPassword     !==  undefined  &&  typeof  newPassword     ===   "string" ) && 
                    ( confirmPassword !==  undefined  &&  typeof  confirmPassword ===   "string" )
                    
                ){
                    let updateData = {};
                    if ( fullName === '' ) {
                        res.status(400).json({
                            "nonce": new Date().getTime(),
                            "status" : config.lang.name_field_empty
                        });
                    } else if ( userName === '' ) {
                        res.status(400).json({
                            "nonce": new Date().getTime(),
                            "status" : config.lang.id_empty
                        });
                    } else {
                        new Promise( ( resolve, reject ) => {
                            let isValidEmail = utility.validateEmail(userName);
                            if ( isValidEmail === true ) {
                                let mailaddress = userName;
                                let agencyModule = req.app.get("agencyModule");
                                agencyModule.uniqueEmailCheck(agencyId, mailaddress, (SQLResult) => {
                                    let result = SQLResult.rows;
                                    if (result.length > 0) {
                                        return reject({
                                            "status": 409,
                                            "message" : config.lang.duplicate_email
                                        });
                                    }else{
                                        return resolve({
                                            "mailAddress": userName,
                                            "userName": userName,
                                        }); 
                                    }
                                });
                            } else {
                                let isValidID = utility.alphaNumericCheck(userName);
                                // console.log(isValidID);

                                if ( isValidID === false || true) {
                                    return reject({
                                        "status": 400,
                                        "message" : config.lang.invalid_id_field_data
                                    });
                                } else {
                                    let agencyModule = req.app.get("agencyModule");
                                    agencyModule.uniqueIdCheck(agencyId, userName, (SQLResult) => {
                                        let result = SQLResult.rows;
                                        if (result.length > 0) {
                                            return reject({
                                                "status": 409,
                                                "message" : config.lang.duplicate_id
                                            });
                                        }else{
                                            return resolve({
                                                "mailAddress": null,
                                                "userName": userName,
                                            });
                                        }
                                    });
                                }
                            }
                        })
                        .then( ( data ) => {
                            if(foNaFlg === 1 && foreignCountry === '' ){
                                res.status(400).json({
                                    "nonce": new Date().getTime(),
                                    "status" : config.lang.country_empty
                                });
                            } else {

                                if (foNaFlg === 1){
                                    updateData['foNaFlg']   =   foNaFlg;
                                }else{
                                    updateData['foNaFlg']   =   0;
                                    foreignCountry = null;
                                }

                                updateData['mailAddress']   = data.mailAddress;
                                updateData['userName']      = data.userName;
                                updateData['agencyId']      = agencyId;
                                updateData['fullName']      = fullName;
                                updateData['acctValidFlg']    = acctValidFlg;
                                updateData['foreignCountry']  = foreignCountry;
                                updateData['occupations']   = occupations;
                                // console.log(updateData);
                                if( newPassword !== '' && confirmPassword !== '' ) {
                                    // Equal check new password  = confirm password 
                                    if(  
                                        ( newPassword.length     <  6  ||  newPassword.length     > 20 ) || 
                                        ( confirmPassword.length <  6  ||  confirmPassword.length > 20 ) 
                                        ){
                                        res.status(400).json({
                                            "nonce": new Date().getTime(),
                                            "status" : config.lang.password_min_six_char
                                        });
                                    } else if (newPassword !== confirmPassword) {
                                        res.status(400).json({
                                            "nonce": new Date().getTime(),
                                            "status" : config.lang.password_not_matched
                                        });
                                    } else {
                                        //========= New password & confirm password ===========================
                                        const PASSWORD_SALT_WORDS = "0123456789abcdef";
                                        let newPasswordLength = PASSWORD_SALT_WORDS.length;
                                        let newPasswordSalt = "";
                                        for (var i = 0; i < newPasswordLength; i++) {
                                            newPasswordSalt += PASSWORD_SALT_WORDS.charAt( Math.floor( Math.random() * newPasswordLength ) );
                                        }

                                        let newPasswordHashed = newPasswordSalt + crypto.createHash('sha256').update(confirmPassword + newPasswordSalt).digest('hex');
                                        // console.log(newPasswordHashed);
                                        if(userType == "agency"){
                                            let agencyModule = req.app.get("agencyModule");
                                            agencyModule.employeeInfoUpdate(agencyId, updateData, newPasswordHashed, (SQLResult) => {
                                                let result = SQLResult.rows;
                                                if(result.affectedRows === 1){
                                                    res.status(200).json({
                                                        "nonce": new Date().getTime(),
                                                        "status" : config.lang.update_success
                                                    });
                                                }else{
                                                    res.status(400).json({
                                                        "nonce": new Date().getTime(),
                                                        "status" : "Password not updated yet"
                                                    });
                                                }
                                            });
                                        } else {
                                            res.status(403).json({
                                                "nonce": new Date().getTime(),
                                                "status" : config.lang.access_denied
                                            });
                                        }
                                    }
                                    //============== New password & old password =================
                                    // let agencyModule = req.app.get("agencyModule");
                                    // agencyModule.getAgencyInfo(agencyId, (SQLResult) => {
                                    //     if (SQLResult ===  undefined) {
                                    //         res.status(403).json({
                                    //             "nonce": new Date().getTime(),
                                    //             "status" : "Access Denied"
                                    //         });
                                    //     } else {
                                    //         let password = SQLResult.rows[0]["f_password"];
                                    //         let oldPasswordSalt = password.substring(0, 16);
                                    //         let oldPasswordHashed = oldPasswordSalt + crypto.createHash('sha256').update(oldPassword + oldPasswordSalt).digest('hex');
                                    //         if( password === oldPasswordHashed ) {
                                    //             const PASSWORD_SALT_WORDS = "0123456789abcdef";
                                    //             let newPasswordLength = PASSWORD_SALT_WORDS.length;
                                    //             let newPasswordSalt = "";
                                    //             for (var i = 0; i < newPasswordLength; i++) {
                                    //                 newPasswordSalt += PASSWORD_SALT_WORDS.charAt( Math.floor( Math.random() * newPasswordLength ) );
                                    //             }

                                    //             let newPasswordHashed = newPasswordSalt + crypto.createHash('sha256').update(newPassword + newPasswordSalt).digest('hex');
                                    //             let agencyModule = req.app.get("agencyModule");
                                    //             agencyModule.agencyInfoUpdate(agencyId, updateData, newPasswordHashed, (SQLResult) => {
                                    //                 let result = SQLResult.rows;
                                    //                 if(result.affectedRows === 1){
                                    //                     res.status(200).json({
                                    //                         "nonce": new Date().getTime(),
                                    //                         "status" : "Your password Updated Successfully"
                                    //                     });
                                    //                 }else{
                                    //                     res.status(400).json({
                                    //                         "nonce": new Date().getTime(),
                                    //                         "status" : "Password not updated yet"
                                    //                     });
                                    //                 }
                                    //             });
                        
                                    //         } else {

                                    //             res.status(400).json({
                                    //                 "nonce": new Date().getTime(),
                                    //                 "status" : "Your old password is not correct."
                                    //             });
                                    //         }
                                    //     }
                                    // });
                                } else if( newPassword !== '' && confirmPassword === ''){
                                    res.status(400).json({
                                        "nonce": new Date().getTime(),
                                        "status" : config.lang.password_not_matched
                                    });
                                } else if( newPassword === '' && confirmPassword !== ''){
                                    res.status(400).json({
                                        "nonce": new Date().getTime(),
                                        "status" : config.lang.password_not_matched
                                    });
                                } else {
                                    try {
                                        if(userType == "agency"){
                                            let agencyModule = req.app.get("agencyModule");
                                            agencyModule.getAgencyInfo(agencyId, (SQLResult) => {
                                                let countResult = SQLResult.rows.length
                                                //console.log(countResult);
                                                if (countResult === 0) {
                                                    res.status(403).json({
                                                        "nonce": new Date().getTime(),
                                                        "status" : config.lang.access_denied
                                                    });
                                                } else {
                                                    let password = SQLResult.rows[0]["f_password"];
                                                    agencyModule.employeeInfoUpdate(agencyId, updateData, password, (SQLResult) => {
                                                        let result = SQLResult.rows;
                                                        if(result.affectedRows === 1){
                                                            res.status(200).json({
                                                                "nonce": new Date().getTime(),
                                                                "status" : config.lang.update_success
                                                            });
                                                        }else{
                                                            res.status(400).json({
                                                                "nonce": new Date().getTime(),
                                                                "status" : "Error: Data not updated"
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }else{
                                            res.status(403).json({
                                                "nonce": new Date().getTime(),
                                                "status" : config.lang.access_denied
                                            });
                                        }
                                    } catch (error) {
    
                                        res.status(500).json({
                                            "nonce": new Date().getTime(),
                                            "status" : "error: " + error
                                        });
                                    }
                                }
                            }
                        })
                        .catch( ( error ) => {
                            res.status(error.status).json({
                                "nonce": new Date().getTime(),
                                "status" : error.message
                            });
                        });
                    }
                }else{
                    res.status(400).json({
                        "nonce": new Date().getTime(),
                        "status" : config.lang.data_not_valid
                    });
                }
            } catch ( error ) {
                res.status(400).json({
                    "nonce": new Date().getTime(),
                    "status" : "Error: Data not found " + error
                });
            }
        }
    } catch ( error ) {
        res.status(500).json({
            "nonce": new Date().getTime(),
            "status" : "error: " + error
        });
    }
});

//============ Agency Employee Update ===========
router.post("/mytop-page-employee-delete", ( req, res, next ) => {
    try {
        let nonce = new Date().getTime();
        let tokenID = req.body["token"];
        if(
            tokenID === undefined || typeof tokenID !== "string" || tokens[tokenID] === undefined
        ) {
            res.status(401).json({
                "nonce": new Date().getTime(),
                "status" : "unauthorized"
            });
        } else {
            try {

                let userInfo    =   tokens[tokenID];
                let agencyId    =   userInfo["id"];
                let userType    =   userInfo["account_attribute"];
                let payload     =   req.body["payload"];
                let employeeId  =   payload["agency_id"];
            
                if(
                    ( agencyId    !==  undefined  && typeof agencyId     === "number" ) && 
                    ( employeeId  !==  undefined  && typeof employeeId   === "number" ) && 
                    ( userType    !==  undefined  && typeof userType     === "string" )
                ){
                    if ( agencyId !== '' && employeeId !== '' && userType !== '' ) {
                        try {
                            if( userType == "agency" ){
                                let agencyModule = req.app.get("agencyModule");
                                agencyModule.employeeDelete( agencyId, employeeId, ( SQLResult ) => {
                                    let result = SQLResult.rows;
                                    if ( result === undefined ) {
                                        res.status(403).json({
                                            "nonce": new Date().getTime(),
                                            "status" : config.lang.access_denied
                                        });
                                    } else {
                                        if(result.affectedRows === 1){
                                            res.status(200).json({
                                                "nonce": new Date().getTime(),
                                                "status" : config.lang.delete_success
                                            });
                                        }else{
                                            res.status(500).json({
                                                "nonce": new Date().getTime(),
                                                "status" : "Error: Data not deleted"
                                            });
                                        }
                                    }
                                });
                            }else{
                                res.status(403).json({
                                    "nonce": new Date().getTime(),
                                    "status" : config.lang.access_denied
                                });
                            }
                        } catch (error) {

                            res.status(500).json({
                                "nonce": new Date().getTime(),
                                "status" : "error: " + error
                            });
                        }
                    } else {

                        res.status(400).json({
                            "nonce": new Date().getTime(),
                            "status" : "Emplyee ID is Empty"
                        });
                    }
                }else{
                    res.status(400).json({
                        "nonce": new Date().getTime(),
                        "status" : config.lang.data_not_valid
                    });
                }
            } catch ( error ) {
                res.status(400).json({
                    "nonce": new Date().getTime(),
                    "status" : "Error: Data not found " + error
                });
            }
        }
    } catch ( error ) {
        res.status(500).json({
            "nonce": new Date().getTime(),
            "status" : "error: " + error
        });
    }
});

//==========================================================================
//                      Register My Top  Page
//==========================================================================

//============ Register View route ==============
router.post("/mytop-page-register", ( req, res, next ) => {
    let nonce = new Date().getTime();
    try {
        let agencyModule = req.app.get("agencyModule");
        let tokenID      = req.body["token"];
        
        if(
            tokenID === undefined || typeof tokenID !== "string" || tokens[tokenID] === undefined
        ) {
            res.status(401).json({
                "nonce": new Date().getTime(),
                "status" : "unauthorized"
            });
        } else {

            let userInfo = tokens[tokenID];
            let agencyId = userInfo["id"];
            let userType = userInfo["account_attribute"];

            agencyModule.getAgencyInfo( agencyId, ( SQLResult ) => {
                let u = SQLResult.rows[0];
                if ( u === undefined ) {
                    res.status(403).json({
                        "nonce": new Date().getTime(),
                        "status" : config.lang.access_denied
                    });
                } else {
                    if( userType === "register" ) {
                        let parentId = u["f_parent_id"];
                        agencyModule.agencyParentInfo( parentId, ( SQLResult ) => {
                            let result = SQLResult.rows[0];
                            let userInfo = {
                                "agency_id" : u["f_agency_id"],
                                "company_name" : result ? result["f_parent_name"] : "",
                                "company_name_name" : result ? result["f_parent_name_kana"] : "",
                                "agency_code" : result ? result["f_parent_agency_cd"] : "",
                                "sales_code" : result ? result["f_parent_sales_cd"] : "",
                                "full_name" : u["f_name2"],
                                "user_id" : u["f_id"],
                                "division" : u["f_id"],
                                "employee_category" : parseInt(u["f_agency_type"]),
                                "foreign_nationality_flg" : u["foreign_nationality_flg"] ? parseInt(u["foreign_nationality_flg"]): 0 ,
                                "foreign_country" : u["f_foreign_country"] ? u["f_foreign_country"] : "",
                                "office_address" : [{
                                    "office"    :   "本部事務局",
                                    "address"   :   "〒108-8211　東京都港区港南2-16-1",
                                    "tel"       :   "(03) - 6718 - 9250",
                                    "fax"       :   "(03) - 6718 - 9251",
                                },
                                {
                                    "office"    :   "持株会事務局（大東コーポレートサービス　シェアードサービス部）",
                                    "address"   :   "〒140-0002　東京都品川区東品川2-2-8　スフィアタワー天王洲",
                                    "tel"       :   "(03) - 6718 - 9056",
                                    "fax"       :   "(03) - 6718 - 9057",
                                }]
                            }
                            res.status(200).json({
                                "nonce" : nonce,
                                "status" : "ok",
                                "payload" : [{
                                    "userInfo" : userInfo
                                }]
                            });
                        });
                    } else {
                        res.status(403).json({
                            "nonce": new Date().getTime(),
                            "status" : config.lang.access_denied
                        });
                    }    
                }
            });
        }
    } catch (error) {
        res.status(500).json({
            "nonce": new Date().getTime(),
            "status" : "error :" + error
        });
    }
});

//============ Register Update Route ===========
router.post("/mytop-page-register-update", ( req, res, next ) => {
    try {
        let nonce   = new Date().getTime();
        let tokenID = req.body["token"];
        if(
            tokenID === undefined || typeof tokenID !== "string" || tokens[tokenID] === undefined
        ) {
            res.status(401).json({
                "nonce": new Date().getTime(),
                "status" : "unauthorized"
            });
        } else {
            try {
                let userInfo    =   tokens[tokenID];
                let agencyId    =   userInfo["id"];
                let payload     =   req.body["payload"];

                let fullName        =   payload["full_name"];
                let userId          =   payload["user_id"];
                let agencyType      =   payload["employee_category"];
                let foreignCountry  =   payload["foreign_country"];
                let foNaFlg         =   payload["foreign_nationality_flg"];
                let newPassword     =   payload["new_password"];
                let confirmPassword =   payload["old_password"];

                if(
                    ( fullName        !==    undefined   &&  typeof  fullName        === "string" ) && 
                    ( userId          !==    undefined   &&  typeof  userId          === "string" ) && 
                    ( agencyType      !==    undefined   &&  typeof  agencyType      === "number" ) && 
                    ( foNaFlg         !==    undefined   &&  typeof  foNaFlg         === "number" ) && 
                    ( foreignCountry  !==    undefined   &&  typeof  foreignCountry  === "string" ) &&
                    ( newPassword     !==    undefined   &&  typeof  newPassword     === "string" ) && 
                    ( confirmPassword !==    undefined   &&  typeof  confirmPassword === "string" )
                    
                ){
                    let updateData = {};
                    if ( fullName === '' ) {
                        res.status(400).json({
                            "nonce": new Date().getTime(),
                            "status" : config.lang.name_field_empty
                        });
                    } else if ( userId === '' ) {
                        res.status(400).json({
                            "nonce": new Date().getTime(),
                            "status" : config.lang.id_empty
                        }); 
                    } else {
                        new Promise( ( resolve, reject ) => {
                            let isValidEmail = utility.validateEmail(userId);
                            if ( isValidEmail === true ) {
                                let mailaddress = userId;
                                let agencyModule = req.app.get( "agencyModule" );
                                agencyModule.uniqueEmailCheck( agencyId, mailaddress, ( SQLResult ) => {
                                    let result = SQLResult.rows;
                                    if ( result.length > 0 ) {
                                        return reject({
                                            "status": 409,
                                            "message" : config.lang.duplicate_email
                                        });
                                    }else{
                                        return resolve({
                                            "mailAddress": userId,
                                            "userId": userId,
                                        }); 
                                    }
                                });
                            } else {
                                let isIDalphaNumeric = utility.alphaNumericCheck(userId);
                                if ( isIDalphaNumeric === false || true) {
                                    return reject({
                                        "status": 400,
                                        "message" : config.lang.invalid_id_field_data
                                    });
                                } else {
                                    let agencyModule = req.app.get("agencyModule");
                                    agencyModule.uniqueIdCheck( agencyId, userId, ( SQLResult ) => {
                                        let result = SQLResult.rows;
                                        if ( result.length > 0 ) {
                                            return reject({
                                                "status": 409,
                                                "message" : config.lang.duplicate_id
                                            });
                                        }else{
                                            return resolve({
                                                "mailAddress": null,
                                                "userId": userId,
                                            });
                                        }
                                    });
                                }
                            }
                        })
                        .then( ( data ) => {
                            if( foNaFlg === 1 && foreignCountry === '' ){
                                res.status(400).json({
                                    "nonce": new Date().getTime(),
                                    "status" : config.lang.country_empty
                                });
                            } else {
                                if ( foNaFlg === 1 ){
                                    updateData['foNaFlg']   =   foNaFlg;
                                }else{
                                    updateData['foNaFlg']   =   0;
                                    foreignCountry = null;
                                }
                                updateData['fullName']      = fullName;
                                updateData['userId']        = data.userId;
                                updateData['mailAddress']   = data.mailAddress;
                                updateData['agencyType']    = agencyType;
                                updateData['foNaFlg']       = foNaFlg;
                                updateData['foreignCountry'] = foreignCountry;
                                // console.log(updateData);
                                
                                if( newPassword !== '' && confirmPassword !== '' ) {
                                    // Equal check new password  and confirm password 
                                    if(  
                                        ( newPassword.length     <  6  ||  newPassword.length     > 20 ) || 
                                        ( confirmPassword.length <  6  ||  confirmPassword.length > 20 ) 
                                        ){
                                        res.status(400).json({
                                            "nonce": new Date().getTime(),
                                            "status" : config.lang.password_min_six_char
                                        });
                                    } else if ( newPassword !== confirmPassword ) {
                                        res.status(400).json({
                                            "nonce": new Date().getTime(),
                                            "status" : config.lang.password_not_matched
                                        });
                                    } else {
                                        //========= New password & confirm password ===========================
                                        const PASSWORD_SALT_WORDS = "0123456789abcdef";
                                        let newPasswordLength = PASSWORD_SALT_WORDS.length;
                                        let newPasswordSalt = "";
                                        for ( var i = 0; i < newPasswordLength; i++ ) {
                                            newPasswordSalt += PASSWORD_SALT_WORDS.charAt( Math.floor( Math.random() * newPasswordLength ) );
                                        }
    
                                        let newPasswordHashed = newPasswordSalt + crypto.createHash('sha256').update(confirmPassword + newPasswordSalt).digest('hex');
                                        // console.log(newPasswordHashed);
                                        let agencyModule = req.app.get("agencyModule");
                                        if ( userInfo["account_attribute"] === "register" ) {
                                            agencyModule.agencyInfoUpdate( agencyId, updateData, newPasswordHashed, ( SQLResult ) => {
                                                let result = SQLResult.rows;
                                                if( result.affectedRows === 1 ){
                                                    res.status(200).json({
                                                        "nonce": new Date().getTime(),
                                                        "status" : config.lang.update_success
                                                    });
                                                }else{
                                                    res.status(400).json({
                                                        "nonce": new Date().getTime(),
                                                        "status" : "Password not updated yet ( query problem )"
                                                    });
                                                }
                                            });
                                        } else {
                                            res.status(403).json({
                                                "nonce": new Date().getTime(),
                                                "status" : config.lang.access_denied
                                            });
                                        }
                                    }
                                    //========= new password and old password ==================
                                    // let agencyModule = req.app.get("agencyModule");
                                    // agencyModule.getAgencyInfo(agencyId, (SQLResult) => {
                                    //     let password = SQLResult.rows[0]["f_password"];
                                    //     let oldPasswordSalt = password.substring(0, 16);
                                    //     let oldPasswordHashed = oldPasswordSalt + crypto.createHash('sha256').update(oldPassword + oldPasswordSalt).digest('hex');
                                        
                                    //     if( password === oldPasswordHashed ) {
    
                                    //         const PASSWORD_SALT_WORDS = "0123456789abcdef";
                                    //         let newPasswordLength = PASSWORD_SALT_WORDS.length;
                                    //         let newPasswordSalt = "";
                                    //         for (var i = 0; i < newPasswordLength; i++) {
                                    //             newPasswordSalt += PASSWORD_SALT_WORDS.charAt( Math.floor( Math.random() * newPasswordLength ) );
                                    //         }
                    
                                    //         let newPasswordHashed = newPasswordSalt + crypto.createHash('sha256').update(newPassword + newPasswordSalt).digest('hex');
                                    //         let agencyModule = req.app.get("agencyModule");
    
                                    //         agencyModule.registerInfoUpdate(agencyId, updateData, newPasswordHashed, (SQLResult) => {
                                    //             let result = SQLResult.rows;
                                    //             if(result.affectedRows === 1){
                                    //                 res.status(200).json({
                                    //                     "nonce": new Date().getTime(),
                                    //                     "status" : "Your password Updated Successfully"
                                    //                 });
                                    //             }else{
                                    //                 res.status(400).json({
                                    //                     "nonce": new Date().getTime(),
                                    //                     "status" : "Password not updated yet"
                                    //                 });
                                    //             }
                                    //         });
                    
                                    //     } else {
    
                                    //         res.status(400).json({
                                    //             "nonce": new Date().getTime(),
                                    //             "status" : "Your old password is not correct."
                                    //         });
                                    //     }
                                    // });
                                } else if( newPassword !== '' && confirmPassword === '' ){
                                    res.status(400).json({
                                        "nonce": new Date().getTime(),
                                        "status" : config.lang.password_not_matched
                                    });
                                } else if( newPassword === '' && confirmPassword !== '' ){
                                    res.status(400).json({
                                        "nonce": new Date().getTime(),
                                        "status" : config.lang.password_not_matched
                                    });
                                } else {
                                    let agencyModule = req.app.get("agencyModule");
                                    agencyModule.getAgencyInfo( agencyId, ( SQLResult ) => {
                                        let countResult = SQLResult.rows.length
                                        if (countResult === 0) {
                                            res.status(403).json({
                                                "nonce": new Date().getTime(),
                                                "status" : config.lang.access_denied
                                            });
                                        } else {
                                            if ( userInfo["account_attribute"] === "register" ) {
                                                let password = SQLResult.rows[0]["f_password"];
                                                agencyModule.registerInfoUpdate( agencyId, updateData, password, ( SQLResult ) => {
                                                    let result = SQLResult.rows;
                                                    if( result.affectedRows === 1 ){
                                                        res.status(200).json({
                                                            "nonce": new Date().getTime(),
                                                            "status" : config.lang.update_success
                                                        });
                                                    }else{
                                                        res.status(400).json({
                                                            "nonce": new Date().getTime(),
                                                            "status" : "Error: Data not updated"
                                                        });
                                                    }
                                                });
                                            } else {
                                                res.status(403).json({
                                                    "nonce": new Date().getTime(),
                                                    "status" : config.lang.access_denied
                                                });
                                            }
                                        }
                                    });
                                }
                            }
                        })
                        .catch( ( error ) => {
                            //console.log(error);
                            res.status(error.status).json({
                                "nonce": new Date().getTime(),
                                "status" : error.message
                            });
                        });
                    }
                } else {
                    res.status(400).json({
                        "nonce": new Date().getTime(),
                        "status" : config.lang.data_not_valid
                    });
                }
            } catch ( error ) {
                res.status(400).json({
                    "nonce": new Date().getTime(),
                    "status" : "error: Data not found"+ error
                });
            }
        }
    } catch ( error ) {
        res.status(500).json({
            "nonce": new Date().getTime(),
            "status" : "error: " + error
        });
    }
});

//==========================================================================
//                      FCM Token
//==========================================================================

//============ Get FCM Token============
router.post("/set-fcm-token", ( req, res, next ) => {
    let payload =   req.body["payload"];
    let tokenID =   req.body["token"];
    let nonce   =   new Date().getTime();
    if(
        ( payload["fcm-token"]  !== undefined && typeof payload["fcm-token"] === "string" ) &&
        ( tokenID !== undefined && typeof tokenID === "string")
    ) {
        let fcm_token =  payload["fcm-token"];
        if( fcm_token === "" ) {
            res.status(400).json({
                "nonce" : nonce,
                "status": "error " + "fcm token is empty"
            });
        } else {
            if( tokens["" + tokenID] === undefined ) {
                res.status(401).json({
                    "nonce" : nonce,
                    "status": "Unauthorized"
                });
            } else {
                for ( let [ key, token ] of Object.entries( tokens ) ) {
                    if( 
                        key                         !== tokenID     &&
                        tokens[key]["fcm-token"]    !== undefined   &&
                        tokens[key]["fcm-token"]    === payload["fcm-token"]
                    ) {
                        try {
                            delete tokens[key];
                        } catch ( error ) {
                        }
                    }
                }
                tokens["" + tokenID]["fcm-token"] = payload["fcm-token"];
                tokenRW.tokenSave();
                res.status(200).json({
                    "nonce" : nonce,
                    "status": "ok"
                });
            }
        }
    } else {
        res.status(400).json({
            "nonce" : nonce,
            "status": "error " + "token or fcm missing"
        });
    }
});

module.exports = router;

