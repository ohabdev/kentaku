var express = require('express');
var router = express.Router();
const moment = require('moment');
const fs = require('fs');

// /* GET content by id */
router.post('/privacypolicyq', function(req, res, next) {
    // Check user valid Auth token
    let userInfo = req.userInfo;
    if(userInfo === undefined && false) {
        res.status(401).json({
            "nonce": new Date().getTime(),
            "status" : "unauthorized"
        });
    } else {
        try {
            let entities = req.app.get("entities");
            let config = req.app.get("config");
            let privacypolicyTitle = "プライバシーポリシー";
            let privacypolicyContent = `<div id="content_left">
                                    <h3 class="h3a">プライバシーポリシー</h3>
                                    <div class="section" id="privacy-policy">
                                        <div class="section">
                                        <h2 class="privacy-policy-title">大東建託協力会<br >個人情報保護方針</h2>
                                        <div class="inner">
                                            <p class="bold-text size-large far" style="font-weight: bold; font-size: 13.2px;">
                                                弊会は、事業活動を展開する中で、個人情報を大切に保護することが社会的使命と認識し、お預かりする個人情報を“お客様の財産そのもの”と捉え、お客様の権利利益の保護を目的として、次に個人情報保護方針を定めて公表し、個人情報の適切な取り扱いを徹底いたします。
                                            </p>
                                            <ol class="numbered">
                                                <li class="bold-text size-large mb15">
                                                    <p>個人情報の取得・利用目的について</p>
                                                    <p class="normal-text size-default">
                                                        弊会は、お客様から個人情報を取得する場合には、その利用目的をあらかじめお知らせ又は公表して、適法かつ公正な手段により取得いたします。また弊会は、お客様の個人情報の利用目的を特定し、その目的の達成に必要な範囲内においてのみ利用します。ご本人から事前のご承諾をいただいた場合や法で定める場合を除き、公表した利用目的以外の利用はいたしません。
                                                    </p>
                                                </li>
                                                <li class="bold-text size-large mb15">
                                                    <p>個人情報の共同利用について</p>
                                                    <p class="normal-text size-default">
                                                        弊会はお客様の個人情報を、弊会グループ企業内において共同して利用いたします。そのため、共同して利用する個人情報の項目、共同利用者名、利用目的をあらかじめ公表し、共同利用上の責任は弊会が負います。
                                                    </p>
                                                </li>
                                                <li class="bold-text size-large mb15">
                                                    <p>個人情報の第三者への提供について</p>
                                                    <p class="normal-text size-default">
                                                        弊会は、お客様の個人情報の取り扱いにおいて、法で定める場合やお客様とのご契約内容の達成に必要な場合等を除き、あらかじめお客様の同意を得ることなく、お客様の個人情報を第三者へ提供いたしません。
                                                    </p>
                                                </li>
                                                <li class="bold-text size-large mb15">
                                                    <p>個人情報の正確性・安全管理について</p>
                                                    <p class="normal-text size-default">
                                                        弊会は、お客様の個人情報の紛失・破壊・改ざん及び漏洩等を予防すべく、組織的・人的・物理的・技術的な安全対策を講じるとともに、必要な是正措置を講じます。また、個人情報の取り扱いを外部へ委託する場合は、委託先との間で取り扱いに関する契約を結ぶなどして適切な監督を行います。
                                                    </p>
                                                </li>
                                                <li class="bold-text size-large mb15">
                                                    <p>個人情報の開示等のご請求について</p>
                                                    <p class="normal-text size-default">
                                                        弊会がお預かりするお客様の個人情報について、お客様から開示・訂正・利用停止・消去及び第三者への提供停止を求められた場合には、法令や社会通念上等に照らし、適切に対応いたします。
                                                    </p>
                                                </li>
                                                <li class="bold-text size-large">
                                                    <p>各種法令等の遵守</p>
                                                    <p class="normal-text size-default">弊会は、個人情報の保護に関する法律及びその他法令、社会的規範等を遵守いたします。</p>
                                                </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>`;

            let  str = `<!DOCTYPE html>
                        <html lang="en">
                            <head>
                                <meta charset="UTF-8">
                                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                                <meta http-equiv="X-UA-Compatible" content="ie=edge">
                                <title>大東建託協力会 | プライバシーポリシー</title>
                                <link href="`+ config.get("url.base_url") + `img/favicon.jpg" type="image/png" rel="icon">
                                <link href="`+ config.get("url.base_url") + `css/common.css" rel="stylesheet" type="text/css" media="all">
                                <link href="`+ config.get("url.base_url") + `css/privacypolicy.css" rel="stylesheet" type="text/css" media="all">
                                <style>
                                    html, body {
                                        
                                        width: inherit !important;
                                        min-width: 10px;
                                    }
                                    #content_left .h3a {
                                        padding: 15px 0 0;
                                        font-size: 21.5px;
                                    }
                                    img {
                                        width: 100%;
                                        height: auto;
                                    }
                                    #content {
                                        padding: 10px 0 0px;
                                    }
                                    #content_left {
                                        padding: 0 0 10px;
                                    }
                                    ol.numbered > li {
                                        margin-left: 1.4em;
                                        
                                    }
                                    #privacy-policy .size-large {
                                        font-size: 0.85rem;
                                    }

                                </style>
                            </head>
                            <body>
                                <div id="content" style="min-width:100%;">
                                    <div class="box">
                                            \n`+ entities.decode(privacypolicyContent) +`
                                    </div>
                                </div>
                            </body>
                        </html>`;
                // fs.writeFile('/home/simec/pavel/projects/kantaku_work/view/pp.html', str, function (err) {
                //     if (err) console.log(err);
                //     console.log('Saved!');
                // });
                let resArr = [{
                    "title": privacypolicyTitle,
                    "html-base64": Buffer.from(str).toString('base64')
                }];
                // console.log(resArr.length);
                //check empty array
                try{
                    if (Array.isArray(resArr) == true && resArr.length === 0) {        
                        let u = {
                            "nonce": new Date().getTime(),
                            "status" : "Error: Data not found",
                        };
                        res.status(404).json(u);
                    } else {
                        let u = {
                            "nonce": new Date().getTime(),
                            "status" : "ok",
                            "payload" : resArr
                        };
                        res.status(200).json(u);
                    }
                }catch(error){
                    res.status(500).json({
                        "nonce": new Date().getTime(),
                        "status" : "error: " + error
                    });
                }
        } catch (error) {
            res.status(500).json({
                "nonce": new Date().getTime(),
                "status" : "error: " + error
            });
        }
    }
});


/* GET Privacy Policy */
router.post('/privacypolicy', function(req, res, next) {
    // Check user valid Auth token
    // console.log(req.userInfo);
    let userInfo = req.userInfo;
    if(userInfo === undefined && false) {
        res.status(401).json({
            "nonce": new Date().getTime(),
            "status" : "unauthorized"
        });
    } else {
        try {
            let entities = req.app.get("entities");
            let config = req.app.get("config");
            let privacypolicyTitle = "プライバシーポリシー";
            let privacypolicyContent = `<div id="content_left">
                                    <h3 class="h3a">プライバシーポリシー</h3>
                                    <div class="section" id="privacy-policy">
                                        <div class="section">
                                        <h2 class="privacy-policy-title">大東建託協力会<br >個人情報保護方針</h2>
                                        <div class="inner">
                                            <p class="bold-text size-large far" style="font-weight: bold; font-size: 13.2px;">
                                                弊会は、事業活動を展開する中で、個人情報を大切に保護することが社会的使命と認識し、お預かりする個人情報を“お客様の財産そのもの”と捉え、お客様の権利利益の保護を目的として、次に個人情報保護方針を定めて公表し、個人情報の適切な取り扱いを徹底いたします。
                                            </p>
                                            <ol class="numbered">
                                                <li class="bold-text size-large mb15">
                                                    <p>個人情報の取得・利用目的について</p>
                                                    <p class="normal-text size-default">
                                                        弊会は、お客様から個人情報を取得する場合には、その利用目的をあらかじめお知らせ又は公表して、適法かつ公正な手段により取得いたします。また弊会は、お客様の個人情報の利用目的を特定し、その目的の達成に必要な範囲内においてのみ利用します。ご本人から事前のご承諾をいただいた場合や法で定める場合を除き、公表した利用目的以外の利用はいたしません。
                                                    </p>
                                                </li>
                                                <li class="bold-text size-large mb15">
                                                    <p>個人情報の共同利用について</p>
                                                    <p class="normal-text size-default">
                                                        弊会はお客様の個人情報を、弊会グループ企業内において共同して利用いたします。そのため、共同して利用する個人情報の項目、共同利用者名、利用目的をあらかじめ公表し、共同利用上の責任は弊会が負います。
                                                    </p>
                                                </li>
                                                <li class="bold-text size-large mb15">
                                                    <p>個人情報の第三者への提供について</p>
                                                    <p class="normal-text size-default">
                                                        弊会は、お客様の個人情報の取り扱いにおいて、法で定める場合やお客様とのご契約内容の達成に必要な場合等を除き、あらかじめお客様の同意を得ることなく、お客様の個人情報を第三者へ提供いたしません。
                                                    </p>
                                                </li>
                                                <li class="bold-text size-large mb15">
                                                    <p>個人情報の正確性・安全管理について</p>
                                                    <p class="normal-text size-default">
                                                        弊会は、お客様の個人情報の紛失・破壊・改ざん及び漏洩等を予防すべく、組織的・人的・物理的・技術的な安全対策を講じるとともに、必要な是正措置を講じます。また、個人情報の取り扱いを外部へ委託する場合は、委託先との間で取り扱いに関する契約を結ぶなどして適切な監督を行います。
                                                    </p>
                                                </li>
                                                <li class="bold-text size-large mb15">
                                                    <p>個人情報の開示等のご請求について</p>
                                                    <p class="normal-text size-default">
                                                        弊会がお預かりするお客様の個人情報について、お客様から開示・訂正・利用停止・消去及び第三者への提供停止を求められた場合には、法令や社会通念上等に照らし、適切に対応いたします。
                                                    </p>
                                                </li>
                                                <li class="bold-text size-large">
                                                    <p>各種法令等の遵守</p>
                                                    <p class="normal-text size-default">弊会は、個人情報の保護に関する法律及びその他法令、社会的規範等を遵守いたします。</p>
                                                </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>`;

            let  str = `
            <!DOCTYPE html>
            <html lang="ja">
            
            <head>
                <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
				<meta name="viewport" content="width=device-width" />
				<meta name="format-detection" content="telephone=no" />
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <link rel="icon" type="image/png" href="`+ config.get("url.base_url") + `img/favicon.jpg">
                <link href="`+ config.get("url.base_url") + `css/common.css?1577532334" rel="stylesheet" type="text/css" media="all" />
                <link href="`+ config.get("url.base_url") + `css/privacypolicy.css?1577532334" rel="stylesheet" type="text/css" media="all" />
                <title>大東建託協力会 | プライバシーポリシー</title>
            	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
				<script language="javascript" type="text/javascript" src="`+ config.get("url.base_url") + `js/com.js"></script>
				<script language="javascript" type="text/javascript" src="`+ config.get("url.base_url") + `js/ex.js"></script>
				<script language="javascript" type="text/javascript" src="`+ config.get("url.base_url") + `js/ajax.js"></script>
				<script language="javascript" type="text/javascript" src="`+ config.get("url.base_url") + `js/main.js"></script>
				<!--[if lt IE 9]>
                    <script>
                    document.createElement('article');
                    document.createElement('aside');
                    document.createElement('section');
                    document.createElement('header');
                    document.createElement('footer');
                    document.createElement('nav');
                    document.createElement('figure');
                    </script>
                    <script type="text/javascript" src="{$common.url}js/respond.min.js"></script>
                <![endif]-->         	   
            <style>
                html, body {
                    
                    width: inherit !important;
                    min-width: 10px;
                }
                html, body, header, #content, footer { 
                    min-width : 1px;
                    width:100%;
                }
                #content_left .h3a {
                    padding: 15px 0 0;
                    font-size: 21.5px;
                }
                img {
                    width: 100%;
                    height: auto;
                }
                #content {
                    padding: 10px 0 0px;
                }
                #content_left {
                    padding: 0 0 10px;
                }
                ol.numbered > li {
                    margin-left: 1.6em;
                    
                }
                #privacy-policy .size-large {
                    font-size: 0.85em;
                }
                #privacy-policy .inner {
                    padding-right: 0.75em;
                    padding-left: 0.75em;
                }
            </style>
            </head>
            
            <body data-gr-c-s-loaded="true" style="">
                <form name="mainfrm" id="mainfrm" aaction="`+ config.get("url.base_url") + `privacypolicy/" amethod="post"
                    aenctype="multipart/form-data">
                   
                    <article id="content" class="about">
                        <div class="box">
                            <div id="content_left">
                                <h3 class="h3a">プライバシーポリシー</h3>
                                <div class="section" id="privacy-policy">
                                    <div class="section">
                                        <h2 class="privacy-policy-title">大東建託協力会<br >個人情報保護方針</h2>
                                        <div class="inner">
                                            <p class="bold-text size-large far">
                                                弊会は、事業活動を展開する中で、個人情報を大切に保護することが社会的使命と認識し、お預かりする個人情報を“お客様の財産そのもの”と捉え、お客様の権利利益の保護を目的として、次に個人情報保護方針を定めて公表し、個人情報の適切な取り扱いを徹底いたします。
                                            </p>
                                            <ol class="numbered">
                                                <li class="bold-text size-large mb15">
                                                    <p>個人情報の取得・利用目的について</p>
                                                    <p class="normal-text size-default">
                                                        弊会は、お客様から個人情報を取得する場合には、その利用目的をあらかじめお知らせ又は公表して、適法かつ公正な手段により取得いたします。また弊会は、お客様の個人情報の利用目的を特定し、その目的の達成に必要な範囲内においてのみ利用します。ご本人から事前のご承諾をいただいた場合や法で定める場合を除き、公表した利用目的以外の利用はいたしません。
                                                    </p>
                                                </li>
                                                <li class="bold-text size-large mb15">
                                                    <p>個人情報の共同利用について</p>
                                                    <p class="normal-text size-default">
                                                        弊会はお客様の個人情報を、弊会グループ企業内において共同して利用いたします。そのため、共同して利用する個人情報の項目、共同利用者名、利用目的をあらかじめ公表し、共同利用上の責任は弊会が負います。
                                                    </p>
                                                </li>
                                                <li class="bold-text size-large mb15">
                                                    <p>個人情報の第三者への提供について</p>
                                                    <p class="normal-text size-default">
                                                        弊会は、お客様の個人情報の取り扱いにおいて、法で定める場合やお客様とのご契約内容の達成に必要な場合等を除き、あらかじめお客様の同意を得ることなく、お客様の個人情報を第三者へ提供いたしません。
                                                    </p>
                                                </li>
                                                <li class="bold-text size-large mb15">
                                                    <p>個人情報の正確性・安全管理について</p>
                                                    <p class="normal-text size-default">
                                                        弊会は、お客様の個人情報の紛失・破壊・改ざん及び漏洩等を予防すべく、組織的・人的・物理的・技術的な安全対策を講じるとともに、必要な是正措置を講じます。また、個人情報の取り扱いを外部へ委託する場合は、委託先との間で取り扱いに関する契約を結ぶなどして適切な監督を行います。
                                                    </p>
                                                </li>
                                                <li class="bold-text size-large mb15">
                                                    <p>個人情報の開示等のご請求について</p>
                                                    <p class="normal-text size-default">
                                                        弊会がお預かりするお客様の個人情報について、お客様から開示・訂正・利用停止・消去及び第三者への提供停止を求められた場合には、法令や社会通念上等に照らし、適切に対応いたします。
                                                    </p>
                                                </li>
                                                <li class="bold-text size-large">
                                                    <p>各種法令等の遵守</p>
                                                    <p class="normal-text size-default">弊会は、個人情報の保護に関する法律及びその他法令、社会的規範等を遵守いたします。</p>
                                                </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </article>
                    
                </form>
            
            </body>
            
            </html>`;
                
                let resArr = [{
                    "title": privacypolicyTitle,
                    "html-base64": Buffer.from(str).toString('base64')
                }];
                // console.log(resArr.length);
                //check empty array
                try{
                    if (Array.isArray(resArr) == true && resArr.length === 0) {
                        let u = {
                            "nonce": new Date().getTime(),
                            "status" : "Error: Data not found",
                        };
                        res.status(404).json(u);
                    } else {
                        let u = {
                            "nonce": new Date().getTime(),
                            "status" : "ok",
                            "payload" : resArr
                        };
                        res.status(200).json(u);
                    }
                }catch(error){
                    res.status(500).json({
                        "nonce": new Date().getTime(),
                        "status" : "error: " + error
                    });
                }
        } catch (error) {
            res.status(500).json({
                "nonce": new Date().getTime(),
                "status" : "error: " + error
            });
        }
    }
});

/* GET Address */
router.post('/address', function(req, res, next) {
    try {
        //Check user valid Auth token
        let userInfo = req.userInfo;
        if(userInfo === undefined && false) {
            res.status(401).json({
                "nonce": new Date().getTime(),
                "status" : "unauthorized"
            });
        } else {
            var aboutModule = req.app.get("aboutModule");
            let config = req.app.get("config");
            aboutModule.addressContent((SQLResult) => {
                var results = SQLResult.rows;
                var  resArr = [];
                if(results.length > 0 || true) {
                    resArr[0]= [
                        {
                            "office"    :   "大東建託協力会事務局",
                            "address"   :   "メールアドレス： jkyouryokukai@kentaku.co.jp",
                            //"address"   :   "〒108-8211　東京都港区港南2-16-1"
                            "tel"       :   "",
                            "fax"       :   "",
                        }
                    ];
                    resArr[1] = [
                        {
                            "file_path"  : config.get("url.base_url") + config.get("url.about_file") + results[0]["f_pdf_file"],
                            "file_info"  : { "id": "about_company_info_file", "index": "", "value" : results[0]["f_pdf_file"] },
                            "file_name"  : results[0]["f_pdf_name"],
                            "file_download_name" : results[0]["f_pdf_name"],
                            "file_size"  : results[0]["f_pdf_size1"],
                            "file_extension" : "pdf"
                        },
                        {
                            "file_path" : config.get("url.base_url") + config.get("url.about_file") + results[0]["f_pdf_directory"],
                            "file_info"  : { "id": "about_officers_file", "index": "", "value" : results[0]["f_pdf_directory"] },
                            "file_name" : results[0]["f_pdf_directory_name"],
                            "file_download_name" : results[0]["f_pdf_directory_name"],
                            "file_size" : results[0]["f_pdf_size2"],
                            "file_extension" : "pdf"
                        }
                    ];
                } else {
                    resArr[0]= [
                        {
                            "office"    :   "本部事務局",
                            "address"   :   "〒108-8211　東京都港区港南2-16-1",
                            "tel"       :   "(03) - 6718 - 9250",
                            "fax"       :   "(03) - 6718 - 9251",
                        },
                        {
                            "office"    :   "持株会事務局（大東コーポレートサービス　シェアードサービス部）",
                            "address"   :   "〒140-0002　東京都品川区東品川2-2-8　スフィアタワー天王洲",
                            "tel"       :   "(03) - 6718 - 9056",
                            "fax"       :   "(03) - 6718 - 9057",
                        }
                    ];
                }

                let u = {
                    "nonce": new Date().getTime(),
                    "status" : "ok",
                    "payload" : resArr
                };
                res.status(200).json(u);
            });
        }
    } catch (error) {
        res.status(500).json({
            "nonce": new Date().getTime(),
            "status" : "error " + error
        });
    }
});

// /* GET contact us info */
router.post('/about-cooperation', function(req, res, next) {
    // Check user valid Auth token
    // console.log(req.userInfo);
    let userInfo = req.userInfo;
    if(userInfo === undefined && false) {
        res.status(401).json({
            "nonce": new Date().getTime(),
            "status" : "unauthorized"
        });
    } else {
        try {
            let entities = req.app.get("entities");
            let config = req.app.get("config");
            let privacypolicyTitle = "アプリについて";
            let  str = `
            <!DOCTYPE html>
            <html lang="ja">
            
            <head>
                <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
				<meta name="viewport" content="width=device-width" />
				<meta name="format-detection" content="telephone=no" />
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <link rel="icon" type="image/png" href="`+ config.get("url.base_url") + `img/favicon.jpg">
                <link href="`+ config.get("url.base_url") + `css/common.css?1577532334" rel="stylesheet" type="text/css" media="all" />
                <link href="`+ config.get("url.base_url") + `css/privacypolicy.css?1577532334" rel="stylesheet" type="text/css" media="all" />
                <title>大東建託協力会アプリについて</title>
            	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
				<script language="javascript" type="text/javascript" src="`+ config.get("url.base_url") + `js/com.js"></script>
				<script language="javascript" type="text/javascript" src="`+ config.get("url.base_url") + `js/ex.js"></script>
				<script language="javascript" type="text/javascript" src="`+ config.get("url.base_url") + `js/ajax.js"></script>
				<script language="javascript" type="text/javascript" src="`+ config.get("url.base_url") + `js/main.js"></script>
				<!--[if lt IE 9]>
                    <script>
                    document.createElement('article');
                    document.createElement('aside');
                    document.createElement('section');
                    document.createElement('header');
                    document.createElement('footer');
                    document.createElement('nav');
                    document.createElement('figure');
                    </script>
                    <script type="text/javascript" src="{$common.url}js/respond.min.js"></script>
                <![endif]-->         	   
            <style>
                html, body {
                    
                    width: inherit !important;
                    min-width: 10px;
                }
                html, body, header, #content, footer { 
                    min-width : 1px;
                    width:100%;
                }
                #content_left .h3a {
                    padding: 15px 0 0;
                    font-size: 21.5px;
                }
                img {
                    width: 100%;
                    height: auto;
                }
                #content {
                    padding: 10px 0 0px;
                }
                #content_left {
                    padding: 0 0 10px;
                }
                ol.numbered > li {
                    margin-left: 1.6em;
                    
                }
                #privacy-policy .size-large {
                    font-size: 0.85em;
                }
                #privacy-policy .inner {
                    padding-right: 0.75em;
                    padding-left: 0.75em;
                }
            </style>
            </head>
            
            <body data-gr-c-s-loaded="true" style="">
                <form name="mainfrm" id="mainfrm" aaction="`+ config.get("url.base_url") + `privacypolicy/" amethod="post"
                    aenctype="multipart/form-data">
                   
                    <article id="content" class="about">
                        <div class="box">
                            <div id="content_left">
                                <h3 class="h3a">大東建託協力会アプリについて</h3>
                                <div class="section" id="privacy-policy">
                                    <div class="section">
                                        <h2 class="privacy-policy-title">大東建託協力会会員様向けアプリケーションです。</h2>
                                        <div class="inner">
                                            <p class="bold-text size-large" style="font-weight: bold; font-size: 13.2px;">アプリについて</p>
                                            <ol class="numbered  mb15">
                                                <li class=" ">
                                                    協力会ホームページの内容をアプリで簡単に閲覧できるようになりました。
                                                </li>
                                                <li class=" ">
                                                    人毎にIDを付与できるため、従業員や作業員も利用出来るようになりました。
                                                </li>
                                                <li class=" ">
                                                    プッシュ通知機能を搭載することで、リアルタイム情報を知る事が出来ます。
                                                </li>
                                                <li class=" ">
                                                    毎回ログインする必要がなくなり、利便性が向上しました。
                                                </li>
                                                <li class=" ">
                                                    スマートフォンに特化したデザインにより、操作性が向上しました。
                                                </li>
                                            </ol>
                                            
                                            <p class="bold-text size-large" style="font-weight: bold; font-size: 13.2px;">利用対象者</p>
                                            
                                            <ol class="numbered  mb15">
                                                <li class=" ">
                                                    協力会会員及び従業員（協力会会員業者取引先含む）
                                                </li>
                                                <li class=" ">
                                                    大東建託社員（工事部門、設計部門のみ）
                                                </li>
                                                <li class=" ">
                                                    大東建託パートナーズ、大東建設社員
                                                </li>
                                            </ol>
                                            
                                            
                                            <p class="bold-text size-large" style="font-weight: bold; font-size: 13.2px;">運営会社</p>
                                            <p class="far" style="font-size: 13.2px;">大東建託協力会</p>
                                            <p class="bold-text size-large" style="font-weight: bold; font-size: 13.2px;">開発会社</p>
                                            <p class="far" style="font-size: 13.2px;">あさかわシステムズ株式会社</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </article>
                    
                </form>
            
            </body>
            
            </html>`;
            // fs.writeFile('as.html', str, function (err) {
            //     if (err) console.log(err);
            //     console.log('Saved!');
            // });
                let resArr = [{
                    "title": privacypolicyTitle,
                    "html-base64": Buffer.from(str).toString('base64')
                }];
                // console.log(resArr.length);
                //check empty array
                try{
                    if (Array.isArray(resArr) == true && resArr.length === 0) {
                        let u = {
                            "nonce": new Date().getTime(),
                            "status" : "Error: Data not found",
                        };
                        res.status(404).json(u);
                    } else {
                        let u = {
                            "nonce": new Date().getTime(),
                            "status" : "ok",
                            "payload" : resArr
                        };
                        res.status(200).json(u);
                    }
                }catch(error){
                    res.status(500).json({
                        "nonce": new Date().getTime(),
                        "status" : "error: " + error
                    });
                }
        } catch (error) {
            res.status(500).json({
                "nonce": new Date().getTime(),
                "status" : "error: " + error
            });
        }
    }
});

router.post('/banner-ear', function(req, res, next) {
    // Check user valid Auth token
        let userInfo = req.userInfo;
        if(userInfo === undefined && false) {
            res.status(401).json({
                "nonce": new Date().getTime(),
                "status" : "unauthorized"
            });
        } else {
            try {
                let entities = req.app.get("entities");
                let config = req.app.get("config");
                let bannerEarContent = `<!DOCTYPE html>
                                    <html lang="ja">
                                    <head>
                                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                                        <meta name="viewport" content="width=device-width">
                                        <meta name="format-detection" content="telephone=no">
                                        <meta name="Description" content="">
                                        <meta name="Keywords" content="">
                                        <link rel="icon" type="image/png" href="`+ config.get("url.base_url") + `img/favicon.jpg">
                                        <link href="`+ config.get("url.base_url") + `css/common.css?1577533852" rel="stylesheet" type="text/css" media="all" />
                                        <link href="`+ config.get("url.base_url") + `css/contact.css?1577533852" rel="stylesheet" type="text/css" media="all" />
                                        <title>大東建託協力会 | プライバシーポリシー</title>
                                        <!--[if lt IE 9]>
                                        <script>
                                        document.createElement('article');
                                        document.createElement('aside');
                                        document.createElement('section');
                                        document.createElement('header');
                                        document.createElement('footer');
                                        document.createElement('nav');
                                        document.createElement('figure');
                                        </script>
                                        <script type="text/javascript" src="{$common.url}js/respond.min.js"></script>
                                        <![endif]-->
                                        <style>
                                        html, body {
                                                
                                            width: inherit !important;
                                            min-width: 10px;
                                        }
                                        
                                        #content_left .h3a {
                                            padding: 15px 0 0;
                                            font-size: 24px;

                                        }
                                        #edit_area p {
                                            font-size: 0.85em;
                                        }
                                        #edit_area h5 {
                                            font-size: 0.93em;
                                        }
                                        #edit_area h6 {
                                            font-size: 0.93em;
                                        }
                                        #edit_area ul {
                                            font-size: 0.82em;
                                        }
                                                                                
                                        img {
                                            width: 100%;
                                            height: auto;
                                        }
                                        #content {
                                            padding: 10px 0 0px;
                                        }
                                        #content_left {
                                            padding: 0 0 10px;
                                        }
                                        ol.numbered > li {
                                            margin-left: 1.6em;
                                            
                                        }
                                        #privacy-policy .size-large {
                                            font-size: 0.85em;
                                        }
                                        #privacy-policy .inner {
                                            padding-right: 0.75em;
                                            padding-left: 0.75em;
                                        }
                            </style>
                                    </head>
                                    <body data-gr-c-s-loaded="true" style="">
                                        <form name="mainfrm" id="mainfrm" aaction="`+ config.get("url.base_url") + `privacypolicy/" amethod="post"
                                            aenctype="multipart/form-data">
                                            <article id="content" class="about">
                                                <section id="content_header">
                                                    <div class="box">
                                                        <div>
                                                            <h2>会員募集のご案内</h2>
                                                            <p id="bc"><a href="#">HOME</a> &gt; 会員募集のご案内</p>
                                                        </div>
                                                    </div>
                                                </section>
                                                <div class="box">
                                                    <div id="content_left">
                                                        <section id="inquiry_210">
                                                            <section>
                                                                <ul>
                                                                    <li>
                                                                    <p>会員募集については各工事支部にお問い合わせください。</p>
                                                                    <p align="center" style="margin-top: 2em;">
                                                                        <a href="http://www.e-map.ne.jp/p/daitomap/" target="_blank">
                                                                            <img src="`+ config.get("url.base_url") + `daito/img/s_koji.png" alt="会員募集">
                                                                        </a>
                                                                    </p>
                                                                </li>
                                                            </ul>
                                                            </section>
                                                        </section>
                                                    </div>
                                                </div>
                                            </article>
                                        </form>
                                    </body>
                                    </html>`;
                                    // fs.writeFile('/home/simec/pavel/projects/kantaku_work/view/mm.html', bannerEarContent, function (err) {
                                    //     if (err) console.log(err);
                                    //     console.log('Saved!');
                                    // });
                    let str = entities.decode(bannerEarContent);
                    let resArr = [{
                        "html-base64": Buffer.from(str).toString('base64')
                    }];
    
                    try{
                        if (Array.isArray(resArr) == true && resArr.length === 0) {
                            let u = {
                                "nonce": new Date().getTime(),
                                "status" : "Error: Data not found",
                            };
                            res.status(404).json(u);
                        } else {
                            let u = {
                                "nonce": new Date().getTime(),
                                "status" : "ok",
                                "payload" : resArr
                            };
                            res.status(200).json(u);
                        }
                    }catch(error){
                        res.status(500).json({
                            "nonce": new Date().getTime(),
                            "status" : "error: " + error
                        });
                    }
            } catch (error) {
                res.status(500).json({
                    "nonce": new Date().getTime(),
                    "status" : "error: " + error
                });
            }
        }
    });

module.exports = router;