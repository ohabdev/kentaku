var express = require('express');
var router = express.Router();
const fs = require('fs');
const moment = require('moment');


/* GET Notification list */
router.post('/list', function(req, res, next) {
    // Check user valid auth token
    let userInfo = req.userInfo;
    if(userInfo === undefined) {
        res.status(401).json({
            "nonce": new Date().getTime(),
            "status" : "unauthorized"
        });
    } else {
        try {
            var fcmModule = req.app.get("fcmModule");
            let agencyId = userInfo["id"];
            fcmModule.getAllFCM(agencyId, (SQLResults) => {
                var results = SQLResults.rows;
                var countResult = results.length;
                let resArr = [];
                for(let i=0; i<countResult; i++) {
                    // console.log( results[i]["f_send_flg"] );
                    /*
                        if url is not null then 
                            unread flag gone after user click on it
                        if url is null or empty then
                            user view new flag first time in the list and user back to list those new flag are gone without click

                        
                    */
                    resArr[i] = {
                        "message_id": results[i]["f_message_id"],
                        "agency_id": results[i]["f_agency_id"],
                        "message_title": "" + results[i]["f_message_title"],
                        "published_date": "" + moment(new Date(results[i]["f_schedule_start_date_time"])).format("YYYY.MM.DD"),
                        "message": results[i]["f_message"],
                        "url": "" + (results[i]["f_url"] ? results[i]["f_url"] : ""),
                        "read_flg": results[i]["f_send_flg"] != 2
                        // "read_flg": (results[i]["f_url"]) ? results[i]["f_send_flg"] != 2 : false //if url empty
                    }
                    // fcmModule.updateAllFMessagesIfSendFlagNot2( agencyId, ( SQLResults ) => {
                    //     // console.log( 'Updated all f messages those are not 2' );
                    // } );
                    // fcmModule.updateFMessagesbyID( results[i]["f_message_id"], ( SQLResults ) => {
                    //     //console.log(SQLResults);
                    //     //console.log( 'Updated all f messages those are not 2' );
                    // });
                    if(!results[i]["f_url"]) {
                        fcmModule.updateFMessagesbyID( results[i]["f_message_id"], ( SQLResults ) => {
                            //console.log(SQLResults);
                            //console.log( 'Updated all f messages those are not 2' );
                        });
                    }
                };
                //Empty result/array check
                try{
                    if (Array.isArray(resArr) == true && resArr.length === 0) {
                        let u = {
                            "nonce": new Date().getTime(),
                            "status" : "Notification Empty",
                        };
                        res.status(404).json(u);
                    } else {
                        
                        let u = {
                            "nonce": new Date().getTime(),
                            "status" : "ok",
                            "payload" : resArr
                        };
                        res.status(200).json(u);
                    }
                }catch(error){
                    res.status(500).json({
                        "nonce": new Date().getTime(),
                        "status" : "error: " + error
                    });
                }
            });
        } catch (error) {
            res.status(401).json({
                "nonce": new Date().getTime(),
                "status" : "unauthorized"
            });
        }
    }
});

/* GET Notification list */
router.post('/new_count', function(req, res, next) {
    // Check user valid auth token
    let userInfo = req.userInfo;
    if(userInfo === undefined) {
        res.status(401).json({
            "nonce": new Date().getTime(),
            "status" : "unauthorized"
        });
    } else {
        try {
            var fcmModule = req.app.get("fcmModule");
            let agencyId = userInfo["id"];
            fcmModule.getAllFCMNewCount(agencyId, (SQLResults) => {
                try {
                    var results = SQLResults.rows;
                    var countResult = results.length;
                    //console.log(results);
                    let resArr = [];
                    let u = {};
                    if(countResult === 0) {
                        u = {
                            "nonce": new Date().getTime(),
                            "status" : "ok",
                            "payload" : {"count" : 0}
                        };
                    } else {
                        u = {
                            "nonce": new Date().getTime(),
                            "status" : "ok",
                            "payload" : {"count" : results[0]["count"]}
                        };
                    }
                    res.status(200).json(u);
                }catch(error){
                    res.status(500).json({
                        "nonce": new Date().getTime(),
                        "status" : "" + error
                    });
                }
            });
        } catch (error) {
            res.status(500).json({
                "nonce": new Date().getTime(),
                "status" : "error: " + error
            });
        }
    }
});
module.exports = router;

