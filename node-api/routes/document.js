var express = require('express');
var router = express.Router();
const fs = require('fs');
const moment = require('moment');

var cat = {"1": "安全", "2": "技術", "3": "品質", "4": "資料", "5": "その他"};

/* GET Document */
router.post('/', function(req, res, next) {
    // Check user valid Auth token
    let userInfo = req.userInfo;
    if(userInfo === undefined) {
        res.status(401).json({
            "nonce": new Date().getTime(),
            "status" : "unauthorized"
        });
    } else {
        var documentModule = req.app.get("documentModule");
        let payload = req.body.payload;
        let searchData = "";
        let keywordId = "";
        let orderBy = "ASC";
        if(payload !== undefined) {
            searchData = payload["search"];
            keywordId = payload["keyword_id"];
            orderBy = payload["order_by"];
            if(searchData === undefined || typeof searchData !== "string") {
                searchData = "";
            }
            if(keywordId === undefined || typeof keywordId !== "string") {
                keywordId = "";
            }
            if(orderBy === undefined || typeof orderBy !== "string") {
                orderBy = "ASC";
            }
        }
        documentModule.getAllDocument(
                searchData = searchData, 
                keywordId = keywordId, 
                orderBy = orderBy, 
                userInfo = {"level" : userInfo["level"]}, (SQLresults) => {
            var results = SQLresults.rows;
            var countResult = results.length;
            
            resArr = [];
            for(let i=0; i<countResult; i++) {
                // let newFLG = false;
                // if(results[i]["f_disp_date_from"] !== undefined && results[i]["f_new_flg_days"] !== undefined) {
                //     let dt = new Date();
                //     try {
                //         dt.setDate(
                //             new Date(results[i]["f_disp_date_from"]).getDate() + parseInt(results[i]["f_new_flg_days"])
                //         );
                //         if( dt.getTime() > (new Date().getTime())) {
                //             newFLG = true;
                //         }
                //     } catch (error) {
                        
                //     }
                // }
                resArr[i] = {
                    "document_id": results[i]["f_document_id"],
                    "title": results[i]["f_title"],
                    "content": results[i]["f_content"],
                    "category": cat[results[i]["f_category"]],
                    //"publish_date_from": moment(new Date(results[i]["f_new_arrival_start_date"]).getTime()).format("YYYY.MM.DD"),
                    "publish_date_from": moment(new Date(results[i]["f_reg_time_ymd"]).getTime()).format("YYYY.MM.DD"),
                    "keyword_ids": results[i]["f_keyword_ids"],
                    "new_flg": ((results[i]["new_flg"] === 0 ? false : true)),
                    "file_name": results[i]["f_filename"],
                    "file_path": results[i]["f_file"],
                    "file_info"  : { "id": "document_file", "index": "", "value" : results[i]["f_file"] },
                    "file_size": results[i]["f_file_size"],
                    "file_download_name": results[i]["f_download_name"],
                    "file_extension" : (results[i]["f_extension"] === null ? "other": results[i]["f_extension"])
                }
            };

            try{
                if (Array.isArray(resArr) == true && resArr.length === 0) {
                    let u = {
                        "nonce": new Date().getTime(),
                        "status" : "Data not found",
                    };
                    res.status(404).json(u);
                } else {
                    
                    let u = {
                        "nonce": new Date().getTime(),
                        "status" : "ok",
                        "payload" : resArr
                    };
                    res.status(200).json(u);
                }
            }catch(error){
                res.status(500).json({
                    "nonce": new Date().getTime(),
                    "status" : "error: " + error
                });
            }
        });
    }
});

/* GET Keywords */
router.post('/keywords', function(req, res, next) {
    let userInfo = req.userInfo;
    //console.log(userInfo);
    if(userInfo === undefined) {
        res.status(401).json({
            "nonce": new Date().getTime(),
            "status" : "unauthorized"
        });
    }else{
        var documentModule = req.app.get("documentModule");
        documentModule.getKeywords((SQLResults) => {
            var results = SQLResults.rows;
            var countResult = results.length;
            let resArr = [];
            for(let i=0; i<countResult; i++) {
                resArr[i] = {
                    "id": results[i]["f_keyword_id"],
                    "name": results[i]["f_keyword"]
                }
            };
            //Empty result/array check
            try{
                if (Array.isArray(resArr) == true && resArr.length === 0) {
                    let u = {
                        "nonce": new Date().getTime(),
                        "status" : "Error: Data not found",
                    };
                    res.status(404).json(u);
                } else {
                    
                    let u = {
                        "nonce": new Date().getTime(),
                        "status" : "ok",
                        "payload" : resArr
                    };
                    res.status(200).json(u);
                }
            }catch(error){
                res.status(500).json({
                    "nonce": new Date().getTime(),
                    "status" : "error: " + error
                });
            }
        });
    }
});


module.exports = router;