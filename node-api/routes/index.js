var express = require('express');
var router = express.Router();
const moment = require('moment');


/* GET home page. */
router.get('/', function(req, res, next) {
  //console.log("okay");
  res.render('index', { title: 'Kentaku' });
});


/* GET FCM page. */
var fcmAgency = undefined;
router.get('/fcm', function(req, res) {
    if(fcmAgency !== undefined) {
        res.render('fcm', { title: 'FCM Push Notification', results: fcmAgency });
    } else {
        let agencyModule = req.app.get("agencyModule");
        agencyModule.getAllAgency((SQLResult) => {
            fcmAgency = SQLResult.rows;
            fcmAgency = [fcmAgency[0]];
            res.render('fcm', { title: 'FCM Push Notification', results: fcmAgency });
        });
    }
});

/* GET FCM page. */
router.post('/fcm', function(req, res) {
    try {
        // console.log("response okay");
        let messageTitle = req.body.message_title;
        let agencyId = req.body.f_agency_id;
        let messageBody = req.body.message_body;
        let getscheduleTime = req.body.schedule_time;
        let pageUrl = req.body.page_url;
        //console.log(getscheduleTime);
        try {
            getscheduleTime = new Date(getscheduleTime);
        } catch (error) {
            getscheduleTime = new Date();
        }
        
        getscheduleTime = moment(getscheduleTime).format("YYYY-MM-DD HH:mm:ss");

        // let moment(time, "HH:mm:ss").format("hh:mm A")
        //console.log(getscheduleTime);;
        
            let fcmModule = req.app.get("fcmModule");
            fcmModule.fcmMsg(agencyId, messageTitle, messageBody, getscheduleTime, pageUrl, (SQLResult) => {
                    // console.log(SQLResult.rows);
                    // console.log("------------------");
                    if(SQLResult.rows.affectedRows === 1){
                        var msg = encodeURIComponent('Push Notification send successfully');
                        res.redirect('/fcm?success=' + msg);
                    }else{
                        var err = encodeURIComponent('Data not inserted');
                        res.redirect('/fcm?erasr=' + err);
                    }
            });
    } catch (error) {
        console.log(error);
        res.redirect('/fcm?error=' + error);
    }
  });

module.exports = router;
