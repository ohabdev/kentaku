var express = require('express');
var router = express.Router();
const fs = require('fs');
const moment = require('moment');

var cat = {"1": "安全", "2": "技術", "3": "品質", "4": "資料", "5": "その他"};

/* GET MOVIE */
router.post('/', function(req, res, next) {
    // Check user valid auth token
    let userInfo = req.userInfo;
    if(userInfo === undefined) {
        res.status(401).json({
            "nonce": new Date().getTime(),
            "status" : "unauthorized"
        });
    } else {
        try {
            var movieModule = req.app.get("movieModule");
            let payload = req.body.payload;
            let searchData = "";
            let keywordId = "";
            let orderBy = "ASC";
            if(payload !== undefined) {
                searchData = payload["search"];
                keywordId = payload["keyword_id"];
                orderBy = payload["order_by"];
                if(searchData === undefined || typeof searchData !== "string") {
                    searchData = "";
                }
                if(keywordId === undefined || typeof keywordId !== "string") {
                    keywordId = "";
                }
                if(orderBy === undefined || typeof orderBy !== "string") {
                    orderBy = "ASC";
                }
                //console.log(payload);
                
            }

            movieModule.getAllMovie(
                searchData = searchData, 
                keywordId = keywordId, 
                orderBy = orderBy, 
                userInfo = {"level" : userInfo["level"]}, (SQLResults) => {
                var results = SQLResults.rows;
                var countResult = results.length;
                let config = req.app.get("config");
                let resArr = [];
                for(let i=0; i<countResult; i++) {
                    // let newFLG = false;
                    // if(results[i]["f_disp_date_from"] !== undefined && results[i]["f_new_flg_days"] !== undefined) {
                    //     let dt = new Date();
                    //     try {
                    //         dt.setDate(
                    //             new Date(results[i]["f_disp_date_from"]).getDate() + parseInt(results[i]["f_new_flg_days"])
                    //         );
                    //         if( dt.getTime() > (new Date().getTime())) {
                    //             newFLG = true;
                    //         }
                    //     } catch (error) {
                            
                    //     }
                    // }
                    resArr[i] = {
                        "movie_id": results[i]["f_movie_id"],
                        "title": results[i]["f_title"],
                        "movie_new_flg": (results[i]["new_flg"] === 1 ? true : false),
                        "property": results[i]["f_property"],
                        "category": cat[results[i]["f_category"]],
                        "description": results[i]["f_description"],
                        "publish_date_from": moment(new Date(results[i]["f_disp_date_from"]).getTime()).format("YYYY.MM.DD"),
                        "movie_filename": results[i]["f_movie_filename"],
                        "video_link": config.get("url.base_url") + config.get("url.movie_video") + results[i]["f_video_name"],
                        "image": config.get("url.base_url") + config.get("url.movie_img") + results[i]["f_image"],
                        "image_thumb": config.get("url.base_url") + config.get("url.movie_img") + results[i]["f_thumb"]
                    }
                };
                //Empty result/array check
                try{
                    if (Array.isArray(resArr) == true && resArr.length === 0) {
                        let u = {
                            "nonce": new Date().getTime(),
                            "status" : "Data not found",
                        };
                        res.status(404).json(u);
                    } else {
                        
                        let u = {
                            "nonce": new Date().getTime(),
                            "status" : "ok",
                            "payload" : resArr
                        };
                        res.status(200).json(u);
                    }
                }catch(error){
                    res.status(500).json({
                        "nonce": new Date().getTime(),
                        "status" : "error: " + error
                    });
                }
            });
        } catch (error) {
            res.status(401).json({
                "nonce": new Date().getTime(),
                "status" : "unauthorized"
            });
        }
    }
});

/* GET Keywords */
router.post('/keywords', function(req, res, next) {
    let userInfo = req.userInfo;
    // console.log(userInfo);
    if(userInfo === undefined ) {
        res.status(401).json({
            "nonce": new Date().getTime(),
            "status" : "unauthorized"
        });
    }else{
        var movieModule = req.app.get("movieModule");
        movieModule.getKeywords((SQLResults) => {
            var results = SQLResults.rows;
            var countResult = results.length;
            // console.log(countResult);
            let resArr = [];
            for(let i=0; i<countResult; i++) {
                resArr[i] = {
                    "id": results[i]["f_keyword_id"],
                    "name": results[i]["f_keyword"]
                }
            };
            //Empty result/array check
            // console.log(resArr);
            try{
                if (Array.isArray(resArr) == true && resArr.length === 0) {
                    let u = {
                        "nonce": new Date().getTime(),
                        "status" : "Error: Data not found",
                    };
                    res.status(404).json(u);
                } else {
                    
                    let u = {
                        "nonce": new Date().getTime(),
                        "status" : "ok",
                        "payload" : resArr
                    };
                    res.status(200).json(u);
                }
            }catch(error){
                res.status(500).json({
                    "nonce": new Date().getTime(),
                    "status" : "error: " + error
                });
            }
        });
    }
});

module.exports = router;