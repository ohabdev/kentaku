var express = require('express');
var router = express.Router();
const fs = require('fs');
const moment = require('moment');
const config = require('config');

/* GET All Content list. */
router.post('/list', function(req, res, next) {
    var contentModule = req.app.get("contentModule");

    // Check user valid Auth token
    let userInfo = req.userInfo;
    if(userInfo === undefined) {
        res.status(401).json({
            "nonce": new Date().getTime(),
            "status" : "unauthorized"
        });
    } else {
        //console.log(userInfo["level"]);
        
        contentModule.getContentList(userInfo = {"level" : userInfo["level"]}, (SQLResult) => {
            let results = SQLResult.rows;
            
            let count_result = results.length;
            resArr = [];
            let contentList = [];
            if(count_result === 0) {
                res.status(404).json({
                    "nonce": new Date().getTime(),
                    "status" : config.get("lang.empty_data")
                });
            } else {
                for(let i=0; i<count_result; i++) {
                    try {
                        if(results[i]['f_parent_position'] === null || results[i]['f_parent_position'] === "" || results[i]['f_parent_position'] === "0") {
                            let newFLG = false;
                            if(results[i]["f_disp_date_from"] !== undefined && results[i]["f_new_flg_days"] !== undefined) {
                                let dt = new Date();
                                try {
                                    let dt_old = new Date(results[i]["f_disp_date_from"]);
                                    let dt_len = parseInt(results[i]["f_new_flg_days"]);
                                    if(isNaN(dt_len) === true) {
                                        dt_len = 0;
                                    }
                                    dt_old.setDate(dt_old.getDate() + dt_len);
                                    //console.log(dt);
                                    //console.log(dt, dt_old, dt_len);
                                    if(dt_old.getTime() > (new Date().getTime())) {
                                        newFLG = true;
                                    }
                                } catch (error) {
                                    // console.log(error);
                                }
                            }
                            contentList.push({
                                "content_id": results[i]["f_page_content_sub_id"],
                                "parent_id": parseInt(results[i]["f_parent_position"]),
                                "slag"  : results[i]["f_slag"],
                                "title" : results[i]["f_page_title"],
                                "new_flg" : newFLG,
                                "new_flg_days" : (results[i]["f_disp_date_from"] == null ? "" : moment(new Date(results[i]["f_disp_date_from"])).format("YYYY.MM.DD")),
                                "childs": []
                            });
                            results.splice(i, 1);
                            count_result--;
                            i--;
                        }
                    } catch(e) {
                        console.log(e);
                    }
                };
                
                
                for(let i=0; i<contentList.length; i++) {
                    for(let j=0; j<count_result; j++) {
                        try {
                            // console.log(contentList[i]["content_id"] + " === " + results[j]['f_parent_position']);
                            if(contentList[i]["content_id"] === parseInt(results[j]['f_parent_position'])) {
                                let newFLG = false;
                                if(results[j]["f_disp_date_from"] !== undefined && results[j]["f_new_flg_days"] !== undefined) {
                                    let dt = new Date();
                                    try {
                                        let dt_old = new Date(results[i]["f_disp_date_from"]);
                                        let dt_len = parseInt(results[i]["f_new_flg_days"]);
                                        
                                        if(isNaN(dt_len) === true) {
                                            dt_len = 0;
                                        }
                                        dt_old.setDate(dt_old.getDate() + dt_len);
                                        //console.log(dt);
                                        //console.log(dt, dt_old, dt_len);
                                        if(dt_old.getTime() > (new Date().getTime())) {
                                            newFLG = true;
                                        }
                                    } catch (error) {
                                        // console.log(error);
                                    }
                                }   
                                contentList[i]["childs"].push({
                                    "content_id": results[j]["f_page_content_sub_id"],
                                    "parent_id": parseInt(results[j]["f_parent_position"]),
                                    "slag": results[j]["f_slag"],
                                    "title": results[j]["f_page_title"],
                                    "new_flg" : newFLG,
                                    "new_flg_days" : (results[j]["f_disp_date_from"] == null ? "" : moment(new Date(results[j]["f_disp_date_from"])).format("YYYY.MM.DD")),
                                    "childs": []
                                });
                                results.splice(j, 1);
                                count_result--;
                                j--;
                            }
                        } catch (error) {
                            console.log(error);
                            
                        }
                    }
                }
                
                for(let i=0; i<contentList.length; i++) {
                    for(let j=0; j<contentList[i]["childs"].length; j++) {
                        for(let k=0; k<count_result; k++) {
                            try {
                                if(contentList[i]["childs"][j]["content_id"] === parseInt(results[k]['f_parent_position'])) {
                                    let newFLG = false;
                                    if(results[k]["f_disp_date_from"] !== undefined && results[k]["f_new_flg_days"] !== undefined) {
                                        let dt = new Date();
                                        try {
                                            let dt_old = new Date(results[i]["f_disp_date_from"]);
                                            let dt_len = parseInt(results[i]["f_new_flg_days"]);
                                            
                                            if(isNaN(dt_len) === true) {
                                                dt_len = 0;
                                            }
                                            dt_old.setDate(dt_old.getDate() + dt_len);
                                            console.log(dt);
                                            console.log(dt, dt_old, dt_len);
                                            if(dt_old.getTime() > (new Date().getTime())) {
                                                newFLG = true;
                                            }
                                        } catch (error) {
                                            // console.log(error);
                                        }
                                    }
                                    contentList[i]["childs"][j]["childs"].push({
                                        "content_id": results[k]["f_page_content_sub_id"],
                                        "parent_id": results[k]["f_parent_position"],
                                        "slag": results[k]["f_slag"],
                                        "title": results[k]["f_page_title"],
                                        "new_flg" : newFLG,
                                        "new_flg_days" : (results[k]["f_disp_date_from"] == null ? "" : moment(new Date(results[k]["f_disp_date_from"])).format("YYYY.MM.DD")),
                                        "childs": []
                                    });
                                }
                            } catch (error) {
                                console.log(error);
                                
                            }
                        }
                    }
                }
                // console.log(contentList);
                let u = {
                    "nonce": new Date().getTime(),
                    "status" : "ok",
                    "payload" : contentList
                };

                res.status(200).json(u);
            }
        });
    }
});

// /* GET content by id */
router.post('/page/:id', function(req, res, next) {
    try {
        //check user valid token
        let userInfo = req.userInfo;
        if(userInfo === undefined) {
            res.status(401).json({
                "nonce": new Date().getTime(),
                "status" : "unauthorized"
            });
        } else {
            let content_id = parseInt(req.params.id, 10);
            if(isNaN(content_id) === true ) {
                res.status(400).json({
                    "nonce": new Date().getTime(),
                    "status" : "error: page ID is not number"
                });
            } else {
                let contentModule = req.app.get("contentModule");
                let entities = req.app.get("entities");
                let config = req.app.get("config");
                contentModule.getContentById(content_id, {"level" : userInfo["level"]}, (SQLResult) => {
                    let results = SQLResult.rows;
                    resObj = {};
                    if(results.length === 0) {
                        res.status(404).json({
                            "nonce": new Date().getTime(),
                            "status" : "このページはご利用いただけません。(This page is not available.)"
                        });
                    } else {
                        for(i = 0; i < results.length; i++) {
                            try {
                                if(!(results[i]["f_page_content_id"] in resObj)) {
                                    str = `<!DOCTYPE html>
                                    <html lang="en">
                                    <head>
                                        <meta charset="UTF-8">
                                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                                        <meta http-equiv="X-UA-Compatible" content="ie=edge">
                                        <title>Document</title>
                                        <link rel="stylesheet" ahref="https://exam54.threet.co.jp/daito/css/common.css?1576143400" href="`+ config.get("url.base_url") +`css/common.css?1576143400">
                                        <link rel="stylesheet" ahref="https://exam54.threet.co.jp/daito/css/dkk.css?1576143400" href="`+ config.get("url.base_url") +`css/dkk.css?1576143400">
                                        <style>
                                        html, body {
                                                
                                            width: inherit !important;
                                            min-width: 10px;
                                        }
                                        html, body, header, #content, footer { 
                                            min-width : 1px;
                                            width:100%;
                                        }
                                        #content_left .h3a {
                                            padding: 15px 0 0;
                                            
                                        }
                                        #edit_area p {
                                            font-size: 0.84em;
                                        }
                                        #edit_area h5 {
                                            font-size: 0.93em;
                                        }
                                        #edit_area h6 {
                                            font-size: 0.93em;
                                        }
                                        #edit_area ul {
                                            font-size: 0.82em;
                                        }
                                        
                                        img {
                                            width: 100%;
                                            height: auto;
                                        }
                                        #content {
                                            padding: 10px 0 0px;
                                        }
                                        #content_left {
                                            padding: 0 0 10px;
                                        }
                                        ol.numbered > li {
                                            margin-left: 1.6em;
                                            
                                        }
                                        #privacy-policy .size-large {
                                            font-size: 0.85em;
                                        }
                                        #privacy-policy .inner {
                                            padding-right: 0.75em;
                                            padding-left: 0.75em;
                                        }
                                        </style>
                                    </head>
                                    <body>
                                        <div id="content" style="min-width:100%;">
                                            <div class="box">
                                                <div id="content_left">
                                                    <h3 class="h3a">\n`+ results[i]["f_page_title"] +`</h3>
                                                    <div id="edit_area">
                                                        \n`+ entities.decode(results[i]["f_content"]) +`
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </body>
                                    </html>`;
                                    // fs.writeFile('/home/simec/pavel/projects/kantaku_work/view/content.html', str, function (err) {
                                    //     if (err) console.log(err);
                                    //     console.log('Saved!');
                                    // });
                                    resObj["" + results[i]["f_page_content_id"]] = {
                                        "content_id" : parseInt(results[i]["f_page_content_id"]),
                                        "slag": results[i]["f_slag"],
                                        "title": results[i]["f_page_title"],
                                        "publish_date_from": moment(new Date(results[i]["f_disp_date_from"]).getTime()).format("YYYY.MM.DD"),
                                        "publish_date_to": moment(new Date(results[i]["f_disp_date_to"]).getTime()).format("YYYY.MM.DD"),
                                        "html-base64": Buffer.from(str).toString('base64'),
                                        "files" : [],
                                        "order": results[i]["f_order"],
                                    };
                                }
                            } catch(error) {
                            }
                        }
                        Object.keys(resObj).forEach(function(key) {
                            for(let j=0; j<results.length; j++) {
                                try {
                                    if(parseInt(key) === parseInt(results[j]["f_page_content_id"])) {
                                        resObj[key]["files"].push({
                                            "file_name": results[j]["f_filename"],
                                            "file_path": config.get("url.base_url") + config.get("url.content_file") + results[j]["f_file"],
                                            "file_info"  : { "id": "content_file", "index" : "",  "value" : "" + results[j]["f_file"] },
                                            "file_size": results[j]["f_file_size"],
                                            "file_download_name": results[j]["f_download_name"],
                                            "file_extension": results[j]["f_extension"],
                                        });
                                    }
                                } catch (error) {
                                    console.log(error);
                                }
                            }
                        });
                        let resArr = [];
                        Object.keys(resObj).forEach((e) => {
                            resArr.push(resObj[e]);
                        });

                        //check empty array
                        try{
                            if (Array.isArray(resArr) == true && resArr.length === 0) {        
                                let u = {
                                    "nonce": new Date().getTime(),
                                    "status" : config.get("lang.empty_data"),
                                };
                                res.status(200).json(u);
                            } else {
                                
                                let u = {
                                    "nonce": new Date().getTime(),
                                    "status" : "ok",
                                    "payload" : resArr
                                };
                                res.status(200).json(u);
                            }
                        }catch(error){
                            console.log(error);
                        }
                    }
                });
            }
        }
    }catch (error) {
        res.status(500).json({
            "nonce": new Date().getTime(),
            "status" : "error: " + error
        });
    }
});

module.exports = router;

