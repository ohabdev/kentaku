var express = require('express');
var router = express.Router();
const fs = require('fs');
const moment = require('moment');
const config = require('config');
var cat = {
    "1": "お知らせ",
    "2": "イベント",
    "3": "会報誌",
    "4": "動画",
    "5": "キャンペーン",
    "6": "クラブオフ"
};

/* GET All Arrival list. */
router.post('/list', function(req, res, next) {
    var arrivalModule = req.app.get("arrivalModule");
    // Check user valid Auth token
    let userInfo = req.userInfo;
    if(userInfo === undefined) {
        res.status(401).json({
            "nonce": new Date().getTime(),
            "status" : "unauthorized"
        });
    } else {
        arrivalModule.getArrivalList(userInfo = {"level" : userInfo["level"]}, (SQLResult) => {
            let results = SQLResult.rows;
            let countResult = results.length;
            // console.log(results);
            if(countResult === 0) {
                res.status(404).json({
                    "nonce": new Date().getTime(),
                    "status" : config.get("lang.empty_data")
                });
            } else {
                let config = req.app.get("config");
                var resArr = [];
                try {
                    for(let i=0; i<countResult; i++) {
                        let newFLG = false;
                        if(results[i]["f_disp_date_from"] !== undefined && results[i]["f_new_flg_days"] !== undefined) {
                            let dt = new Date();
                            try {
                                let dt_old = new Date(results[i]["f_disp_date_from"]);
                                let dt_len = parseInt(results[i]["f_new_flg_days"]);
                                if(isNaN(dt_len) === true) {
                                    dt_len = 0;
                                }
                                dt_old.setDate(dt_old.getDate() + dt_len);
                                //console.log(dt);
                                // console.log(dt, dt_old, dt_len);
                                if(dt_old.getTime() > (new Date().getTime())) {
                                    newFLG = true;
                                }
                            } catch (error) {
                                // console.log(error);
                            }
                        }
                        resArr[i] = {
                            "arrival_id": results[i]["f_new_arrival_id"],
                            "arrival_sub_id": results[i]["f_new_arrival_sub_id"],
                            "arrival_title": "" + results[i]["f_list_title"],
                            "arrival_new_flg": newFLG,
                            "category_id": parseInt(results[i]["f_category"]),
                            "category_name": cat[results[i]["f_category"]],
                            "published_date": "" + moment(new Date(results[i]["f_disp_date"])).format("YYYY.MM.DD"),
                            "page_setting_kbn": parseInt(results[i]["f_page_setting_kbn"]),
                            "url": "" + results[i]["f_url"],
                            "url_target": "" + results[i]["f_url_target"],
                            "files" : [
                                {
                                    "file_name": "" + results[i]["f_filename"],
                                    "file_path":  "" +  config.get("url.base_url") + config.get("url.arrival_file") +  results[i]["f_file"],
                                    "file_info"  : { "id": "arrival_file", "index": "", "value" :  "" + results[i]["f_file"] },
                                    "file_size": "" +  results[i]["f_file_size"],
                                    "file_download_name": "" +  results[i]["f_download_name"],
                                    "file_extension":  "" + results[i]["f_extension"],
                                }
                            ]
                        }
                    }
                } catch (error) {
                    
                }
                //Empty result/array check
                try{
                    if (Array.isArray(resArr) == true && resArr.length === 0) {
                        let u = {
                            "nonce": new Date().getTime(),
                            "status" : config.get("lang.no_employ_found")
                        };
                        res.status(404).json(u);
                    } else {
                        
                        let u = {
                            "nonce": new Date().getTime(),
                            "status" : "ok",
                            "payload" : resArr
                        };
                        res.status(200).json(u);
                    }
                }catch(error){
                    res.status(500).json({
                        "nonce": new Date().getTime(),
                        "status" : "error: " + error
                    });
                }
            }
            // let u = {
            //     "nonce": new Date().getTime(),
            //     "status" : "ok",
            //     "payload" : resArr
            // };
            // res.status(200).json(u);
        });
    }
});


// /* GET All Arrival list. */
// router.post('/list', function(req, res, next) {
//     let payload = req.body;
//     if(payload["token"] !==  undefined && typeof payload["token"] === "string") {
//         console.log("token:",tokens["" + payload["token"]]);
//         var arrivalModule = req.app.get("arrivalModule");
//         arrivalModule.getArrivalList(userInfo = {"level": 3}, (SQLResult) => {
//             let results = SQLResult.rows;
//             let countResult = results.length;
//             var resArr = [];
//                 try {
//                     for(let i=0; i<countResult; i++) {
//                         resArr[i] = {
//                             "arrival_id": results[i]["f_new_arrival_id"],
//                             "arrival_title": results[i]["f_list_title"],
//                             "category_id": parseInt(results[i]["f_category"]),
//                             "category_name": cat[results[i]["f_category"]],
//                             "published_date": moment(new Date(results[i]["f_disp_date"])).format("YYYY.MM.DD"),
//                             "page_setting_kbn": parseInt(results[i]["f_page_setting_kbn"]),
//                             "url": results[i]["f_url"],
//                             "url_target": results[i]["f_url_target"],
//                         }
//                     }
//                 } catch (error) {
                    
//                 }

//             let u = {
//                 "nonce": new Date().getTime(),
//                 "status" : "ok",
//                 "payload" : resArr
//             };
//             res.status(200).json(u);
//         });
//     } else {
//         res.status(401).json({"msg": "unauth"});
//     }
// });

// /* GET Arrival by ID */
router.post('/page/:id', function(req, res, next) {
    // Check user valid Auth token
    let userInfo = req.userInfo;
    if(userInfo === undefined) {
        res.status(401).json({
            "nonce": new Date().getTime(),
            "status" : "unauthorized"
        });
    } else {
        let arrival_id = -1;
        try {
            arrival_id = parseInt(req.params.id, 10);
            if(isNaN(arrival_id) === true ) {
                res.status(400).json({
                    "nonce": new Date().getTime(),
                    "status" : "error: page ID is not number"
                });
            } else {
            let arrivalModule = req.app.get("arrivalModule");
            let entities = req.app.get("entities");
            arrivalModule.getArrivalById(arrival_id, userInfo = {"level" : userInfo["level"]}, (SQLResult) => {
                let results = SQLResult.rows;
                let countResult = results.length;
                //console.log(results);
                
                let config = req.app.get("config");
                let resObj = {};
                // console.log(countResult);
                for(let i=0; i<countResult; i++) {
                    // console.log(results[i]["f_new_arrival_id"]);
                    if(!(results[i]["arrival_f_new_arrival_id"] in resObj)) {
                        var str = `<!DOCTYPE html>
                        <html lang="en">
                        <head>
                            <meta charset="UTF-8">
                            <meta name="viewport" content="width=device-width, initial-scale=1.0">
                            <meta http-equiv="X-UA-Compatible" content="ie=edge">
                            <title>Document</title>
                            <link rel="stylesheet" aahref="https://exam54.threet.co.jp/daito/css/common.css?1576143400" href="`+ config.get("url.base_url") +`css/common.css?1576143400">
                            <link rel="stylesheet" aahref="https://exam54.threet.co.jp/daito/css/dkk.css?1576143400" href="`+ config.get("url.base_url") +`css/dkk.css?1576143400">
                            <style>
                                html, body {
                                                
                                            width: inherit !important;
                                            min-width: 10px;
                                        }
                                        
                                        #content_left .h3a {
                                            padding: 15px 0 0;
                                            font-size: 24px;

                                        }
                                        #edit_area p {
                                            font-size: 0.85em;
                                        }
                                        #edit_area h5 {
                                            font-size: 0.93em;
                                        }
                                        #edit_area h6 {
                                            font-size: 0.93em;
                                        }
                                        #edit_area ul {
                                            font-size: 0.82em;
                                        }
                                                                                
                                        img {
                                            width: 100%;
                                            height: auto;
                                        }
                                        #content {
                                            padding: 10px 0 0px;
                                        }
                                        #content_left {
                                            padding: 0 0 10px;
                                        }
                                        ol.numbered > li {
                                            margin-left: 1.6em;
                                            
                                        }
                                        #privacy-policy .size-large {
                                            font-size: 0.85em;
                                        }
                                        #privacy-policy .inner {
                                            padding-right: 0.75em;
                                            padding-left: 0.75em;
                                        }
                            </style>
                        </head>
                        <body>
                            <div id="content" style="min-width:100%;">
                                <div class="box">
                                    <div id="content_left">
                                        <h3 class="h3a">\n`+ results[i]["f_list_title"] +`</h3>
                                        <div id="edit_area">
                                            \n`+ entities.decode(results[i]["f_content"]) +`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </body>
                        </html>`;

                        resObj["" + results[i]["arrival_f_new_arrival_id"] + ""] = {
                            "arrival_id": "" + results[i]["arrival_f_new_arrival_id"] + "",
                            "arrival_title": "" + results[i]["f_list_title"],
                            "content-base64": "" + Buffer.from(str).toString('base64'),
                            "published_date": "" + moment(new Date(results[i]["f_disp_date"]).getTime()).format("YYYY.MM.DD"),
                            "files": [],
                        }
                    }
                }

                Object.keys(resObj).forEach(function(key) {
                    let pdfFile = 0;
                    for(let j=0; j<countResult; j++) {
                        try {
                            if(parseInt(key) === parseInt(results[j]["arrival_f_new_arrival_id"]) ) {
                                if( results[j]["new_ad_filename"] === undefined || results[j]["new_ad_filename"] === null ) {
                                    continue;
                                }
                                // console.log(results[j]["f_filename"]);
                                resObj[key]["files"][pdfFile] =  {
                                    "file_name": "" + results[j]["new_ad_filename"],
                                    "file_path": "" + config.get("url.base_url") + config.get("url.arrival_file") + results[j]["new_ad_file"],
                                    "file_info"  : { "id": "arrvial_detials_file", "index": "", "value" : "" + results[j]["new_ad_file"] },
                                    "file_size": "" + results[j]["new_ad_file_size"],
                                    "file_download_name": "" + results[j]["new_ad_download_name"],
                                    "file_extension": "" + results[j]["new_ad_extension"],
                                };
                            }
                        } catch (error) {
                            
                        }
                        pdfFile++
                    }
                });

                resArr = [];
                Object.keys(resObj).forEach((e) => {
                    resArr.push(resObj[e]);
                });
                // console.log(Array.isArray(resArr));
                // console.log(resArr.length);
                try{
                    if (Array.isArray(resArr) == true && resArr.length === 0) {        
                        let u = {
                            "nonce": new Date().getTime(),
                            "status" : "Error: Data not found",
                        };
                        res.status(404).json(u);
                    } else {
                        
                        let u = {
                            "nonce": new Date().getTime(),
                            "status" : "ok",
                            "payload" : resArr
                        };
                        res.status(200).json(u);
                    }
                }catch(error){
                    res.status(500).json({
                        "nonce": new Date().getTime(),
                        "status" : "error: " + error
                    });
                }
            });
            }
        }catch (error) {
            res.status(500).json({
                "nonce": new Date().getTime(),
                "status" : "error: " + error
            });
        }
    }
});

module.exports = router;

