var express = require('express');
var router = express.Router();

router.get("/", function ( req, res, next) {
    res.status(200).json({"p": "hi all"});
});

/* GET Manager Greetings. */
router.post('/manager', function(req, res, next) {
    try {
        let userInfo = req.userInfo;
        if(userInfo === undefined) {
            res.status(401).json({
                "nonce": new Date().getTime(),
                "status" : "unauthorized"
            });
        } else {
            let aboutModule = req.app.get("aboutModule");
            let config = req.app.get("config");
            aboutModule.managerContent((SQLResult) => {
                let results = SQLResult.rows;
                var count_result = results.length;
                resArr = [];
                for(let i=0; i<count_result; i++) {
                    resArr[i] = {
                        "greeting_content": results[i]["f_greeting_content"],
                        "main_company_name": results[i]["f_main_company_name"],
                        "main_company_url": results[i]["f_main_company_url"],
                        "chairman_ship": results[i]["f_chairman_ship"],
                        "president_name": results[i]["f_president_name"],
                        "president_kana": results[i]["f_president_kana"],
                        "president_image": config.get("url.base_url") + config.get("url.about_img") + results[i]["f_president_image"],
                        "president_list_image": config.get("url.base_url") + config.get("url.about_img") + results[i]["f_president_list_image"],
                        "sign_image": config.get("url.base_url") + config.get("url.about_img") + results[i]["f_sign_image"],
                    }
                }

                let u = {
                    "nonce": new Date().getTime(),
                    "status" : "ok",
                    "payload" : resArr
                };
                res.status(200).json(u);
            });
        }
    } catch (error) {
        res.status(500).json({
            "nonce": new Date().getTime(),
            "status" : "error " + error
        });
    }
    
});

/* GET Address */
router.post('/address', function(req, res, next) {
    try {
        //Check user valid Auth token
        let userInfo = req.userInfo;
        if(userInfo === undefined) {
            res.status(401).json({
                "nonce": new Date().getTime(),
                "status" : "unauthorized"
            });
        } else {

            var aboutModule = req.app.get("aboutModule");
            let config = req.app.get("config");
            aboutModule.addressContent((SQLResult) => {
                var results = SQLResult.rows;
                var  resArr = [];
                if(results.length > 0) {
                    resArr[0]= [
                        {
                            "office"    :   "本部事務局",
                            "address"   :   "〒108-8211　東京都港区港南2-16-1",
                            "tel"       :   "(03) - 6718 - 9250",
                            "fax"       :   "(03) - 6718 - 9251",
                        },
                        {
                            "office"    :   "持株会事務局（大東コーポレートサービス　シェアードサービス部）",
                            "address"   :   "〒140-0002　東京都品川区東品川2-2-8　スフィアタワー天王洲",
                            "tel"       :   "(03) - 6718 - 9056",
                            "fax"       :   "(03) - 6718 - 9057",
                        }
                    ];
                    resArr[1] = [
                        {
                            "file_path"  : config.get("url.base_url") + config.get("url.about_file") + results[0]["f_pdf_file"],
                            "file_info"  : { "id": "about_company_info_file", "index": "", "value" : results[0]["f_pdf_file"] },
                            "file_name"  : results[0]["f_pdf_name"],
                            "file_download_name" : results[0]["f_pdf_name"],
                            "file_size"  : results[0]["f_pdf_size1"],
                            "file_extension" : "pdf"
                        }
                        // {
                        //     "file_path" : config.get("url.base_url") + config.get("url.about_file") + results[0]["f_pdf_directory"],
                        //     "file_info"  : { "id": "about_officers_file", "index": "", "value" : results[0]["f_pdf_directory"] },
                        //     "file_name" : results[0]["f_pdf_directory_name"],
                        //     "file_download_name" : results[0]["f_pdf_directory_name"],
                        //     "file_size" : results[0]["f_pdf_size2"],
                        //     "file_extension" : "pdf"
                        // }
                    ];
                } else {
                    resArr[0]= [
                        {
                            "office"    :   "本部事務局",
                            "address"   :   "〒108-8211　東京都港区港南2-16-1",
                            "tel"       :   "(03) - 6718 - 9250",
                            "fax"       :   "(03) - 6718 - 9251",
                        },
                        {
                            "office"    :   "持株会事務局（大東コーポレートサービス　シェアードサービス部）",
                            "address"   :   "〒140-0002　東京都品川区東品川2-2-8　スフィアタワー天王洲",
                            "tel"       :   "(03) - 6718 - 9056",
                            "fax"       :   "(03) - 6718 - 9057",
                        }
                    ];
                }
                let u = {
                    "nonce": new Date().getTime(),
                    "status" : "ok",
                    "payload" : resArr
                };
                res.status(200).json(u);
            });
        }
    } catch (error) {
        res.status(500).json({
            "nonce": new Date().getTime(),
            "status" : "error " + error
        });
    }
});

router.post('/officers', function(req, res, next) {
    try {

        //Check user valid Auth token
        let userInfo = req.userInfo;
        if(userInfo === undefined) {
            res.status(401).json({
                "nonce": new Date().getTime(),
                "status" : "unauthorized"
            });
        } else {

            var aboutModule = req.app.get("aboutModule");
            let config = req.app.get("config");
            aboutModule.officersContent((SQLResult) => {
                let results = SQLResult.rows;
                let resArr = [];
                resArr.push({
                    "officer_id" : 0,
                    "title": "会長",//results[0]["f_main_company_name"], //
                    "area": "名古屋主幹",//results[0]["f_area"  + i ], //
                    "name": results[0]["f_president_name"],
                    "kana_name": results[0]["f_president_kana"],
                    "company_title": results[0]["f_chairman_ship"],
                    "company_name": results[0]["f_main_company_name"],
                    "company_url": results[0]["f_main_company_url"],
                    "image": config.get("url.base_url") + config.get("url.about_img") + results[0]["f_president_list_image"],
                    "files":
                    [
                        {
                            "file_path" : config.get("url.base_url") + config.get("url.about_file") + results[0]["f_pdf_directory"],
                            "file_info"  : { "id": "about_officers_file", "index": "", "value" : results[0]["f_pdf_directory"] },
                            "file_name" : results[0]["f_pdf_directory_name"],
                            "file_download_name" : results[0]["f_pdf_directory_name"],
                            "file_size" : results[0]["f_pdf_size2"],
                            "file_extension" : "pdf"
                        }
                    ]
                });
                for(let i=1; i<1500 && results.length !== 0 ;i++) {
                    try {
                        if(results[0]["f_title" + i ] === undefined) {
                            break;
                        }
                        
                        resArr.push({
                            "officer_id" : i,
                            "title": results[0]["f_title" + i ], //副会長
                            "area": results[0]["f_area"  + i ],  //神戸主幹
                            "name": results[0]["f_name"  + i ],  //大森　正晴
                            "kana_name": results[0]["f_kana_name"  + i ], //オオモリ　マサハル
                            "company_title": results[0]["f_company_title"  + i ], //代表取締役社長
                            "company_name": results[0]["f_company_name"  + i ],   //株式会社　OMORI
                            "company_url": results[0]["f_company_url"  + i ] ? results[0]["f_company_url"  + i ]: "",
                            "image": config.get("url.base_url") + config.get("url.about_img") + results[0]["f_image"  + i ],
                        });
                    } catch (error) {
                        
                    }
                }
                let u = {
                    "nonce": new Date().getTime(),
                    "status" : "ok",
                    "payload" : resArr
                };
                res.status(200).json(u);
            });
        }
    } catch (error) {
        res.status(500).json({
            "nonce": new Date().getTime(),
                "status" : "server error",
                "payload" : []
        });
    }
   
});

module.exports = router;

