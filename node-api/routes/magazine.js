var express = require('express');
var router = express.Router();
const fs = require('fs');
const moment = require('moment');


/* GET MAGAZINE */
var magazine = undefined;
router.post('/', function(req, res, next) {
    let userInfo = req.userInfo;
    if(userInfo === undefined) {
        res.status(401).json({
            "nonce": new Date().getTime(),
            "status" : "unauthorized"
        });
    } else {
        if(magazine !== undefined  && false) {
            let u = {
                "nonce": new Date().getTime(),
                "status" : "ok",
                "payload" : magazine
            };
            res.status(200).json(u);
        } else {
            magazine = undefined;
            let magazineModule = req.app.get("magazineModule");
            let entities = req.app.get("entities");
            magazineModule.getAllMagazine(userInfo = {"level" : userInfo["level"]}, (SQLResults) => {
                let results = SQLResults.rows;
                let config = req.app.get("config");
                let countResult = results.length;
                let resObj = {};
                if(countResult === 0) {
                    let u = {
                        "nonce": new Date().getTime(),
                        "status" : config.get("lang.empty_data"),
                    };
                    res.status(404).json(u);
                } else {
                    for(let i=0; i<countResult; i++) {
                        if(!(results[i]["f_magazine_id_master"] in resObj)) {
                            let str = 
                            `<!DOCTYPE html>
                            <html lang="en">
                            <head>
                                <meta charset="UTF-8">
                                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                                <meta http-equiv="X-UA-Compatible" content="ie=edge">
                                <title>Document</title>
                                <link rel="stylesheet" aahref="https://exam54.threet.co.jp/daito/css/common.css?1576143400" href="`+ config.get("url.base_url") +`css/common.css?1576143400">
                                <link rel="stylesheet" aahref="https://exam54.threet.co.jp/daito/css/dkk.css?1576143400" href="`+ config.get("url.base_url") +`css/dkk.css?1576143400">

                                <link rel="stylesheet" aahref="https://exam54.threet.co.jp/daito/css/takumi.css?1577182821" type="text/css" media="all" href="`+ config.get("url.base_url") +`css/takumi.css?1577182821">
                         
                                <style>
                                    html, body, header, #content, footer {
                                        width: 100%;
                                        min-width: inherit !important;
                                    }
                                    img {
                                        width:100%;
                                        height:auto;
                                    }
                                    #content {
                                        padding: 10px 0 0px;
                                    }
                                    #content_left {
                                        padding: 0 0 10px;
                                    }
                                    #content_left .h3a {
                                        padding: 15px 0 0;
                                    }
                                    #takumi_list_130{
                                        margin-top:0;
                                    }
                                </style>
                            </head>
                            <body>
                                <div id="content" style="min-width:100%;">
                                    <div class="box">
                                        <div id="content_left">

                                            <section id="takumi_list_100">
                                                <div id="takumi_list_130">
                                                    <div class="right">
                                                        <p class="desc">
                                                            <small>`
                                                            + ( "" + results[i]["f_content"] + "").replace(/(?:\r\n|\r|\n)/g, '<br>\n') +`
                                                        </small>
                                                        </p>
                                                    </div>
                                                </div>
                                            </section>

                                        </div>
                                    </div>
                                </div>
                            </body>
                            </html>`;
                            // fs.writeFile('/home/simec/pavel/projects/kantaku_work/view/magazine.html', str, function (err) {
                            //     if (err) console.log(err);
                            //     console.log('Saved!');
                            // });
                            resObj["" + results[i]["f_magazine_id_master"] + ""] = {
                                "magazine_publish_year": parseInt(moment(results[i]["f_disp_date"]).format("YYYY")),
                                "magazine_id": results[i]["f_magazine_id_master"],
                                "magazine_title": results[i]["magazine_title"],
                                "magazine_publish_date": moment(results[i]["f_disp_date"]).format("YYYY") + "年" +
                                                        moment(results[i]["f_disp_date"]).format("MM") + "月" +
                                                        moment(results[i]["f_disp_date"]).format("DD") + "日発刊",
                                "html-base64": Buffer.from(str).toString('base64'), 
                                "image": config.get("url.base_url") + config.get("url.magazine_img") + results[i]["f_image"],
                                "magazine_issue_no": results[i]["f_magazine_issue_no"],
                                "publish_date" : moment(new Date(results[i]["f_disp_date"]).getTime()).format("YYYY.MM.DD"),
                                "publish_date_long" : new Date(results[i]["f_disp_date"]).getTime(),
                                "image_thumb": config.get("url.base_url") + config.get("url.magazine_img") + results[i]["f_thumb"],
                                "file_name": results[i]["f_filename"],
                                "file_path": config.get("url.base_url") + config.get("url.magazine_file") + results[i]["f_file"],
                                "file_info": { "id": "magazine_file", "index" : "" + "" + "", "value": "" + results[i]["f_file"]},
                                "file_size": results[i]["f_file_size"],
                                "file_download_name": "" + results[i]["f_download_name"],
                                "file_extension" : "" + results[i]["magazine_f_extension"],
                                "view_level": results[i]["f_view_level"],
                                "articles": []
                            };
                        }
                    }

                    Object.keys(resObj).forEach(function(key) {
                        let article = 0;
                        for(let i=0; i<countResult; i++) {
                            try {
                                if(parseInt(key) === parseInt(results[i]["f_magazine_id_master"]) && results[i]["f_magazine_detail_id"] !== null  ) {
                                    resObj[key]["articles"][article] =  {
                                        "id": results[i]["f_magazine_detail_id"],
                                        "title": results[i]["magazine_details_file_title"],
                                        "files" : []
                                    };
                                    file_index=0;
                                    for(let j=1; j<150; j++) {
                                        if( results[i]["f_filename_" + j + ""] === undefined || results[i]["f_filename_" + j + ""] === null ) {
                                            continue;
                                        }
                                        try {
                                            resObj[key]["articles"][article]["files"].push({
                                                "id" : (j-1),
                                                "file_name": results[i]["f_content_" + j],
                                                "file_path":  config.get("url.base_url") + config.get("url.magazine_file") + results[i]["f_file_" + j],
                                                "file_info": { "id": "magazine_detials_file", "index" : "" + j + "",  "value": "" + results[i]["f_file_" + j]},
                                                "file_size": results[i]["f_file_size_" + j],
                                                "file_download_name": "" + results[i]["f_download_name_" + j],
                                                "file_extension" : "" + results[i]["f_extension_" + j],
                                            });
                                        } catch(error) {
                                            console.log(error);
                                        }
                                        file_index++;
                                    }
                                    article++;
                                }
                            } catch(error) {
                                
                            }
                        }
                    });

                    resArr = [];
                    Object.keys(resObj).forEach((e) => {
                        resArr.push(resObj[e]);
                    });

                    resArr.sort((a, b) => {
                        return b.publish_date_long - a.publish_date_long;
                    });

                    if(magazine === undefined) {
                        magazine = resArr;
                    }

                    let u = {
                        "nonce": new Date().getTime(),
                        "status" : "ok",
                        "payload" : magazine
                    };
                    res.status(200).json(u);
                }
            });
        }
    }
});
// ================ Magazine list Year Wise ==============
router.post('/list', function(req, res, next) {
    let userInfo = req.userInfo;
    if(userInfo === undefined) {
        res.status(401).json({
            "nonce": new Date().getTime(),
            "status" : "unauthorized"
        });
    } else {
        if(magazine !== undefined  && false) {
            let u = {
                "nonce": new Date().getTime(),
                "status" : "ok",
                "payload" : magazine
            };
            res.status(200).json(u);
        } else {
            magazine = undefined;
            let magazineModule = req.app.get("magazineModule");
            let entities = req.app.get("entities");
            magazineModule.getMagazineList(userInfo = {"level" : userInfo["level"]}, (SQLResults) => {
                let results = SQLResults.rows;
                let config = req.app.get("config");
                let countResult = results.length;
                // console.log(results);
                // let resObj = {};
                if(countResult === 0) {
                    let u = {
                        "nonce": new Date().getTime(),
                        "status" : config.get("lang.empty_data"),
                    };
                    res.status(404).json(u);
                } else {
                    
                    let magazineObj = {};
                    for(let i=0; i<countResult; i++) {
                        if(magazineObj[moment(results[i]["f_disp_date"]).format("YYYY")] === undefined) {
                            magazineObj[moment(results[i]["f_disp_date"]).format("YYYY")] = [];
                        }
                        try {
                            let str = 
                                `<!DOCTYPE html>
                                <html lang="en">
                                <head>
                                    <meta charset="UTF-8">
                                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                                    <meta http-equiv="X-UA-Compatible" content="ie=edge">
                                    <title>Document</title>
                                    <link rel="stylesheet" href="`+ config.get("url.base_url") +`css/common.css" >
                                    <link rel="stylesheet" href="`+ config.get("url.base_url") +`css/dkk.css" >
                                    <link rel="stylesheet" href="`+ config.get("url.base_url") +`css/takumi.css" type="text/css" media="all" >
                            
                                    <style>
                                        html, body, header, #content, footer {
                                            width: 100%;
                                            min-width: inherit !important;
                                        }
                                        img {
                                            width:100%;
                                            height:auto;
                                        }
                                        #content {
                                            padding: 10px 0 0px;
                                        }
                                        #content_left {
                                            padding: 0 0 10px;
                                        }
                                        #content_left .h3a {
                                            padding: 15px 0 0;
                                        }
                                        #takumi_list_130{
                                            margin-top:0;
                                        }
                                    </style>
                                </head>
                                <body>
                                    <div id="content" style="min-width:100%;">
                                        <div class="box">
                                            <div id="content_left">

                                                <section id="takumi_list_100">
                                                    <div id="takumi_list_130">
                                                        <div class="right">
                                                            <p class="desc">
                                                                <small>`
                                                                + ( "" + results[i]["f_content"] + "").replace(/(?:\r\n|\r|\n)/g, '<br>\n') +`
                                                            </small>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </section>

                                            </div>
                                        </div>
                                    </div>
                                </body>
                                </html>`
                            // fs.writeFile('/home/simec/pavel/projects/kantaku_work/view/magazine.html', str, function (err) {
                            //     if (err) console.log(err);
                            //     console.log('Saved!');
                            // });
                            magazineObj[moment(results[i]["f_disp_date"]).format("YYYY")].push({
                                "magazine_publish_year": parseInt(moment(results[i]["f_disp_date"]).format("YYYY")),
                                "magazine_id": results[i]["f_magazine_id"],
                                "magazine_title": "" +  results[i]["f_title"],
                                "magazine_publish_date": moment(results[i]["f_disp_date"]).format("YYYY") + "年" +
                                                        moment(results[i]["f_disp_date"]).format("MM") + "月" +
                                                        moment(results[i]["f_disp_date"]).format("DD") + "日発刊",
                                "html-base64": Buffer.from(str).toString('base64'), 
                                "image": config.get("url.base_url") + config.get("url.magazine_img") + results[i]["f_image"],
                                "magazine_issue_no": "" + results[i]["f_magazine_issue_no"],
                                "publish_date" : moment(new Date(results[i]["f_disp_date"]).getTime()).format("YYYY.MM.DD"),
                                "publish_date_long" : new Date(results[i]["f_disp_date"]).getTime(),
                                // "image_thumb": config.get("url.base_url") + config.get("url.magazine_img") + results[i]["f_thumb"],
                                // "file_name": results[i]["f_filename"],
                                // "file_path": config.get("url.base_url") + config.get("url.magazine_file") + results[i]["f_file"],
                                // "file_info": { "id": "magazine_file", "index" : "" + "" + "", "value": "" + results[i]["f_file"]},
                                // "file_size": results[i]["f_file_size"],
                                // "file_download_name": "" + results[i]["f_download_name"],
                                // "file_extension" : "" + results[i]["magazine_f_extension"],
                                // "view_level": results[i]["f_view_level"],
                                // "articles": []
                                }
                            );
                        } catch (error) {
                            res.status(401).json({
                                "nonce": new Date().getTime(),
                                "status" : "error:" + error
                            });
                        }
                    }
                    
                    resArr = [magazineObj];
                    // Object.keys(magazineObj).forEach((e) => {
                    //     resArr.push(magazineObj[e]);
                    // });

                    // resArr.sort((a, b) => {
                    //     return b.publish_date_long - a.publish_date_long;
                    // });
                    
                    let u = {
                        "nonce": new Date().getTime(),
                        "status" : "ok",
                        "payload" : resArr
                    };
                    res.status(200).json(u);
                }
            });
        }
    }
});

/* GET Magazine Details*/
router.post('/page/:id', function(req, res, next) {
    let userInfo = req.userInfo;
    if(userInfo === undefined) {
        res.status(401).json({
            "nonce": new Date().getTime(),
            "status" : "unauthorized"
        });
    } else {
        if(magazine !== undefined  && false) {
            let u = {
                "nonce": new Date().getTime(),
                "status" : "ok",
                "payload" : magazine
            };
            res.status(200).json(u);
        } else {
            magazine = undefined;
            let magazineModule = req.app.get("magazineModule");
            let entities = req.app.get("entities");
            let magazine_id = parseInt(req.params.id);
            //console.log(magazine_id);
            magazineModule.getMagazineDetails(magazine_id, userInfo = {"level" : userInfo["level"]}, (SQLResults) => {
                let results = SQLResults.rows;
                let config = req.app.get("config");
                let countResult = results.length;
                let resObj = {};
                if(countResult === 0) {
                    let u = {
                        "nonce": new Date().getTime(),
                        "status" : config.get("lang.empty_data"),
                    };
                    res.status(404).json(u);
                } else {
                    for(let i=0; i<countResult; i++) {
                        // console.log(results[i]["f_magazine_id_master"]);
                        if(!(results[i]["f_magazine_id_master"] in resObj)) {
                            resObj["" + results[i]["f_magazine_id_master"] + ""] = {
                                "magazine_publish_year": "" + parseInt(moment(results[i]["f_disp_date"]).format("YYYY")),
                                "magazine_id": results[i]["f_magazine_id_master"],
                                "magazine_title": results[i]["magazine_title"],
                                // "magazine_publish_date": moment(results[i]["f_disp_date"]).format("YYYY") + "年" +
                                //                         moment(results[i]["f_disp_date"]).format("MM") + "月" +
                                //                         moment(results[i]["f_disp_date"]).format("DD") + "日発刊",
                                // "html-base64": Buffer.from(
                                //     entities.decode(
                                //         results[i]["f_content"]
                                //     )).toString('base64'), 
                                "image": config.get("url.base_url") + config.get("url.magazine_img") + results[i]["f_image"],
                                "magazine_issue_no": "" + results[i]["f_magazine_issue_no"],
                                "publish_date" : moment(new Date(results[i]["f_disp_date"]).getTime()).format("YYYY.MM.DD"),
                                "publish_date_long" : new Date(results[i]["f_disp_date"]).getTime(),
                                // "image_thumb": config.get("url.base_url") + config.get("url.magazine_img") + results[i]["f_thumb"],
                                "file_name": results[i]["f_filename"],
                                "file_path": config.get("url.base_url") + config.get("url.magazine_file") + results[i]["f_file"],
                                "file_info": { "id": "magazine_file", "index" : "" + "" + "", "value": "" + results[i]["f_file"]},
                                "file_size": results[i]["f_file_size"],
                                "file_download_name": "" + results[i]["f_download_name"],
                                "file_extension" : "" + results[i]["magazine_f_extension"],
                                "view_level": results[i]["f_view_level"],
                                "articles": []
                            };
                        }
                    }

                    Object.keys(resObj).forEach(function(key) {
                        let article = 0;
                        for(let i=0; i<countResult; i++) {
                            try {
                                if(parseInt(key) === parseInt(results[i]["f_magazine_id_master"]) && results[i]["f_magazine_detail_id"] !== null  ) {
                                    resObj[key]["articles"][article] =  {
                                        "id": results[i]["f_magazine_detail_id"],
                                        "title": results[i]["magazine_details_file_title"],
                                        "files" : []
                                    };
                                    file_index=0;
                                    for(let j=1; j<150; j++) {
                                        if( results[i]["f_filename_" + j + ""] === undefined || results[i]["f_filename_" + j + ""] === null ) {
                                            continue;
                                        }
                                        try {
                                            resObj[key]["articles"][article]["files"].push({
                                                "id" : (j-1),
                                                "file_name": results[i]["f_content_" + j],
                                                "file_path":  config.get("url.base_url") + config.get("url.magazine_file") + results[i]["f_file_" + j],
                                                "file_info": { "id": "magazine_detials_file", "index" : "" + j + "",  "value": "" + results[i]["f_file_" + j]},
                                                "file_size": results[i]["f_file_size_" + j],
                                                "file_download_name": "" + results[i]["f_download_name_" + j],
                                                "file_extension" : "" + results[i]["f_extension_" + j],
                                            });
                                        } catch(error) {
                                            console.log(error);
                                        }
                                        file_index++;
                                    }
                                    article++;
                                }
                            } catch(error) {
                                
                            }
                        }
                    });

                    resArr = [];
                    Object.keys(resObj).forEach((e) => {
                        resArr.push(resObj[e]);
                    });

                    resArr.sort((a, b) => {
                        return b.publish_date_long - a.publish_date_long;
                    });

                    if(magazine === undefined) {
                        magazine = resArr;
                    }

                    let u = {
                        "nonce": new Date().getTime(),
                        "status" : "ok",
                        "payload" : magazine
                    };
                    res.status(200).json(u);
                }
            });
        }
    }
});

function roughScale(x, base = 10) {
    const parsed = parseInt(x, base);
    if (isNaN(parsed)) { return 0; }
    return parsed;
}

// ================ Magazine list Year Wise ==============
router.post('/yearlist', function(req, res, next) {
    let userInfo = req.userInfo;
    if(userInfo === undefined) {
        res.status(401).json({
            "nonce": new Date().getTime(),
            "status" : "unauthorized"
        });
    } else {
        if(magazine !== undefined  && false) {
            let u = {
                "nonce": new Date().getTime(),
                "status" : "ok",
                "payload" : magazine
            };
            res.status(200).json(u);
        } else {
            magazine = undefined;
            let magazineModule = req.app.get("magazineModule");
            let entities = req.app.get("entities");
            magazineModule.getMagazineList(userInfo = {"level" : userInfo["level"]}, (SQLResults) => {
                let results = SQLResults.rows;
                let config = req.app.get("config");
                let countResult = results.length;
                // console.log(results);
                // let resObj = {};
                if(countResult === 0) {
                    let u = {
                        "nonce": new Date().getTime(),
                        "status" : config.get("lang.empty_data"),
                    };
                    res.status(404).json(u);
                } else {
                    
                    let magazineYearArr = [];
                    for(let i=0; i<countResult; i++) {
                        try {
                            let magazineYear = moment(results[i]["f_disp_date"]).format("YYYY");
                            let dispdate = roughScale(results[i]["f_disp_date"]);
                            let current_year = roughScale(results[i]["f_year"]);
                            let previous_year = current_year - 1;
                            let nxt_previous_year = current_year + 1;
                            let year = current_year;
                            let current_period  = (previous_year*10000) + 401;
                            let previous_period = (current_year*10000)  + 331;
                            let nxt_current_period  = (current_year*10000) + 401;
                            let nxt_previous_period = (nxt_previous_year*10000) + 331;
                            //console.log(dispdate +  " >= " + current_period + " && " + dispdate + " <= " + previous_period);
                            if (dispdate >= current_period && dispdate <= previous_period) {
                                //$list[$previous_year] = $previous_year;
                                magazineYear = previous_year;
                            } else if (dispdate >= nxt_current_period && dispdate <= nxt_previous_period) {
                                //$list[$current_year] = $current_year;
                                magazineYear = current_year;
                            }
                            if(magazineYearArr.includes(magazineYear) === false) {
                                magazineYearArr.push(magazineYear);
                            }
                            
                        } catch (error) {
                            res.status(404).json({
                                "nonce": new Date().getTime(),
                                "status" : "error:" + error
                            });
                            return ;
                        }
                    }
                    
                    resArr = magazineYearArr;
                    // Object.keys(magazineObj).forEach((e) => {
                    //     resArr.push(magazineObj[e]);
                    // });

                    // resArr.sort((a, b) => {
                    //     return b.publish_date_long - a.publish_date_long;
                    // });
                    
                    let u = {
                        "nonce": new Date().getTime(),
                        "status" : "ok",
                        "payload" : resArr
                    };
                    res.status(200).json(u);
                }
            });
        }
    }
});




// ================ Magazine list Year Wise ==============
router.post('/yearmagazinelist', function(req, res, next) {
    let userInfo = req.userInfo;
    if(userInfo === undefined) {
        res.status(401).json({
            "nonce": new Date().getTime(),
            "status" : "unauthorized"
        });
    } else {
        if(magazine !== undefined  && false) {
            let u = {
                "nonce": new Date().getTime(),
                "status" : "ok",
                "payload" : magazine
            };
            res.status(200).json(u);
        } else {
            magazine = undefined;
            let payload = req.body.payload;
            let year = "";
            if(payload !== undefined) {
                year = payload["year"];
                if(year === undefined || typeof year !== "string") {
                    year = "";
                }
            }
            let magazineModule = req.app.get("magazineModule");
            let entities = req.app.get("entities");
            magazineModule.getMagazineListByYear(year, userInfo = {"level" : userInfo["level"]}, (SQLResults) => {
                let results = SQLResults.rows;
                let config = req.app.get("config");
                let countResult = results.length;
                // console.log(results);
                // let resObj = {};
                if(countResult === 0) {
                    let u = {
                        "nonce": new Date().getTime(),
                        "status" : config.get("lang.empty_data"),
                    };
                    res.status(404).json(u);
                } else {
                    
                    let magazineYearArr = [];
                    for(let i=0; i<countResult; i++) {
                        try {
                            let str = 
                            `<!DOCTYPE html>
                            <html lang="en">
                            <head>
                                <meta charset="UTF-8">
                                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                                <meta http-equiv="X-UA-Compatible" content="ie=edge">
                                <title>Document</title>
                                <link rel="stylesheet" href="`+ config.get("url.base_url") +`css/common.css">
                                <link rel="stylesheet" href="`+ config.get("url.base_url") +`css/dkk.css">
                                <link rel="stylesheet" href="`+ config.get("url.base_url") +`css/takumi.css" type="text/css" media="all" >
                         
                                <style>
                                    html, body, header, #content, footer {
                                        width: 100%;
                                        min-width: inherit !important;
                                    }
                                    img {
                                        width:100%;
                                        height:auto;
                                    }
                                    #content {
                                        padding: 10px 0 0px;
                                    }
                                    #content_left {
                                        padding: 0 0 10px;
                                    }
                                    #content_left .h3a {
                                        padding: 15px 0 0;
                                    }
                                    #takumi_list_130{
                                        margin-top:0;
                                    }
                                </style>
                            </head>
                            <body>
                                <div id="content" style="min-width:100%;">
                                    <div class="box">
                                        <div id="content_left">

                                            <section id="takumi_list_100">
                                                <div id="takumi_list_130">
                                                    <div class="right">
                                                        <p class="desc">
                                                            <small>`
                                                            + ( "" + results[i]["f_content"] + "").replace(/(?:\r\n|\r|\n)/g, '<br>\n') +`
                                                        </small>
                                                        </p>
                                                    </div>
                                                </div>
                                            </section>

                                        </div>
                                    </div>
                                </div>
                            </body>
                            </html>`
                            // fs.writeFile('/home/simec/pavel/projects/kantaku_work/view/magazine.html', str, function (err) {
                            //     if (err) console.log(err);
                            //     console.log('Saved!');
                            // });
                            magazineYearArr.push({
                                "magazine_publish_year": parseInt(moment(results[i]["f_disp_date"]).format("YYYY")),
                                "magazine_id": results[i]["f_magazine_id"],
                                "magazine_title": "" +  results[i]["f_title"],
                                "magazine_publish_date": moment(results[i]["f_disp_date"]).format("YYYY") + "年" +
                                                        moment(results[i]["f_disp_date"]).format("MM") + "月" +
                                                        moment(results[i]["f_disp_date"]).format("DD") + "日発刊",
                                "html-base64": Buffer.from(str).toString('base64'), 
                                "image": config.get("url.base_url") + config.get("url.magazine_img") + results[i]["f_image"],
                                "magazine_issue_no": "" + results[i]["f_magazine_issue_no"],
                                "publish_date" : moment(new Date(results[i]["f_disp_date"]).getTime()).format("YYYY.MM.DD"),
                                "publish_date_long" : new Date(results[i]["f_disp_date"]).getTime(),
                                // "image_thumb": config.get("url.base_url") + config.get("url.magazine_img") + results[i]["f_thumb"],
                                // "file_name": results[i]["f_filename"],
                                // "file_path": config.get("url.base_url") + config.get("url.magazine_file") + results[i]["f_file"],
                                // "file_info": { "id": "magazine_file", "index" : "" + "" + "", "value": "" + results[i]["f_file"]},
                                // "file_size": results[i]["f_file_size"],
                                // "file_download_name": "" + results[i]["f_download_name"],
                                // "file_extension" : "" + results[i]["magazine_f_extension"],
                                // "view_level": results[i]["f_view_level"],
                                // "articles": []
                            });
                            
                        } catch (error) {
                            res.status(404).json({
                                "nonce": new Date().getTime(),
                                "status" : "error:" + error
                            });
                            return ;
                        }
                    }
                    
                    resArr = magazineYearArr;
                    // Object.keys(magazineObj).forEach((e) => {
                    //     resArr.push(magazineObj[e]);
                    // });

                    // resArr.sort((a, b) => {
                    //     return b.publish_date_long - a.publish_date_long;
                    // });
                    
                    let u = {
                        "nonce": new Date().getTime(),
                        "status" : "ok",
                        "payload" : resArr
                    };
                    res.status(200).json(u);
                }
            });
        }
    }
});

module.exports = router;
