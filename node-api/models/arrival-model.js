var mysqlDB;

exports.init = function (mysqlDB) {
    this.mysqlDB = mysqlDB;
};

exports.getArrivalList = function (userInfo = { "level": 3 }, callback) {
    let sql = "SELECT \
            new_arrival.f_new_arrival_id ,new_arrival.f_new_arrival_sub_id , \
            new_arrival.f_list_title ,new_arrival.f_category, new_arrival.f_page_setting_kbn ,new_arrival.f_url ,\
            new_arrival.f_url_target ,new_arrival.f_disp_date_from ,new_arrival.f_disp_date_to ,new_arrival.f_disp_date ,\
            new_arrival.f_open_flg ,new_arrival.f_new_flg ,new_arrival.f_new_flg_days ,new_arrival.f_content ,new_arrival.f_filename ,\
            new_arrival.f_file ,new_arrival.f_extension ,new_arrival.f_file_size ,new_arrival.f_download_name ,new_arrival.f_view_level ,\
            new_arrival.f_upd_time FROM (SELECT new_arrival1.f_new_arrival_id ,new_arrival1.f_new_arrival_sub_id ,new_arrival1.f_list_title ,\
            new_arrival1.f_category ,new_arrival1.f_page_setting_kbn ,new_arrival1.f_url ,new_arrival1.f_url_target ,\
            date_format(new_arrival1.f_disp_date_from, '%Y/%m/%d %H:%i:%S') AS f_disp_date_from ,\
            date_format(new_arrival1.f_disp_date_to, '%Y/%m/%d %H:%i:%S') AS f_disp_date_to ,\
            date_format(new_arrival1.f_disp_date, '%Y.%m.%d') AS f_disp_date ,new_arrival1.f_open_flg ,\
            new_arrival1.f_new_flg ,new_arrival1.f_new_flg_days ,new_arrival1.f_content ,new_arrival1.f_filename ,\
            new_arrival1.f_file ,new_arrival1.f_extension ,new_arrival1.f_file_size ,new_arrival1.f_download_name ,\
            new_arrival1.f_view_level ,new_arrival1.f_upd_time \
        FROM t_new_arrival new_arrival1 \
        WHERE \
            new_arrival1.f_del_flg = '0' AND new_arrival1.f_open_flg = '1' AND new_arrival1.f_view_level <= ? \
            AND new_arrival1.f_new_arrival_id = ( \
                SELECT f_new_arrival_id FROM t_new_arrival \
                WHERE \
                    f_del_flg = '0' AND f_open_flg = '1' AND f_disp_date_from <= now() \
                    AND (f_disp_date_to >= now() OR f_disp_date_to IS NULL) \
                    AND f_new_arrival_sub_id = new_arrival1.f_new_arrival_sub_id ORDER BY ABS(NOW() - f_disp_date_from) ASC LIMIT 0, 1) \
                GROUP BY new_arrival1.f_new_arrival_sub_id ) new_arrival \
        ORDER BY new_arrival.f_disp_date DESC, new_arrival.f_upd_time DESC";

    this.mysqlDB.excSQL(sql, [parseInt(userInfo["level"])], callback);
}





exports.getArrivalById = function (arrival_id, userInfo = { "level": 3 }, callback) {
    let sql = "SELECT\
                new_arrival.f_new_arrival_id as \"arrival_f_new_arrival_id\" ,\
                new_arrival.f_new_arrival_sub_id,\
                new_arrival.f_list_title,\
                new_arrival.f_category,\
                new_arrival.f_page_setting_kbn,\
                new_arrival.f_url,\
                new_arrival.f_url_target,\
                DATE_FORMAT(\
                    new_arrival.f_disp_date_from,\
                    '%Y/%m/%d'\
                ) AS f_disp_date_from,\
                DATE_FORMAT(\
                    new_arrival.f_disp_date_to,\
                    '%Y/%m/%d'\
                ) AS f_disp_date_to,\
                DATE_FORMAT(\
                    new_arrival.f_disp_date,\
                    '%Y/%m/%d'\
                ) AS f_disp_date,\
                DATE_FORMAT(\
                    new_arrival.f_disp_date_from,\
                    '%k'\
                ) AS f_disp_hour_from,\
                DATE_FORMAT(\
                    new_arrival.f_disp_date_from,\
                    '%i'\
                ) AS f_disp_minute_from,\
                DATE_FORMAT(new_arrival.f_disp_date_to, '%k') AS f_disp_hour_to,\
                DATE_FORMAT(new_arrival.f_disp_date_to, '%i') AS f_disp_minute_to,\
                DATE_FORMAT(\
                    new_arrival.f_disp_date_to,\
                    '%Y/%m/%d %H:%i:%s'\
                ) AS f_disp_datetime_to,\
                DATE_FORMAT(\
                    new_arrival.f_disp_date_from,\
                    '%Y/%m/%d %H:%i:%s'\
                ) AS f_disp_datetime_from,\
                new_arrival.f_open_flg,\
                new_arrival.f_new_flg,\
                new_arrival.f_new_flg_days,\
                new_arrival.f_content,\
                new_arrival.f_filename as new_a_filename,\
                new_arrival.f_file as new_a_file,\
                new_arrival.f_extension as new_a_extension,\
                new_arrival.f_file_size as new_a_file_size,\
                new_arrival.f_download_name as new_a_download_name,\
                new_arrival.f_view_level,\
                new_arrival.f_del_flg,\
                new_arrival.f_reg_account,\
                new_arrival.f_reg_time,\
                new_arrival.f_upd_account,\
                new_arrival.f_upd_time,\
                new_arrival.f_file as new_arrival_file,\
                t_new_arrival_file.f_file as new_ad_file,\
                t_new_arrival_file.f_filename as new_ad_filename,\
                t_new_arrival_file.f_file_size as new_ad_file_size,\
                t_new_arrival_file.f_download_name as new_ad_download_name,\
                t_new_arrival_file.f_extension as new_ad_extension,\
                t_new_arrival_file.*\
            FROM\
                t_new_arrival new_arrival\
                LEFT JOIN  \
                        `t_new_arrival_file` \
                    ON  `new_arrival`.`f_new_arrival_id` = `t_new_arrival_file`.`f_new_arrival_id` \
                        AND t_new_arrival_file.f_del_flg = '0' \
            WHERE \
                new_arrival.f_del_flg = '0' \
                AND new_arrival.f_open_flg = '1' AND new_arrival.f_new_arrival_id = ? and new_arrival.f_view_level <= ? ";
    this.mysqlDB.excSQL(sql, [arrival_id, parseInt(userInfo["level"])], callback);
}

// exports.getArrivalById = function (arrival_id, userInfo = {"level": 3}, callback) {
//     let sql = "SELECT `a`.* , `ad`.* \
//         FROM `t_new_arrival` AS `a` \
//         LEFT JOIN `t_new_arrival_file` AS `ad` \
//             ON `a`.`f_new_arrival_id` = `ad`.`f_new_arrival_id` \
//                 AND `ad`.`f_del_flg` = 0 \
//         WHERE `a`.`f_del_flg` = 0 \
//             AND `a`.`f_page_setting_kbn` = 3 \
//             AND `a`.`f_new_arrival_id` = ? \
//             AND `a`.`f_view_level` = ? \
//         ORDER BY `a`.`f_disp_date` DESC, `ad`.`f_order` ASC";
//     this.mysqlDB.excSQL(sql, [arrival_id, parseInt(userInfo["level"])], callback);
// }

