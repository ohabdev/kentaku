var mysqlDB;
exports.init = function (mysqlDB) {
    this.mysqlDB = mysqlDB;
};

// exports.getAllMagazine = function (callback) {
//     let sql = 
//     "SELECT \
//         `m`.`f_magazine_id`,`m`.`f_title`,`m`.`f_magazine_issue_no`,`m`.`f_disp_date`,`m`.`f_content`, \
//         `m`.`f_image`,`m`.`f_thumb`,`m`.`f_filename`,`m`.`f_file`,`m`.`f_file_size`,`m`.`f_download_name`, \
//         `m`.`f_view_level`, \
//         `md`.`f_title`,`md`.`f_filename_1`,`md`.`f_filename_2`,`md`.`f_filename_3`, `md`.`f_filename_4`, \
//         `md`.`f_filename_5`,`md`.`f_file_1`,`md`.`f_file_2`,`md`.`f_file_3`,`md`.`f_file_4`,`md`.`f_file_5`, \
//         `md`.`f_extension_1`,`md`.`f_extension_2`,`md`.`f_extension_3`,`md`.`f_extension_4`,`md`.`f_extension_5`, \
//         `md`.`f_file_size_1`,`md`.`f_file_size_2`,`md`.`f_file_size_3`,`md`.`f_file_size_4`,`md`.`f_file_size_5`, \
//         `md`.`f_content_1`,`md`.`f_content_2`,`md`.`f_content_3`,`md`.`f_content_4`,`md`.`f_content_5`, \
//         `md`.`f_download_name_1`,`md`.`f_download_name_2`,`md`.`f_download_name_3`,`md`.`f_download_name_4`, \
//         `md`.`f_download_name_5`,`m`.`f_title` `magazine_title`,`md`.`f_title` `magazine_details_title`\
//     FROM `t_magazine` AS `m` \
//     LEFT JOIN `t_magazine_detail` AS `md` \
//         ON `m`.`f_magazine_id` = `md`.`f_magazine_id` \
//         WHERE \
//             `m`.`f_del_flg` = 0 \
//             AND `md`.`f_del_flg` = 0 \
//     ORDER BY \
//         `m`.`f_disp_date` DESC, `md`.`f_order` ASC";
//     this.mysqlDB.excSQL(sql, [], callback);
// }

exports.getAllMagazine = function (userInfo = {"level": 0}, callback) {
    let sql = 
    "SELECT \
        `m`.`f_magazine_id` as  \"f_magazine_id_master\", \
        `m`.* , \
        `m`.`f_extension` as  \"magazine_f_extension\", \
        `m`.`f_title` as \"magazine_title\", \
        `md`.`f_title` as \"magazine_details_file_title\", \
        `md`.* \
    FROM \
        `t_magazine` AS `m` \
    LEFT JOIN \
        `t_magazine_detail` AS `md` \
    ON  `m`.`f_magazine_id` = `md`.`f_magazine_id` \
        AND `md`.`f_del_flg` = 0 \
    WHERE \
        `m`.`f_del_flg` = 0 \
        AND `m`.`f_disp_flg` = 0 \
        AND `m`.`f_view_level` <= ? \
    ORDER BY \
        `m`.`f_disp_date` DESC " ;
    this.mysqlDB.excSQL(sql, [parseInt(userInfo["level"])], callback);
}



exports.getMagazineList = function (userInfo = {"level": 0}, callback) {
    let sql = 
    "SELECT \
        date_format(magazine.f_disp_date, '%Y') AS f_year, \
        date_format(magazine.f_disp_date, '%Y%m%d') AS f_disp_date \
    FROM t_magazine magazine \
    WHERE \
            magazine.f_del_flg = '0' \
        AND magazine.f_disp_date_from <= CURDATE() \
        AND magazine.f_disp_flg = '0' \
        AND date_format(magazine.f_disp_date, '%Y') <= YEAR(NOW()) \
        AND magazine.f_view_level <= ? \
    ORDER BY date_format(magazine.f_disp_date_from, '%Y') DESC ";
    this.mysqlDB.excSQL(sql, [parseInt(userInfo["level"])], callback);
}



exports.getMagazineList_old = function (userInfo = {"level": 0}, callback) {
    let sql = 
    "SELECT \
        `m`.* \
    FROM \
        `t_magazine` AS `m` \
    WHERE \
        `m`.`f_del_flg` = 0 \
        AND `m`.`f_disp_flg` = 0 \
        AND `m`.`f_view_level` <= ? \
    ORDER BY \
        `m`.`f_disp_date` DESC " ;
    this.mysqlDB.excSQL(sql, [parseInt(userInfo["level"])], callback);
}



exports.getMagazineListByYear = function (year = "", userInfo = {"level": 0}, callback) {
    let startYear   = ("" + (parseInt(year) +0) ) + "0401";
    let endYear     = ("" + (parseInt(year) +1) ) + "0331";

    //console.log(startYear, endYear);
    let sql = 
    "SELECT \
        `m`.* \
    FROM \
        `t_magazine` AS `m` \
    WHERE \
        m.f_disp_date_from <= NOW() \
        AND m.f_disp_date >= ? \
        AND m.f_disp_date <= ? \
        AND `m`.`f_del_flg` = 0 \
        AND `m`.`f_disp_flg` = 0 \
        AND `m`.`f_view_level` <= ? \
    ORDER BY \
        `m`.`f_disp_date` DESC " ;

    this.mysqlDB.excSQL(sql, [startYear, endYear, parseInt(userInfo["level"])], callback);
}

exports.getMagazineDetails = function (magazine_id, userInfo = {"level": 0}, callback) {
    let sql = 
    "SELECT \
        `m`.`f_magazine_id` as  \"f_magazine_id_master\", \
        `m`.* , \
        `m`.`f_extension` as  \"magazine_f_extension\", \
        `m`.`f_title` as \"magazine_title\", \
        `md`.`f_title` as \"magazine_details_file_title\", \
        `md`.* \
    FROM \
        `t_magazine` AS `m` \
    LEFT JOIN \
        `t_magazine_detail` AS `md` \
    ON  `m`.`f_magazine_id` = `md`.`f_magazine_id` \
        AND `md`.`f_del_flg` = 0 \
    WHERE \
        `m`.`f_del_flg` = 0 \
        AND `m`.`f_disp_flg` = 0 \
        AND `m`.`f_view_level` <= ? \
        AND `m`.`f_magazine_id` = ? \
    ORDER BY \
        `md`.`f_order`ASC, \
        `m`.`f_disp_date` DESC" ;
    this.mysqlDB.excSQL(sql, [parseInt(userInfo["level"]), magazine_id], callback);
}