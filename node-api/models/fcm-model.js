var mysqlDB;

exports.init = function (mysqlDB) {
    this.mysqlDB = mysqlDB;
};

exports.getAllFCM_old = function (agencyId, callback) {
    sql ="SELECT \
        * \
        FROM `t_message` msg \
        WHERE msg.`f_agency_id` = ? \
            AND msg.`f_schedule_start_date_time` < now() \
            AND (msg.`f_send_flg` != null OR msg.`f_send_flg` != 0) \
            AND `f_del_flg` = 0 \
        ORDER BY `msg`.`f_schedule_start_date_time` DESC ";
    this.mysqlDB.excSQL(sql, [agencyId], callback);
}

exports.getAllFCM = function (agencyId, callback) {
    sql ="SELECT \
        * \
        FROM `t_message` msg \
        WHERE msg.`f_agency_id` = ? \
            AND msg.`f_schedule_start_date_time` < now() \
            AND (msg.`f_send_flg` != null OR msg.`f_send_flg` != 0) \
            AND `f_del_flg` = 0 \
        ORDER BY `msg`.`f_upd_time` DESC ";
    this.mysqlDB.excSQL(sql, [agencyId], callback);
}

exports.getAllFCMNewCount = function (agencyId, callback) {
    sql ="SELECT \
        count(*) as \"count\" \
        FROM `t_message` msg \
        WHERE \
            `f_agency_id` = ? \
            AND `f_schedule_start_date_time` <= NOW() \
            AND (`f_send_flg` != 2 AND `f_send_flg` != 0 ) \
            AND `f_del_flg` = 0 ";
    this.mysqlDB.excSQL(sql, [parseInt(agencyId)], callback);
}

exports.fcmMsg = function (agencyId, messageTitle, messageBody, scheduleTime, pageUrl, callback) {
    var sql = "INSERT INTO `t_message` (`f_push_reserve_id`, `f_agency_id`, `f_message_title`, \
                `f_message`, `f_schedule_start_date_time`, `f_url`, `f_send_flg`) VALUES ('2', ?, ?, \
                ?, ?, ?, '0') ";
        

    this.mysqlDB.excSQL(sql, [agencyId, messageTitle, messageBody, scheduleTime, pageUrl], callback);
    // console.log(this.mysqlDB.excSQL(sql, [agencyId, messageTitle, messageBody, scheduleTime, pageUrl], callback));
}

exports.updateAllFMessagesIfSendFlagNot2 = function ( agencyId, callback ) {
    sql ="UPDATE\
            `t_message`\
        SET\
            `f_send_flg` = 2\
        WHERE\
            `f_send_flg` != 2 AND `f_agency_id` = ?";
    this.mysqlDB.excSQL( sql, [ agencyId ], callback );
}


exports.updateFMessagesbyID = function ( messageID = -1, callback ) {
    
    sql ="UPDATE\
            `t_message`\
        SET\
            `f_send_flg` = 2\
        WHERE\
            `f_send_flg` != 2 AND `f_message_id` = ?";
    //console.log(sql, messageID);
    
    this.mysqlDB.excSQL( sql, [ messageID ], callback );
}
