var mysqlDB;
const crypto = require('crypto');

exports.init = function (mysqlDB) {
    this.mysqlDB = mysqlDB;
};

exports.getAgencyInfo = function (agencyId, callback) {
    let sql = "SELECT\
                IFNULL(ma.`f_foreign_nationality_flg`, 0) as \"foreign_nationality_flg\", \
                ma.f_agency_id,\
                ma.f_agency_type,\
                ma.f_emp_kbn,\
                ma.f_branch_cd,\
                ma.f_agency_cd,\
                ma.f_company_cd,\
                ma.f_sales_cd,\
                ma.f_trad_company_category,\
                ma.f_industry,\
                ma.f_occupation,\
                ma.f_emp_cd,\
                ma.f_name,\
                ma.f_name_kana,\
                ma.f_director_h_kbn,\
                ma.f_director_s_kbn,\
                ma.f_sales_name,\
                ma.f_representative_name,\
                ma.f_representative_name_kana,\
                ma.f_siten_cd,\
                ma.f_parent_id,\
                ma.f_name2,\
                ma.f_name2_kana,\
                ma.f_foreign_nationality_flg,\
                ma.f_foreign_country,\
                ma.f_foreign_emp_flg,\
                ma.f_zip,\
                ma.f_prefecture_id,\
                ma.f_town,\
                ma.f_address,\
                ma.f_no_emp,\
                ma.f_tel,\
                ma.f_fax,\
                ma.f_url,\
                ma.f_mailaddress,\
                ma.f_id,\
                ma.f_password,\
                ma.f_password AS f_password_conf,\
                ma.f_csv_del_off_flg,\
                ma.f_view_level,\
                ma.f_register_date,\
                ma.f_acct_valid_flg,\
                ma.f_authentication_flg,\
                ma.f_auth,\
                ma.f_register_date,\
                ma.f_basic_contract_date,\
                ma.f_date_consignment_contract,\
                ma.f_temporary_suspension_date,\
                ma.f_suspension_release_date,\
                ma.f_cancellation_date,\
                ma.f_revival_date,\
                ma.f_membership_date,\
                ma.f_membership_withdrawal_date,\
                ma.f_del_flg,\
                ma.f_reg_account,\
                ma.f_reg_time,\
                ma.f_upd_account,\
                ma.f_upd_time\
            FROM\
                m_agency ma\
            WHERE\
                ma.f_del_flg = '0' AND ma.f_agency_id = ?";

    this.mysqlDB.excSQL(sql, [agencyId], callback);
}

exports.ckAgencyValid = function (agencyId, callback) {
    let sql = "SELECT \
                1 as count\
            FROM\
                m_agency ma\
            WHERE\
                    ma.f_del_flg = '0' \
                AND ma.f_acct_valid_flg = 1 \
                AND ma.f_auth = 1 \
                AND ma.f_agency_id = ? ";
    //console.log(sql);
    
    this.mysqlDB.excSQL(sql, [agencyId], callback);
}

exports.getAgencyLogin = function (user, password, callback) {
    if (
        (typeof user === "undefined" || user === null)
        ||
        (typeof password === "undefined" || password === null)
    ) {
        callback({ "rows": [], "len": 0, "status": "error " + "user or password" });

    } else {
        // let sql = 
        //     "SELECT \
        //         * \
        //     FROM `m_agency` \
        //     where \
        //         `f_id` = ? \
        //         and f_del_flg = 0 ";
        // console.log(sql, [user]);
        let sql = "SELECT\
                    IFNULL(agency.f_acct_valid_flg, 2) as f_acct_valid_flg_new, \
                    IFNULL(agency.f_auth, 0) as f_auth_new, \
                    agency.* \
                FROM\
                    m_agency agency\
                WHERE\
                    agency.f_id = ? AND agency.f_del_flg = '0'"; // f_acct_valid_flag = 1 and f_auth = 1 

        this.mysqlDB.excSQL(sql, [user], (SQlResult) => {
            let userInfo = SQlResult["rows"];
            // console.log(userInfo);

            if (userInfo.length !== 0) {
                userInfo = userInfo[0];
                // console.log(userInfo);

                let salt = userInfo["f_password"].substring(0, 16);
                let pass = crypto.createHash('sha256').update(password + salt).digest('hex');
                //console.log(salt + pass === userInfo["f_password"]);
                if (salt + pass === userInfo["f_password"]) {
                    callback([userInfo]);
                } else {
                    callback([]);
                }

            }
            else {
                callback([]);
            }
        });
    }
}


//get All Industry
exports.getAllGnavi = function (userInfo = { "level": 0 }, callback) {
    let sql = "SELECT *, IF(`f_kbn` <= ? , TRUE, FALSE) as FLAG FROM `t_gnavi`";
    let parm = [userInfo["level"]];
    this.mysqlDB.excSQL(sql, parm, callback);
}


// get All Industry
exports.getAllIndustry = function (callback) {
    let sql = "SELECT\
                industry.f_industry_id as id,\
                industry.f_industry_name as name\
                FROM m_industry industry\
                WHERE industry.f_del_flg = '0'";
    this.mysqlDB.excSQL(sql, [], callback);
}

// get All Occupation
exports.getAllOccupation = function (callback) {
    let sql = "SELECT\
                occupation.f_occupation_id as id,\
                occupation.f_occupation_name as name \
                FROM m_occupation occupation\
                WHERE occupation.f_del_flg = '0'";
    this.mysqlDB.excSQL(sql, [], callback);
}

//get All Siten
exports.getAllSiten = function (callback) {
    let sql = "SELECT\
                siten.f_siten_id as id,\
                siten.f_siten_name as name \
            FROM m_siten siten\
                WHERE siten.f_del_flg = '0'";
    this.mysqlDB.excSQL(sql, [], callback);
}

//get All Siten
exports.getAllAgency = function (callback) {
    let sql = "SELECT\
                IFNULL(agency.`f_foreign_nationality_flg`, 0) as \"foreign_nationality_flg\", \
                IFNULL(agency.f_acct_valid_flg, 2) as f_acct_valid_flg_new, \
                IFNULL(agency.f_auth, 0) as f_auth_new, \
                agency.* \
            FROM\
                m_agency agency\
            WHERE\
                agency.f_del_flg = '0'";
    this.mysqlDB.excSQL(sql, [], callback);
}

// Employee List Zip code wise
exports.employeeList = function (parentId,callback) {
    let sql = "SELECT\
                f_agency_id,\
                f_agency_type,\
                f_branch_cd,\
                f_agency_cd,\
                f_sales_cd,\
                f_name,\
                f_emp_cd,\
                f_name2,\
                f_zip,\
                f_prefecture_id,\
                f_town,\
                f_address,\
                f_tel,\
                f_fax,\
                f_url,\
                f_mailaddress,\
                f_id,\
                f_view_level,\
                f_acct_valid_flg\
            FROM\
                (\
            SELECT\
                ma.f_agency_id,\
                ma.f_agency_type,\
                ma.f_branch_cd,\
                ma.f_agency_cd,\
                ma.f_sales_cd,\
                ma.f_name,\
                ma.f_emp_cd,\
                ma.f_name2,\
                ma.f_zip,\
                ma.f_prefecture_id,\
                ma.f_town,\
                ma.f_address,\
                ma.f_tel,\
                ma.f_fax,\
                ma.f_url,\
                ma.f_mailaddress,\
                ma.f_id,\
                ma.f_view_level,\
                ma.f_acct_valid_flg\
            FROM\
                m_agency ma\
            WHERE\
                ma.f_del_flg = '0' AND ma.f_parent_id = ?\
            ) results\
            ORDER BY\
                f_agency_id ASC\
            LIMIT 0,100";
    this.mysqlDB.excSQL(sql, [parentId], callback);
}

// Edit view for single Employee
exports.employeeInfo = function (empoloyeeId, callback) {
    let sql = "SELECT\
                IFNULL(agency.`f_foreign_nationality_flg`, 0) as \"foreign_nationality_flg\",\
                agency.* \
            FROM\
                m_agency agency\
            WHERE\
                agency.f_del_flg = '0'\
                AND agency.f_agency_id = ?";
    // let sql = "";
    this.mysqlDB.excSQL(sql, [empoloyeeId], callback);
}

// ======== Employee info update ==========
exports.employeeInfoUpdate = function (agencyId, updateData, password, callback) {
    let mailAddress     =   updateData['mailAddress'];
    let userName        =   updateData['userName'];
    let fullName        =   updateData['fullName'];
    let acctValidFlg    =   updateData['acctValidFlg'];
    // let viewLevel = 2;
    // if(agencyType === 6){
    //     viewLevel = 3;
    // }
    let foNaFlg         =   updateData['foNaFlg'];
    let foreignCountry  =   updateData['foreignCountry'];
    let occupations  =   updateData['occupations'];

    if ( mailAddress === null ) {
        let sql = "UPDATE `m_agency`\
            SET \
                `f_id` = ?,\
                `f_name2`    = ?,\
                `f_acct_valid_flg`  = ?,\
                `f_foreign_nationality_flg`  = ?,\
                `f_foreign_country`  = ?,\
                `f_occupation`  = ?,\
                `f_password`    = ?\
            WHERE\
                f_del_flg = '0'\
                AND f_agency_id = ?";
        this.mysqlDB.excSQL(sql, [userName, fullName, acctValidFlg, foNaFlg, foreignCountry, occupations, password, agencyId], callback);
    } else {
        let sql = "UPDATE `m_agency`\
            SET \
                `f_id` = ?,\
                `f_mailaddress` = ?,\
                `f_name2`    = ?,\
                `f_acct_valid_flg`  = ?,\
                `f_foreign_nationality_flg`  = ?,\
                `f_foreign_country`  = ?,\
                `f_occupation`  = ?,\
                `f_password`    = ?\
            WHERE\
                f_del_flg = '0'\
                AND f_agency_id = ?";
        this.mysqlDB.excSQL(sql, [mailAddress, userName, fullName, acctValidFlg, foNaFlg, foreignCountry, occupations, password, agencyId], callback);
    }
}
// ======== Employee Delete ==========
exports.employeeDelete = function (agencyId, employeeId, callback) {
    let sql = "UPDATE\
                    `m_agency`\
                SET\
                    `f_del_flg` = '1',\
                    `f_upd_account` = ?,\
                    `f_upd_time` = now()\
                WHERE\
                    `f_agency_id` = ?";
    this.mysqlDB.excSQL(sql, [agencyId, employeeId], callback);
}
// ======== Update Agency Info ==========
exports.adminInfoUpdate = function (agencyId, updateData, password, callback) {
    let mailAddress = updateData["f_mailaddress"];
    // let userId      = updateData["f_id"];
    let occupations =   updateData['occupations'];
    let sql = "UPDATE `m_agency`\
            SET \
                `f_mailaddress` = ?,\
                `f_occupation` = ?,\
                `f_password`    = ?\
            WHERE\
                f_del_flg = '0'\
                AND f_agency_id = ?";
    this.mysqlDB.excSQL(sql, [mailAddress, occupations, password, agencyId], callback);
}

// ======== Update Register Info ==========
exports.registerInfoUpdate = function (agencyId, updateData, password, callback) {
    let mailAddress     =   updateData['mailAddress'];
    let fullName    =   updateData['fullName'];
    let userId      =   updateData['userId'];
    let agencyType  =   updateData['agencyType'];
    
    let viewLevel = 2;
    if(agencyType === 6){
        viewLevel = 3;
    }
    let foNaFlg         =   updateData['foNaFlg'];
    let foreignCountry  =   updateData['foreignCountry'];
    if ( mailAddress === null ) {
        let sql = "UPDATE `m_agency`\
                SET \
                    `f_agency_type` = ?,\
                    `f_name2`       = ?,\
                    `f_id`          = ?,\
                    `f_foreign_nationality_flg`  = ?,\
                    `f_foreign_country`  = ?,\
                    `f_view_level`  = ?,\
                    `f_password`    = ?\
                WHERE\
                    f_del_flg = '0'\
                    AND f_agency_id = ?";
        this.mysqlDB.excSQL(sql, [agencyType, fullName, userId, foNaFlg, foreignCountry, viewLevel, password, agencyId], callback);        
    } else {
        let sql = "UPDATE `m_agency`\
                SET \
                    `f_agency_type` = ?,\
                    `f_name2`       = ?,\
                    `f_id`          = ?,\
                    `f_mailaddress` = ?,\
                    `f_foreign_nationality_flg`  = ?,\
                    `f_foreign_country`  = ?,\
                    `f_view_level`  = ?,\
                    `f_password`    = ?\
                WHERE\
                    f_del_flg = '0'\
                    AND f_agency_id = ?";
        this.mysqlDB.excSQL(sql, [agencyType, fullName, userId, mailAddress, foNaFlg, foreignCountry, viewLevel, password, agencyId], callback);
    }
}


// ======== Update Agency Info ==========
exports.agencyInfoUpdate = function (agencyId, updateData, password, callback) {
    let mailAddress =   updateData['mailAddress'];
    // let userName    =   updateData['userName'];
    let empNo       =   updateData['empNo'];
    let fullName    =   updateData['fullName'];
    let industries  =   updateData['industries'];
    // let occupations =   updateData['occupations'];
    let sitens      =   updateData['sitens'];
    let foNaFlg     =   updateData['foNaFlg'];

    let sql = "UPDATE `m_agency`\
            SET \
                `f_mailaddress` = ?,\
                `f_no_emp`          = ?,\
                `f_name2`          = ?,\
                `f_industry`    = ?,\
                `f_siten_cd`    = ?,\
                `f_foreign_nationality_flg`   = ?,\
                `f_password`    = ?\
            WHERE\
                f_del_flg = '0'\
                AND f_agency_id = ?";
    this.mysqlDB.excSQL(sql, [mailAddress, empNo, fullName, industries, sitens,foNaFlg, password, agencyId], callback);
}

// ======== Unique mail address check ==========
exports.uniqueEmailCheck = function (agencyId, mailaddress, callback) {
    let sql = "SELECT\
                *\
            FROM\
                m_agency ma\
            WHERE\
                ma.f_del_flg = '0' AND (ma.f_mailaddress = ? OR ma.f_id = ?)\
                AND ma.f_agency_id != ?";
    this.mysqlDB.excSQL(sql, [mailaddress, mailaddress, agencyId], callback);
}
// ======== Unique mail address check ==========
exports.uniqueIdCheck = function (agencyId, userName, callback) {
    let sql = "SELECT\
                *\
            FROM\
                m_agency ma\
            WHERE\
                ma.f_del_flg = '0' AND ma.f_id = ?\
                AND ma.f_agency_id != ?";
    this.mysqlDB.excSQL(sql, [userName, agencyId], callback);
}
// ======== Register parent Info ==========
exports.agencyParentInfo = function (parentId, callback) {
        let sql = "SELECT\
                agency.f_agency_id,\
                agency.f_agency_type as f_parent_agency_type,\
                agency.f_name as f_parent_name,\
                agency.f_name_kana as f_parent_name_kana,\
                agency.f_agency_cd as f_parent_agency_cd,\
                agency.f_sales_cd as f_parent_sales_cd\
            FROM\
                m_agency agency\
            WHERE\
                agency.f_del_flg = '0' AND agency.f_agency_id = ?";
    this.mysqlDB.excSQL(sql, [parentId], callback);
}



// SELECT\
//     ma.f_agency_id,\
//     ma.f_agency_type,\
//     ma.f_branch_cd,\
//     ma.f_agency_cd,\
//     ma.f_sales_cd,\
//     ma.f_name,\
//     ma.f_emp_cd,\
//     ma.f_name2,\
//     ma.f_zip,\
//     ma.f_prefecture_id,\
//     ma.f_town,\
//     ma.f_address,\
//     ma.f_tel,\
//     ma.f_fax,\
//     ma.f_url,\
//     ma.f_mailaddress,\
//     ma.f_id,\
//     ma.f_view_level,\
//     ma.f_acct_valid_flg\
// FROM\
//     m_agency ma\
// WHERE\
//     ma.f_del_flg = '0' AND ma.f_agency_id = ? AND 1=0\
// UNION\



// SELECT
//     ma.f_agency_id,
//     ma.f_agency_type,
//     ma.f_emp_kbn,
//     ma.f_branch_cd,
//     ma.f_agency_cd,
//     ma.f_company_cd,
//     ma.f_sales_cd,
//     ma.f_no_emp,
//     ma.f_trad_company_category,
//     ma.f_industry,
//     ma.f_occupation,
//     ma.f_emp_cd,
//     ma.f_name,
//     ma.f_name_kana,
//     ma.f_director_h_kbn,
//     ma.f_director_s_kbn,
//     ma.f_sales_name,
//     ma.f_representative_name,
//     ma.f_representative_name_kana,
//     ma.f_siten_cd,
//     ma.f_name2,
//     ma.f_name2_kana,
//     ma.f_foreign_nationality_flg,
//     ma.f_foreign_country,
//     ma.f_foreign_emp_flg,
//     ma.f_zip,
//     ma.f_prefecture_id,
//     ma.f_town,
//     ma.f_address,
//     ma.f_tel,
//     ma.f_fax,
//     ma.f_url,
//     ma.f_mailaddress,
//     ma.f_id,
//     ma.f_password,
//     ma.f_password AS f_password_conf,
//     ma.f_csv_del_off_flg,
//     ma.f_view_level,
//     ma.f_register_date,
//     ma.f_acct_valid_flg,
//     ma.f_authentication_flg,
//     ma.f_auth,
//     ma.f_register_date,
//     ma.f_basic_contract_date,
//     ma.f_date_consignment_contract,
//     ma.f_temporary_suspension_date,
//     ma.f_suspension_release_date,
//     ma.f_cancellation_date,
//     ma.f_revival_date,
//     ma.f_membership_date,
//     ma.f_membership_withdrawal_date,
//     ma.f_del_flg,
//     ma.f_reg_account,
//     ma.f_reg_time,
//     ma.f_upd_account,
//     ma.f_upd_time
// FROM
//     m_agency ma
// WHERE
//     ma.f_del_flg = '0' AND ma.f_agency_id = 24559