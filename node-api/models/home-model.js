var mysqlDB;
exports.init = function (mysqlDB) {
    this.mysqlDB = mysqlDB;
};

exports.getAllSlider = function (callback) {
    let sql = "SELECT *\
        FROM `t_top` \
        WHERE `f_del_flg` = 0 \
            AND ( `f_disp_flg` IS NULL or `f_disp_flg` = '' or `f_disp_flg` = '0' or `f_disp_flg` = 0 ) ";
    this.mysqlDB.excSQL(sql, [], callback);
};

exports.getAllBanner = function (callback) {
    let sql = "SELECT \
        `f_banner_id`,`f_image1`,`f_thumb1`,`f_image_alt1`,`f_page_content_id1`,`f_sort_no1`, \
        `f_image2`,`f_thumb2`,`f_image_alt2`,`f_page_content_id2`,`f_sort_no2`,\
        `f_image3`,`f_thumb3`,`f_image_alt3`,`f_page_content_id3`,`f_sort_no3`,\
        `f_image4`,`f_thumb4`,`f_image_alt4`,`f_page_content_id4`,`f_sort_no4`,\
        `f_image5`,`f_thumb5`,`f_image_alt5`,`f_page_content_id5`,`f_sort_no5`,\
        `f_image6`,`f_thumb6`,`f_image_alt6`,`f_page_content_id6`,`f_sort_no6` \
        FROM `t_banner` \
        WHERE `f_del_flg` = 0 \
            AND ( `f_disp_flg` IS NULL or `f_disp_flg` = '' or `f_disp_flg` = '0' or `f_disp_flg` = 0 ) ";
    this.mysqlDB.excSQL(sql, [], callback);
};

exports.getAllTopLink = function (callback) {
    let sql = "SELECT * \
        FROM `t_top_link` \
        WHERE \
                ( `f_del_flg` IS NULL or `f_del_flg` = '' or `f_del_flg` = '0' or `f_del_flg` = 0 ) \
            and ( `f_open_flg` IS NULL or `f_open_flg` = '' or `f_open_flg` = '1' or `f_open_flg` = 1 ) \
         ORDER BY f_order ASC ";
    this.mysqlDB.excSQL(sql, [], callback);
};