var mysqlDB;
exports.init = function (mysqlDB) {
    this.mysqlDB = mysqlDB;
};

// Keywords query
exports.getKeywords = function (callback) {
    let sql = "SELECT\
                    keyword.f_keyword_id,\
                    keyword.f_keyword,\
                    keyword.f_del_flg\
                FROM t_keyword keyword\
                WHERE\ keyword.f_del_flg = '0'\
                    AND keyword.f_disp_flg = '0'\
                ORDER BY keyword.f_order";
    this.mysqlDB.excSQL(sql, [], callback);
}

// Document search query
exports.getAllDocument = function (searchData = "", keywordId = "", orderBy = "ASC", userInfo = {"level": 0}, callback) {
    if (orderBy[0] === "D" ) {
        orderBy = "DESC";
    } else {
        orderBy = "ASC";
    }
    let sql = "SELECT\
                document.f_document_id,\
                document.f_title,\
                document.f_content,\
                document.f_category,\
                document.f_disp_flg,\
                document.f_new_arrival_flg,\
                if(document.`f_new_arrival_start_date` <= NOW() and document.`f_new_arrival_end_date` >= NOW(), 1, 0 ) as new_flg, \
                DATE_FORMAT(\
                    document.f_new_arrival_start_date,\
                    '%Y/%m/%d'\
                ) AS f_new_arrival_start_date,\
                DATE_FORMAT(\
                    document.f_new_arrival_end_date,\
                    '%Y/%m/%d'\
                ) AS f_new_arrival_end_date,\
                document.f_filename,\
                document.f_file,\
                document.f_extension,\
                document.f_file_size,\
                document.f_download_name,\
                document.f_keyword_ids AS f_document_keyword_ids,\
                document.f_order,\
                DATE_FORMAT(document.f_reg_time, '%Y.%m.%d') AS f_reg_time_ymd\
            FROM\
                t_document document\
            WHERE \
                document.f_del_flg = '0'\
                AND(document.f_title LIKE ? \
                    OR document.f_content LIKE ? \
                    OR document.f_category LIKE ? ) \
                AND document.f_disp_flg = '0' \
                AND ( ? = '' or FIND_IN_SET( ?, document.f_keyword_ids) ) \
                AND 1 <= ? \
            ORDER BY\
                document.f_order "+ orderBy + " ";
    this.mysqlDB.excSQL(sql, [
        "%" + searchData + "%",
        "%" + searchData + "%",
        "%" + searchData + "%",
        keywordId, keywordId,
        parseInt(userInfo["level"])], callback);
}