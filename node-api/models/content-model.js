var mysqlDB;

exports.init = function (mysqlDB) {
    this.mysqlDB = mysqlDB;
};

exports.getContentList = function (userInfo = {"level": 3}, callback) {
    sql =" \
        SELECT \
            * \
        FROM ( \
            SELECT  \
                page_content1.f_page_content_id, \
                page_content1.f_page_content_sub_id, \
                page_content1.f_page_title, \
                page_content1.f_current_position, \
                IF(CHAR_LENGTH(COALESCE(page_content1.f_parent_position,''))>0, \
                page_content1.f_parent_position, 0) AS f_parent_position, \
                page_content1.f_slag,  \
                DATE_FORMAT(page_content1.f_disp_date_from, '%Y/%m/%d') AS f_disp_date_from, \
                DATE_FORMAT(page_content1.f_disp_date_to, '%Y/%m/%d') AS f_disp_date_to, \
                page_content1.f_open_flg, \
                page_content1.f_new_flg, \
                page_content1.f_new_flg_days, \
                page_content1.f_individual_chk, \
                page_content1.f_view_level, \
                page_content1.f_order \
            FROM t_page_content page_content1 \
            WHERE  \
                    page_content1.f_del_flg = '0' \
                AND f_open_flg = '1' \
                AND f_individual_chk != '1' \
                AND page_content1.f_page_content_id = ( \
                        SELECT  \
                            f_page_content_id \
                        FROM t_page_content \
                        WHERE  \
                                f_del_flg = '0' \
                            AND f_open_flg = '1' \
                            And page_content1.f_view_level <= ? \
                            AND f_disp_date_from <= NOW() \
                            AND ( f_disp_date_to >= NOW() OR f_disp_date_to IS NULL ) \
                                AND f_page_content_sub_id = page_content1.f_page_content_sub_id \
                        ORDER BY ABS(NOW() - f_disp_date_from) ASC \
                        LIMIT 0, 1 \
                ) \
            GROUP BY page_content1.f_page_content_sub_id \
            ) page_content \
        ORDER BY page_content.f_order ";
    this.mysqlDB.excSQL(sql, [parseInt(userInfo["level"])], callback);
}

exports.getContentById = function (content_id, userInfo = {"level": 3}, callback) {
    let sql = "SELECT \
                    page_content.f_page_content_id, \
                    page_content.f_page_content_sub_id, \
                    page_content.f_page_title, \
                    page_content.f_current_position, \
                    page_content.f_current_position AS f_current_position_old, \
                    page_content.f_parent_position, \
                    page_content.f_parent_position AS f_parent_position_old, \
                    page_content.f_slag, \
                    DATE_FORMAT(page_content.f_disp_date_from, '%Y/%m/%d') AS f_disp_date_from, \
                    DATE_FORMAT( page_content.f_disp_date_to, '%Y/%m/%d' ) AS f_disp_date_to, \
                    DATE_FORMAT(page_content.f_disp_date_from,'%k') AS f_disp_hour_from, \
                    DATE_FORMAT( page_content.f_disp_date_from, '%i') AS f_disp_minute_from, \
                    DATE_FORMAT( page_content.f_disp_date_to, '%k' ) AS f_disp_hour_to, \
                    DATE_FORMAT( page_content.f_disp_date_to,'%i') AS f_disp_minute_to, \
                    DATE_FORMAT(page_content.f_disp_date_to, '%Y/%m/%d %H:%i:%s') AS f_disp_datetime_to, \
                    DATE_FORMAT(page_content.f_disp_date_from,'%Y/%m/%d %H:%i:%s') AS f_disp_datetime_from, \
                    page_content.f_open_flg, \
                    page_content.f_new_flg, \
                    page_content.f_new_flg_days, \
                    page_content.f_individual_chk, \
                    page_content.f_content, \
                    page_content.f_del_flg, \
                    page_content.f_reg_account, \
                    page_content.f_reg_time, \
                    page_content.f_upd_account, \
                    page_content.f_upd_time, \
                    page_content.f_order, \
                    page_content.f_view_level, \
                    t_page_content_file.* \
                FROM \
                    t_page_content page_content \
                    LEFT JOIN  \
                        `t_page_content_file` \
                    ON  `page_content`.`f_page_content_id` = `t_page_content_file`.`f_page_content_id` \
                        AND t_page_content_file.f_del_flg = '0' \
                WHERE \
                    page_content.f_del_flg = '0' \
                    AND page_content.f_open_flg = '1' AND page_content.f_page_content_sub_id = ? \
                        AND page_content.f_disp_date_from =( \
                    SELECT \
                        MAX(f_disp_date_from) \
                    FROM \
                        t_page_content \
                    WHERE \
                        f_del_flg = '0' AND f_open_flg = '1' AND f_page_content_sub_id = ? \
                        AND `f_view_level` <= ? \
                        AND f_disp_date_from <= NOW() AND( \
                            f_disp_date_to >= NOW() OR f_disp_date_to IS NULL) \
                        ) \
                ORDER BY t_page_content_file.f_order ASC";
    console.log({SQL: sql, DATA: [content_id, content_id, parseInt(userInfo["level"])]});
    this.mysqlDB.excSQL(sql, [content_id, content_id, parseInt(userInfo["level"])], callback);
}

exports.getSlagWiseContent = async function( slag, callback ) {
    let sql = "SELECT\
                    page_content.f_page_content_id,\
                    page_content.f_page_content_sub_id,\
                    (\
                    SELECT\
                        GROUP_CONCAT(f_page_title SEPARATOR ',')\
                    FROM\
                        t_page_content\
                    WHERE\
                        f_del_flg = '0' AND page_content.f_page_content_id = f_parent_position\
                ) AS f_child_slags\
                FROM\
                    t_page_content page_content\
                WHERE\
                    page_content.f_del_flg = '0' AND page_content.f_open_flg = '1' AND page_content.f_slag = ? AND page_content.f_disp_date_from =(\
                    SELECT\
                        MAX(f_disp_date_from)\
                    FROM\
                        t_page_content\
                    WHERE\
                        f_del_flg = '0' AND f_open_flg = '1' AND f_slag = ? AND f_disp_date_from <= NOW() AND(\
                            f_disp_date_to >= NOW() OR f_disp_date_to IS NULL)\
                        )\
                    LIMIT 1";
    await this.mysqlDB.excSQL( sql, [ slag, slag ], callback );
}



// exports.getContentById = function (content_id, userInfo = {"level": 3}, callback) {
//     let sql = "SELECT \
//                     * \
//                 FROM ( \
//                     SELECT  \
//                         page_content1.f_page_content_id, \
//                         page_content1.f_page_content_sub_id, \
//                         page_content1.f_page_title, \
//                         page_content1.f_current_position, \
//                         IF(CHAR_LENGTH(COALESCE(page_content1.f_parent_position,'' ))>0, \
//                         page_content1.f_parent_position, 0) AS f_parent_position, \
//                         page_content1.f_slag,  \
//                         DATE_FORMAT(page_content1.f_disp_date_from, '%Y/%m/%d') AS f_disp_date_from, \
//                         DATE_FORMAT(page_content1.f_disp_date_to, '%Y/%m/%d') AS f_disp_date_to, \
//                         page_content1.f_open_flg, \
//                         page_content1.f_new_flg, \
//                         page_content1.f_new_flg_days, \
//                         page_content1.f_individual_chk, \
//                         page_content1.f_view_level, \
//                         page_content1.f_order, \
//                         page_content1.f_content, \
//                         t_file.f_page_content_id as \"file_content_id\", \
//                         t_file.* \
//                     FROM t_page_content page_content1 \
//                     LEFT JOIN `t_page_content_file` t_file \
//                         ON page_content1.`f_page_content_id` = t_file.`f_page_content_id` \
//                     WHERE  \
//                             page_content1.f_del_flg = '0' \
//                         AND f_open_flg = '1' \
//                         AND f_individual_chk != '1' \
//                         AND page_content1.f_page_content_id = ( \
//                                 SELECT  \
//                                     f_page_content_id \
//                                 FROM t_page_content \
//                                 WHERE  \
//                                         f_del_flg = '0' \
//                                     AND f_open_flg = '1' \
//                                     And page_content1.f_view_level = ? \
//                                     and page_content1.f_page_content_sub_id = ? \
//                                     AND f_disp_date_from <= NOW() \
//                                     AND ( f_disp_date_to >= NOW() OR f_disp_date_to IS NULL ) \
//                                         AND f_page_content_sub_id = page_content1.f_page_content_sub_id \
//                                 ORDER BY ABS(NOW() - f_disp_date_from) ASC \
//                                 LIMIT 0, 1 \
//                         ) \
//                     GROUP BY page_content1.f_page_content_sub_id \
//                     ) page_content \
//                     ORDER BY page_content.f_order " ;
//     this.mysqlDB.excSQL(sql, [content_id, parseInt(userInfo["level"])], callback);
// }

// exports.getAllContent = function (userInfo = {"level": 3}, callback) {
//     let sql = "select `f_page_content_id`, `f_parent_position`, `f_current_position`, \
//     `f_page_title`, `f_slag`, `f_disp_date_from`, `f_disp_date_to`] \
//     FROM `t_page_content`\
//     WHERE `f_del_flg` = 0 \
//         AND now() between `f_disp_date_from` \
//         AND IFNULL(`f_disp_date_to`, now())\
//         AND `f_view_level` <= ? ";
//     this.mysqlDB.excSQL(sql, [parseInt(userInfo["level"])], callback);
// }



/*

exports.getContentList = function (userInfo = {"level": 3}, callback) {
    sql = 
        "select \
            `f_page_content_id`, `f_parent_position`, `f_current_position`, `f_page_title`, `f_slag`, \
            `f_disp_date_from`, \
            IFNULL(`f_disp_date_to`, now()) as f_disp_date_to, \
            IF(`f_new_flg` IS NULL or `f_new_flg` = '' or `f_new_flg` = ' ', '0', `f_new_flg`) as f_new_flg \
        FROM `t_page_content` \
        WHERE \
            `f_del_flg` = 0 \
            and f_open_flg = 1 \
            AND now() between `f_disp_date_from` \
            AND IFNULL(`f_disp_date_to`, now()) \
            AND `f_current_position` is not null \
            AND `f_view_level` <= ? \
        ORDER BY \
            `t_page_content`.`f_parent_position`, f_order ASC";
    this.mysqlDB.excSQL(sql, [parseInt(userInfo["level"])], callback);
}


SELECT 
	*
FROM (
	SELECT 
		page_content1.f_page_content_id,page_content1.f_page_content_sub_id,page_content1.f_page_title,page_content1.f_current_position,
		IF(CHAR_LENGTH(COALESCE(page_content1.f_parent_position,''))>0,
		page_content1.f_parent_position, 0) AS f_parent_position,
	 	page_content1.f_slag, 
	 	DATE_FORMAT(page_content1.f_disp_date_from, '%Y/%m/%d') AS f_disp_date_from, 
	 	DATE_FORMAT(page_content1.f_disp_date_to, '%Y/%m/%d') AS f_disp_date_to,
	 	page_content1.f_open_flg,page_content1.f_new_flg,page_content1.f_new_flg_days,page_content1.f_individual_chk,page_content1.f_content,page_content1.f_view_level,page_content1.f_order
	FROM t_page_content page_content1
	WHERE 
		    page_content1.f_del_flg = '0' 
		AND f_open_flg = '1' 
		AND f_individual_chk != '1' 
		AND page_content1.f_page_content_id = (
				SELECT 
					f_page_content_id
				FROM t_page_content
				WHERE 
						 f_del_flg = '0' 
					AND f_open_flg = '1' 
					AND f_disp_date_from <= NOW() 
					AND ( f_disp_date_to >= NOW() OR f_disp_date_to IS NULL ) 
						AND f_page_content_sub_id = page_content1.f_page_content_sub_id
				ORDER BY ABS(NOW() - f_disp_date_from) ASC
				LIMIT 0, 1
		)
	GROUP BY page_content1.f_page_content_sub_id
	) page_content
ORDER BY page_content.f_order


exports.getContentById = function (content_id, callback) {
    let sql = "SELECT * FROM `t_page_content` \
        LEFT JOIN `t_page_content_file` \
            ON `t_page_content`.`f_page_content_id` = `t_page_content_file`.`f_page_content_id` \
        WHERE \
            `t_page_content`.`f_del_flg` = 0 \
            AND now() between `t_page_content`.`f_disp_date_from` AND IFNULL(`t_page_content`.`f_disp_date_to`, now()) \
            AND `t_page_content`.`f_page_content_id` = ? " ;
    this.mysqlDB.excSQL(sql, [content_id], callback);
}
*/