var mysqlDB;
exports.init = function (mysqlDB) {
    this.mysqlDB = mysqlDB;
};

exports.getKeywords = function (callback) {
    let sql = "SELECT\
                    keyword.f_keyword_id,\
                    keyword.f_keyword,\
                    keyword.f_del_flg\
                FROM t_keyword keyword\
                WHERE\ keyword.f_del_flg = '0'\
                    AND keyword.f_disp_flg = '0'\
                ORDER BY keyword.f_order";
    this.mysqlDB.excSQL(sql, [], callback);
}

exports.getAllMovie = function (searchData = "", keywordId = "", orderBy = "ASC", userInfo = {"level": 0}, callback) {
    if (orderBy[0] === "D" ) {
        orderBy = "DESC";
    } else {
        orderBy = "ASC";
    }

    let sql = "SELECT \
                movie.f_movie_id,\
                movie.f_title,\
                movie.f_property,\
                movie.f_description,\
                movie.f_disp_date_from, \
                movie.f_category,\
                movie.f_disp_flg,\
                movie.f_new_arrival_flg,\
                if(movie.`f_new_arrival_start_date` <= NOW() and movie.`f_new_arrival_end_date` >= NOW(), 1, 0 ) as new_flg, \
                DATE_FORMAT(\
                    movie.f_new_arrival_start_date,\
                    '%Y/%m/%d'\
                ) AS f_new_arrival_start_date,\
                DATE_FORMAT(\
                    movie.f_new_arrival_end_date,\
                    '%Y/%m/%d'\
                ) AS f_new_arrival_end_date,\
                movie.f_movie_filename,\
                movie.f_video_name,\
                movie.f_image,\
                movie.f_thumb,\
                movie.t_movie_keyword_ids,\
                movie.f_order,\
                movie.f_del_flg\
            FROM\
                t_movie movie\
            WHERE\
                movie.f_disp_date_from <= now() \
                AND movie.f_del_flg = '0' \
                AND(movie.f_title LIKE ? \
                    OR movie.f_property LIKE ? \
                    OR movie.f_description LIKE ?) \
                AND movie.f_disp_flg = '0' \
                AND ( ? = '' or FIND_IN_SET( ? ,movie.t_movie_keyword_ids) ) \
                AND movie.f_view_level <= ? \
            ORDER BY\
                movie.f_order " + orderBy +  " ";
    this.mysqlDB.excSQL(sql, ["%" + searchData + "%",  "%" + searchData + "%", "%" + searchData + "%", keywordId, keywordId, parseInt(userInfo["level"])], callback);
}

// AND DATE_FORMAT(movie.f_disp_date_from, '%Y%m%d%H%i') <= '201912121601'\
// AND FIND_IN_SET("+keywordId+", movie.t_movie_keyword_ids)\
// SELECT
//     movie.f_movie_id,
//     movie.f_title,
//     movie.f_property,
//     movie.f_description,
//     movie.f_category,
//     movie.f_disp_flg,
//     movie.f_new_arrival_flg,
//     DATE_FORMAT(
//         movie.f_new_arrival_start_date,
//         '%Y/%m/%d'
//     ) AS f_new_arrival_start_date,
//     DATE_FORMAT(
//         movie.f_new_arrival_end_date,
//         '%Y/%m/%d'
//     ) AS f_new_arrival_end_date,
//     movie.f_movie_filename,
//     movie.f_video_name,
//     movie.f_image,
//     movie.f_thumb,
//     movie.t_movie_keyword_ids,
//     movie.f_order,
//     movie.f_del_flg
// FROM
//     t_movie movie
// WHERE
//     movie.f_del_flg = '0' AND movie.f_disp_flg = '0' AND FIND_IN_SET(1, movie.t_movie_keyword_ids) AND movie.f_view_level <= '3' AND DATE_FORMAT(
//         movie.f_disp_date_from,
//         '%Y%m%d%H%i'
//     ) <= '201912121706'
// ORDER BY
//     movie.f_view_level
// DESC
//     ,
//     movie.f_order ASC
// LIMIT 0, 5