var mysqlDB;

exports.init = function (mysqlDB) {
    this.mysqlDB = mysqlDB;
};

exports.getFile = function (fileInfo, userInfo = undefined, callback) {
    //console.log(userInfo);
    let table = fileInfo["table"];
    let field = fileInfo["field"];
    let  sql = "SELECT  count(*) as count FROM `" + table + "` WHERE ??  = ? ";
    // let  sql = "SELECT  count(*) as count FROM `" + table + "` WHERE " + mysqlDB.connDB.escapeId(field ) + "  = ? ";

    let fileName = [];
    if(fileInfo !== undefined) {
        fileName = fileInfo["value"];
    }

    
    if( userInfo !== undefined) {
        let viewLevel = userInfo["level"];
        sql += "AND `f_view_level` <= " + viewLevel;
    }

    //console.log(sql, [field, fileName]);
    
    this.mysqlDB.excSQL(sql, [field, fileName], callback);
}